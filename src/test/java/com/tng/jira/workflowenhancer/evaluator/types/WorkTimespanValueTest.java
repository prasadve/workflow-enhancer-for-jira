package com.tng.jira.workflowenhancer.evaluator.types;

import static org.fest.assertions.api.Assertions.assertThat;
import junit.framework.TestCase;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;
import com.tng.jira.workflowenhancer.evaluator.types.WorkTimespanValue;

public class WorkTimespanValueTest extends TestCase {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testConstructor() {
        setParams(4, 8);
        assertThat(new WorkTimespanValue("4w 3d 2h 1min").toString()).isEqualTo("4w 3d 2h 1min");
        assertThat(new WorkTimespanValue("1w2d3h4min").toString()).isEqualTo("1w 2d 3h 4min");
    }

    @Test
    public void testConstructor2() {
        setParams(6, 9);
        assertThat(new WorkTimespanValue(1, 2, 3, 44).toString()).isEqualTo("1w 2d 3h 44min");
        assertThat(new WorkTimespanValue(-1, 7, 0, 0).toString()).isEqualTo("0w 1d 0h 0min");
        assertThat(new WorkTimespanValue(-1, 5, 0, 0).toString()).isEqualTo("0w 1d 0h 0min");
        assertThat(new WorkTimespanValue(-1, 6, 0, 0).toString()).isEqualTo("0w 0d 0h 0min");
        assertThat(new WorkTimespanValue(-1, 5, 10, -60).toString()).isEqualTo("0w 0d 0h 0min");
    }

    @Test
    public void testConstructor3() {
        setParams(4, 4);
        assertThat(new WorkTimespanValue(-1).toString()).isEqualTo("0w 0d 0h 1min");
        assertThat(new WorkTimespanValue(1).toString()).isEqualTo("0w 0d 0h 1min");
        assertThat(new WorkTimespanValue(1624).toString()).isEqualTo("1w 2d 3h 4min");
        assertThat(new WorkTimespanValue(-1624).toString()).isEqualTo("1w 2d 3h 4min");
    }

    @Test
    public void testGets() {
        setParams(6, 7);
        assertThat(new WorkTimespanValue("1w 2d 3h 44min").getInMinutes()).isEqualTo(3584);
        assertThat(new WorkTimespanValue(3584).getWeeks()).isEqualTo(1);
        assertThat(new WorkTimespanValue(3584).getDays()).isEqualTo(2);
        assertThat(new WorkTimespanValue(3584).getHours()).isEqualTo(3);
        assertThat(new WorkTimespanValue(3584).getMinutes()).isEqualTo(44);
        assertThat(new WorkTimespanValue(3584).getInMinutes()).isEqualTo(3584);
    }

    @Test
    public void testFloat() {
        setParams(5.5f, 6.25f);
        assertThat(new WorkTimespanValue("1d").getInMinutes()).isEqualTo(375);
        // 1w == 2062,5 minutes
        assertThat(new WorkTimespanValue("1w").getInMinutes()).isEqualTo(2063);
        assertThat(new WorkTimespanValue("1w 2d 1h 27min").getInMinutes()).isEqualTo(2900);
        assertThat(new WorkTimespanValue("1w 37min").getInMinutes()).isEqualTo(2100);
        // 1w = 2550 minutes
        setParams(5f, 8.5f);
        assertThat(new WorkTimespanValue(2600).toString()).isEqualTo("1w 0d 0h 50min");
        assertThat(new WorkTimespanValue(1, 0, 0, 50).toString()).isEqualTo("1w 0d 0h 50min");
    }

    @Test
    public void testFloat2() {
        setParams(3.14895752f, 12.756832148521f);
        assertThat(new WorkTimespanValue("1d").getInMinutes()).isEqualTo(765);
    }

    @Test
    public void testAdd() throws IncompatibleTypesException {
        WorkTimespanValue span1 = new WorkTimespanValue("0w0d0h99min");
        WorkTimespanValue span2 = new WorkTimespanValue("0w0d0h1min");
        assertThat(span1.add(span2).toString()).isEqualTo("0w 0d 1h 40min");

        span1 = new WorkTimespanValue("-80");
        span2 = new WorkTimespanValue("80");
        assertThat(span1.add(span2).toString()).isEqualTo("0w 0d 0h 0min");

        span1 = new WorkTimespanValue(-80);
        span2 = new WorkTimespanValue(80);
        assertThat(span1.add(span2).toString()).isEqualTo("0w 0d 2h 40min");
    }

    @Test
    public void testSubtract() throws IncompatibleTypesException {
        setParams(4.5f, 3f);
        WorkTimespanValue span1 = new WorkTimespanValue("1w 1d 0h 0min");
        WorkTimespanValue span2 = new WorkTimespanValue(" 5d 1h 30min");
        assertThat(span1.subtract(span2).toString()).isEqualTo("0w 0d 0h 0min");
    }

    @Ignore
    private void setParams(float DaysPerWeek, float HoursPerDay) {
        WorkTimespanValue.numberOfDaysPerWorkingWeek = DaysPerWeek;
        WorkTimespanValue.numberOfHoursPerWorkingDay = HoursPerDay;
    }
}