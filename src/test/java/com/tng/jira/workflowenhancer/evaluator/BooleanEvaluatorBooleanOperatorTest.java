package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class BooleanEvaluatorBooleanOperatorTest extends BooleanEvaluatorMockTests {

    /**
     * Tests if expressions containing booleanOperators.
     * 
     * @param expression1 a complete expression like "5<6"
     * @param booleanOperator e.g. "|", "AND", "&&"
     * @param expression2 a complete expression like "5<6"
     * @return true if and only if _expression1 + booleanOperator + expression2_ is evaluated to true
     * @throws Exception
     */
    public boolean evaluationResultOfBooleanExpression(String expression1, String booleanOperator, String expression2)
            throws Exception {
        boolean result = booleanEvaluator.evaluate("(" + expression1 + ")" + booleanOperator + "(" + expression2 + ")");
        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testOrBothTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<6", "|", "7>5")).isTrue();
    }

    @Test
    public void testOrFirstTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("4<5", "|", "7<6")).isTrue();
    }

    @Test
    public void testOrSecondTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<4", "|", "5<7")).isTrue();
    }

    @Test
    public void testOrFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<1", "|", "5<2")).isFalse();
    }

    @Test
    public void testOrOrBothTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<6", "||", "7>5")).isTrue();
    }

    @Test
    public void testOrOrFirstTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("4<5", "||", "7<6")).isTrue();
    }

    @Test
    public void testOrOrSecondTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<4", "||", "5<7")).isTrue();
    }

    @Test
    public void testOrOrFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<1", "||", "5<2")).isFalse();
    }

    @Test
    public void testORBothTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<6", "OR", "7>5")).isTrue();
    }

    @Test
    public void testORFirstTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("4<5", "OR", "7<6")).isTrue();
    }

    @Test
    public void testORSecondTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<4", "OR", "5<7")).isTrue();
    }

    @Test
    public void testORFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("5<1", "OR", "5<2")).isFalse();
    }

    @Test
    public void testAndAndTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("3<4", "&&", "3<5")).isTrue();
    }

    @Test
    public void testAndAndFirstFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "&&", "5<6")).isFalse();
    }

    @Test
    public void testAndAndSecondFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("1<3", "&&", "2<1")).isFalse();
    }

    @Test
    public void testAndAndBothFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "&&", "2<1")).isFalse();
    }

    @Test
    public void testAndTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("3<4", "&", "3<5")).isTrue();
    }

    @Test
    public void testAndFirstFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "&", "5<6")).isFalse();
    }

    @Test
    public void testAndSecondFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("1<3", "&", "2<1")).isFalse();
    }

    @Test
    public void testAndBothFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "&", "2<1")).isFalse();
    }

    @Test
    public void testANDTrue() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("3<4", "AND", "3<5")).isTrue();
    }

    @Test
    public void testANDFirstFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "AND", "5<6")).isFalse();
    }

    @Test
    public void testANDSecondFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("1<3", "AND", "2<1")).isFalse();
    }

    @Test
    public void testANDBothFalse() throws Exception {
        assertThat(evaluationResultOfBooleanExpression("2<1", "AND", "2<1")).isFalse();
    }
}