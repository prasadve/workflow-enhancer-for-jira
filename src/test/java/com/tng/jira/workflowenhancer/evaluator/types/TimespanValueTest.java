package com.tng.jira.workflowenhancer.evaluator.types;
//package com.tng.jira.workflowenhancer.evaluator.types;
//
//import junit.framework.TestCase;
//
//import org.junit.Ignore;
//import org.junit.Test;
//@Ignore
//public class TimespanValueTest extends TestCase {
//
//   
//    @Test
//    public void testConstructor() {
//        setParams();
//        assertTrue((new TimespanValue("1w 2d 3h 4min")).toString().equals("1w 2d 3h 4min"));
//        assertTrue((new TimespanValue("1w2d3h4min")).toString().equals("1w 2d 3h 4min"));
//    }
//    
//    
//    
//    @Test
//    public void testConstructor2() {
//        setParams();        
//        assertTrue(new TimespanValue(1, 2, 3, 44).toString().equals("1w 2d 3h 44min"));
//
//        assertTrue(new TimespanValue(-1, 6, 0, 0).toString().equals("0w 1d 0h 0min"));
//        assertTrue(new TimespanValue(-1, 7, 0, 0).toString().equals("0w 0d 0h 0min"));
//        assertTrue(new TimespanValue(-1, 6, 25, -60).toString().equals("0w 0d 0h 0min"));
//
//        assertTrue((new TimespanValue(-12, 34, 567, -8)).toString().equals("3w 5d 9h 8min"));
//    }
//    
//    @Test
//    public void testConstructor3() {
//        setParams();
//        assertTrue(new TimespanValue(-1).toString().equals("0w 0d 0h 1min"));
//        assertTrue(new TimespanValue(1).toString().equals("0w 0d 0h 1min"));
//
//        assertTrue(new TimespanValue(37988).toString().equals("3w 5d 9h 8min"));
//    }
//
//    @Test
//    public void testGets() {
//        setParams();
//        assertTrue(new TimespanValue("1w 2d 3h 44min").getInMinutes() == 13184);
//
//        assertTrue(new TimespanValue(37988).getWeeks() == 3);
//        assertTrue(new TimespanValue(37988).getDays() == 5);
//        assertTrue(new TimespanValue(37988).getHours() == 9);
//        assertTrue(new TimespanValue(37988).getMinutes() == 8);
//
//        assertTrue(new TimespanValue(37988).getInMinutes() == 37988);
//    }
//
//    @Test
//    public void testAdd() {
//        setParams();
//        TimespanValue span1 = new TimespanValue("0w0d0h99min");
//        TimespanValue span2 = new TimespanValue("0w0d0h1min");
//        assertTrue(span1.add(span2).toString().equals("0w 0d 1h 40min"));
//
//        span1 = new TimespanValue("-80");
//        span2 = new TimespanValue("80");
//        assertTrue(span1.add(span2).toString().equals("0w 0d 0h 0min"));
//    }
//
//    @Test
//    public void testSubtract() throws IncompatibleTypesException {
//        setParams();
//        TimespanValue span1 = new TimespanValue(-3456);
//        TimespanValue span2 = new TimespanValue(87654);
//        assertTrue(span1.subtract(span2).equals(new TimespanValue(84198)));
//    }
//    
//    private void setParams(){
//        TimespanValue.numberOfDaysPerWeek  = 7f;
//        TimespanValue.numberOfHoursPerDay = 24f;
//        }
//}
