package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.jira.user.ApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorAssigneeFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{Assignee}" + orderOperation + expression_
     * 
     * @param fieldValue The username of the assignee
     * @param orderOperator e.g. "=="
     * @param expression e.g. "\"merkela\""
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean assigneeFieldWithValue(String fieldValue, String orderOperator, String expression)
            throws ParseException {
        // Given:
        ApplicationUser user = mock(ApplicationUser.class);
        setReturnValuesForFieldIdAndField("Assignee", IssueFieldConstants.ASSIGNEE);
        when(issue.getAssigneeUser()).thenReturn(user);
        when(user.getName()).thenReturn(fieldValue);

        // When:
        boolean result = booleanEvaluator.evaluate("{Assignee}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testAssigneeEquals() throws Exception {
        assertThat(assigneeFieldWithValue("merkela", "==", "\"merkela\"")).isTrue();
    }

    @Test
    public void testAssigneeEqualsFalse() throws Exception {
        assertThat(assigneeFieldWithValue("merkela", "==", "\"admin\"")).isFalse();
    }

    @Test
    public void testAssigneeNotEquals() throws Exception {
        assertThat(assigneeFieldWithValue("merkela", "!=", "\"gabries\"")).isTrue();
    }

}
