package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.jira.user.ApplicationUser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorReporterFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{Reporter}" + orderOperator + expression_
     * 
     * @param fieldValue The username of the assignee
     * @param orderOperator e.g. "=="
     * @param expression e.g. "\"admin\""
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean reporterFieldWithValue(String fieldValue, String orderOperator, String expression)
            throws ParseException {
        // Given:
        ApplicationUser user = mock(ApplicationUser.class);
        setReturnValuesForFieldIdAndField("Reporter", IssueFieldConstants.REPORTER);
        when(issue.getReporter()).thenReturn(user);
        when(user.getName()).thenReturn(fieldValue);

        // When:
        boolean result = booleanEvaluator.evaluate("{Reporter}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testReporterEquals() throws Exception {
        assertThat(reporterFieldWithValue("merkela", "==", "\"merkela\"")).isTrue();
    }

    @Test
    public void testReporterEqualsFalse() throws Exception {
        assertThat(reporterFieldWithValue("admin", "==", "\"merkela\"")).isFalse();
    }

    @Test
    public void testReporterNotEquals() throws Exception {
        assertThat(reporterFieldWithValue("gabries", "!=", "\"admin\"")).isTrue();
    }

}
