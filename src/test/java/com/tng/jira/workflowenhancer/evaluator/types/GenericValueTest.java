package com.tng.jira.workflowenhancer.evaluator.types;

import com.tng.jira.workflowenhancer.evaluator.types.DateValue;
import com.tng.jira.workflowenhancer.evaluator.types.FloatValue;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValue;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValueFactory;
import com.tng.jira.workflowenhancer.evaluator.types.RealTimespanValue;
import com.tng.jira.workflowenhancer.evaluator.types.RegExValue;
import com.tng.jira.workflowenhancer.evaluator.types.StringValue;
import com.tng.jira.workflowenhancer.evaluator.types.WorkTimespanValue;

import junit.framework.TestCase;

public class GenericValueTest extends TestCase {

    public void testRun() throws IncompatibleTypesException {
    	GenericValueFactory genericValueFactory = new GenericValueFactory(null, null, null, null, null);
        String testCase;
        GenericValue genericValue;

        genericValue = null;
        testCase = "10.01.2000";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof DateValue);

        genericValue = null;
        testCase = "10.01.2000 23:45";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof DateValue);

        genericValue = null;
        testCase = "10.01.2000 00:00";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof DateValue);

        genericValue = null;
        testCase = "1.12";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);

        genericValue = null;
        testCase = "9e-34";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);

        genericValue = null;
        testCase = "+133E+7";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);
        assertEquals(genericValue.compareTo(new FloatValue(1330000000d)), 0);

        genericValue = null;
        testCase = "0";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);

        genericValue = null;
        testCase = "0.3";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);

        genericValue = null;
        testCase = ".7";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof FloatValue);

        genericValue = null;
        testCase = "1d";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertFalse(genericValue instanceof FloatValue);
        assertTrue(genericValue instanceof WorkTimespanValue);

        genericValue = null;
        testCase = "1D";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertFalse(genericValue instanceof FloatValue);
        assertTrue(genericValue instanceof RealTimespanValue);

        genericValue = null;
        testCase = "\"HelloWorld\"";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof StringValue);

        //WorkTimespanValue
        genericValue = null;
        testCase = "1w2d10min";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof WorkTimespanValue);

        genericValue = null;
        testCase = "5min";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof WorkTimespanValue);

        //Real TimespanValue
        genericValue = null;
        testCase = "1W2D5H 3MIN";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof RealTimespanValue);

        genericValue = null;
        testCase = "2D 51MIN";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof RealTimespanValue);

        genericValue = null;
        testCase = "/^.{1,9}$/";
        genericValue = genericValueFactory.getGenericValueFromString(testCase, null);
        assertFalse(genericValue.getValue() == null);
        assertTrue(genericValue instanceof RegExValue);
    }
}
