package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.joda.time.DateTime;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorDuedateFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{Duedate}" + orderOperator + expression_ The value of {Duedate} is day.month.year hour:minute
     * 
     * @param year year >= 1900
     * @param month 1 <= month <= 12
     * @param day
     * @param hour
     * @param minute
     * @param orderOperator e.g. "=="
     * @param expression e.g. "19.12.1989"
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean duedateFieldWithValue(int year, int month, int day, int hour, int minute, String orderOperator,
            String expression) throws ParseException {

        DateTime a = new DateTime(year, month, day, hour, minute, 0, 0);
        Timestamp value = new Timestamp(a.getMillis());
        // Given:
        assertThat(year >= 1900).isTrue();
        setReturnValuesForFieldIdAndField("Duedate", IssueFieldConstants.DUE_DATE);
        when(issue.getDueDate()).thenReturn(value);

        // When:
        boolean result = booleanEvaluator.evaluate("{Duedate}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testDuedateEqualsDate() throws Exception {
        assertThat(duedateFieldWithValue(1989, 12, 19, 0, 0, "==", "19.12.1989")).isTrue();
    }

    @Test
    public void testDuedateEqualsDateTime() throws Exception {
        assertThat(duedateFieldWithValue(1970, 1, 21, 0, 0, "==", "21.01.1970 00:00")).isTrue();
    }

    @Test
    public void testDuedateEqualsDateTime2() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 12, "==", "17.08.2025 12:12")).isTrue();
    }

    @Test
    public void testDuedateNotEqualsDateTime() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 12, "!=", "17.08.2025 12:13")).isTrue();
    }

    @Test
    public void testDuedateSmallerOrEqualDate() throws Exception {
        assertThat(duedateFieldWithValue(1980, 11, 2, 15, 12, "<=", "17.08.2000")).isTrue();
    }

    @Test
    public void testDuedateSmallerOrEqualDateTime() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 12, "<=", "17.08.2025 12:13")).isTrue();
    }

    @Test
    public void testDuedateSmallerOrEqualDateTime2() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 16, 23, 59, "<=", "17.08.2025 00:00")).isTrue();
    }

    @Test
    public void testDuedateSmallerOrEqualDateTime3() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 13, "<=", "17.08.2025 12:13")).isTrue();
    }

    @Test
    public void testDuedateGreaterOrEqualDate() throws Exception {
        assertThat(duedateFieldWithValue(2000, 11, 2, 15, 12, ">=", "17.08.2000")).isTrue();
    }

    @Test
    public void testDuedateGreaterOrEqualDateTime() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 12, ">=", "17.08.2025 12:11")).isTrue();
    }

    @Test
    public void testDuedateSmallerDate() throws Exception {
        assertThat(duedateFieldWithValue(1980, 11, 2, 15, 12, "<", "17.08.2000")).isTrue();
    }

    @Test
    public void testDuedateSmallerDateTime() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 16, 23, 59, "<", "17.08.2025 00:00")).isTrue();
    }

    @Test
    public void testDuedateGreaterDate() throws Exception {
        assertThat(duedateFieldWithValue(2000, 11, 2, 15, 12, ">", "17.08.2000")).isTrue();
    }

    @Test
    public void testDuedateGreaterDateTime() throws Exception {
        assertThat(duedateFieldWithValue(2025, 8, 17, 12, 12, ">", "17.08.2025 12:11")).isTrue();
    }

    /**
     * The exception is thrown in ArithmeticEvaluator and caught in BooleanEvaluator
     * 
     * @throws Exception
     */
    @Test
    public void testIncompatibleTypesExceptionEquals() throws Exception {
        assertThat(duedateFieldWithValue(1992, 10, 9, 15, 55, "==", "45621.5713")).isFalse();
    }

    /**
     * The exception is thrown in ArithmeticEvaluator and caught in BooleanEvaluator
     * 
     * @throws Exception
     */
    @Test
    public void testIncompatibleTypesExceptionGreater() throws Exception {
        assertThat(duedateFieldWithValue(2012, 10, 15, 13, 37, ">", "5.8")).isFalse();
    }

    /**
     * The exception is thrown in ArithmeticEvaluator and caught in BooleanEvaluator
     * 
     * @throws Exception
     */
    @Test
    public void testIncompatibleTypesExceptionGreaterEquals() throws Exception {
        assertThat(duedateFieldWithValue(2015, 11, 24, 13, 42, ">=", ".45687")).isFalse();
    }

    /**
     * The exception is thrown in ArithmeticEvaluator and caught in BooleanEvaluator
     * 
     * @throws Exception
     */
    @Test
    public void testIncompatibleTypesExceptionLess() throws Exception {
        assertThat(duedateFieldWithValue(2016, 9, 25, 4, 46, "<", "237.1337")).isFalse();
    }

    /**
     * The exception is thrown in ArithmeticEvaluator and caught in BooleanEvaluator
     * 
     * @throws Exception
     */
    @Test
    public void testIncompatibleTypeExceptionLessEqual() throws Exception {
        assertThat(duedateFieldWithValue(2024, 5, 27, 8, 57, "<=", "78654324.74356451374")).isFalse();
    }

    @Test
    public void testParseException() throws Exception {
        exception.expect(ParseException.class);
        duedateFieldWithValue(2012, 5, 6, 7, 45, "<", ">5");
    }
}