package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.ArithmeticEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ASTArithmeticOperator;
import com.tng.jira.workflowenhancer.evaluator.parser.ASTAtom;
import com.tng.jira.workflowenhancer.evaluator.parser.ASTLiteral;
import com.tng.jira.workflowenhancer.evaluator.types.DateValue;
import com.tng.jira.workflowenhancer.evaluator.types.FloatValue;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValue;
import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;
import com.tng.jira.workflowenhancer.evaluator.types.RealTimespanValue;
import com.tng.jira.workflowenhancer.evaluator.types.StringValue;
import com.tng.jira.workflowenhancer.evaluator.types.WorkTimespanValue;

public class ArithmeticEvaluatorTest {

    public static final Logger LOGGER = Logger.getLogger(ArithmeticEvaluatorTest.class);

    protected ArithmeticEvaluator arithmeticEvaluator;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    //    @Before
    //    public void setParameters(){
    //        TimespanValue.numberOfDaysPerWeek=7;
    //        TimespanValue.numberOfHoursPerDay=24;
    //    }

    @Before
    public void setup() {
        new MockComponentWorker()
                .addMock(LabelManager.class, mock(LabelManager.class))
                .init();
        arithmeticEvaluator = new ArithmeticEvaluator(null, null, null, null, null, null, null, null, null);
    }

    @Test
    public void testFloatPlusFloat() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("53.1111", "+", "5.2222")).isEqualsToByComparingFields(
                new FloatValue(58.3333));
    }

    @Test
    public void testStringPlusFloat() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("Teststring", "+", "5658786.789")).isEqualsToByComparingFields(
                new StringValue("Teststring"));
    }

    @Test
    public void testFloatPlusString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("5", "+", "Teststring")).isEqualsToByComparingFields(new FloatValue(5));
    }

    @Test
    public void testStringPlusString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("String", "+", "test")).isEqualsToByComparingFields(
                new StringValue("Stringtest"));
    }

    @Test
    public void testFloatMinusFloat() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("8984.625", "-", ".125"))
                .isEqualsToByComparingFields(new FloatValue(8984.5));
    }

    @Test
    public void testFloatMinusString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("4568786", "-", "Tringstest")).isEqualsToByComparingFields(
                new FloatValue(4568786));
    }

    @Test
    public void testStringMinusFloat() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("Strineldy - the little string", "-", ".4562482"))
                .isEqualsToByComparingFields(new StringValue("Strineldy - the little string"));
    }

    @Test
    public void testStringMinusString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("a String", "-", "Test")).isEqualsToByComparingFields(
                new StringValue("a String"));
    }

    //Date plus Date is not defined so the result must be a Stringvalue
    @Test
    public void testDatePlusDateNotDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("24.12.2012", "+", "15.03.2011")).isEqualsToByComparingFields(
                new DateValue("24.12.2012"));
    }

    // 1440 minutes = 1 day
    @Test
    public void testDateMinusDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("25.12.2012", "-", "24.12.2012")).isEqualsToByComparingFields(
                new RealTimespanValue("1D"));
    }

    @Test
    public void testTimespanPlusTimespanNotDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("4W 3D 4H 3MIN", "+", "2W 3D 4H 5MIN")).isEqualsToByComparingFields(
                new RealTimespanValue("6W 6D 8H 8MIN"));
    }

    @Test
    public void testTimespanPlusTimespanNotString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("4w 3d 4h 3min", "+", "2w 3d 4h 5min")).isEqualsToByComparingFields(
                new WorkTimespanValue("6w 6d 8h 8min"));
    }

    @Test
    public void testTimespanMinusTimespanNotDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("8w 1d 3h 13min", "-", "1d 2h 9min")).isEqualsToByComparingFields(
                new WorkTimespanValue("8w 0d 1h 4min"));
    }

    @Test
    public void testWorkTimespanPlusRealTimespan() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("8w 1d 3h 13min", "+", "2H 7MIN")).isEqualsToByComparingFields(
                new WorkTimespanValue("8w 1d 5h 20min"));
    }

    @Test
    public void testRealTimespanMinusWorkTimespan() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("1W 7H", "-", "1h 1min")).isEqualsToByComparingFields(
                new RealTimespanValue("1W 5H 59MIN"));
    }

    @Test
    public void testTimespanMinusTimespanNotString() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("8w 1d 3h", "-", "8w 2d 4h 9min")).isEqualsToByComparingFields(
                new WorkTimespanValue("1d 1h 9min"));
    }

    @Test
    public void testDatePlusTimespan() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("12.09.2010 12:00", "+", "1D 2H 30MIN")).isEqualsToByComparingFields(
                new DateValue("13.09.2010 14:30"));
    }

    @Test
    public void testTimespanPlusDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("1D 3H 45MIN", "+", "19.05.2012 14:00")).isEqualsToByComparingFields(
                new DateValue("20.05.2012 17:45"));
    }

    @Test
    public void testDateMinusTimespan() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("12.09.2010 12:00", "-", "1D 2H 30MIN")).isEqualsToByComparingFields(
                new DateValue("11.09.2010 09:30"));
    }

    @Test
    public void testTimespanMinusDate() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("1D 2H 30MIN", "-", "12.09.2010 12:00")).isEqualsToByComparingFields(
                new DateValue("11.09.2010 09:30"));
    }

    @Test
    //This test causes an exception in arithmeticEvaluator.evaluate, but it is also caught there
    public void testIncompatibleTypeException() throws IncompatibleTypesException {
        assertThat(arithmeticEvaluatorTest("5.85", "+", "1d 5h 25min")).isEqualsToByComparingFields(
                new FloatValue("5.85"));

    }

    /**
     * This test creates an ASTAtom with three children(ASTLiteral,ASTOperator,ASTLiteral) and tests, if the term is
     * evaluated correctly.
     * 
     * @param leftValue value of the first ASTLiteral
     * @param operator value of ASTOperator
     * @param rightValue value of the second ASTLiteral
     * @throws IncompatibleTypesException
     */

    public GenericValue arithmeticEvaluatorTest(String leftValue, String operator, String rightValue)
            throws IncompatibleTypesException {
        ASTAtom atom = new ASTAtom(null, 789);
        ASTLiteral leftVal = new ASTLiteral(856);
        ASTArithmeticOperator op = new ASTArithmeticOperator(975);
        ASTLiteral rightVal = new ASTLiteral(78543);
        leftVal.setName(leftValue);
        rightVal.setName(rightValue);
        op.setName(operator);

        atom.jjtAddChild(rightVal, 2);
        atom.jjtAddChild(leftVal, 0);
        atom.jjtAddChild(op, 1);

        GenericValue value = arithmeticEvaluator.evaluate(atom);
        LOGGER.debug(value.toString());

        return value;
    }
}
