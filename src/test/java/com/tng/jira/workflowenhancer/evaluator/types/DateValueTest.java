package com.tng.jira.workflowenhancer.evaluator.types;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.types.DateValue;
import com.tng.jira.workflowenhancer.evaluator.types.RealTimespanValue;

public class DateValueTest extends TestCase {

    @Rule
    ExpectedException exception = ExpectedException.none();

    public void testConstructor() {

        DateValue value = new DateValue("10.10.2001");
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 0);
        Date date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testConstructor2() {
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 0);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date);
        assertTrue(date.equals(value.getValue()));
    }

    public void testConstructor3() {
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 1, 21);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        assertTrue(date.equals(value.getValue()));
    }

    public void testAdd() throws Exception {

        // testData
        int weeks = 1, days = 2, hours = 3, minutes = 4;

        // test
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 12);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        RealTimespanValue timespan = mockRealTimespan(weeks, days, hours, minutes);
        value = (DateValue) value.add(timespan);
        myDate = new GregorianCalendar(2001, 9, 10 + weeks * 7 + days, 12 + hours, 12 + minutes);
        date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testAdd2() throws Exception {
        // testData
        int weeks = 0, days = 0, hours = 0, minutes = 0;

        // test
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 12);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        RealTimespanValue timespan = mockRealTimespan(weeks, days, hours, minutes);
        value = (DateValue) value.add(timespan);
        myDate = new GregorianCalendar(2001, 9, 10 + weeks * 7 + days, 12 + hours, 12 + minutes);
        date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testAdd3() throws Exception {
        // testData
        int weeks = 20, days = 1, hours = 123, minutes = 2;

        // test
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 12);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        RealTimespanValue timespan = mockRealTimespan(weeks, days, hours, minutes);
        value = (DateValue) value.add(timespan);
        myDate = new GregorianCalendar(2001, 9, 10 + weeks * 7 + days, 12 + hours, 12 + minutes);
        date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testSubtract() throws Exception {
        // testData
        int weeks = 20, days = 1, hours = 123, minutes = 2;

        // test
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 12);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        RealTimespanValue timespan = mockRealTimespan(weeks, days, hours, minutes);
        value = (DateValue) value.subtract(timespan);
        myDate = new GregorianCalendar(2001, 9, 10 - weeks * 7 - days, 12 - hours, 12 - minutes);
        date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testSubstract2() throws Exception {
        // testData
        int weeks = 340, days = 1, hours = 123, minutes = 2;

        // test
        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 12);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);
        RealTimespanValue timespan = mockRealTimespan(weeks, days, hours, minutes);
        value = (DateValue) value.subtract(timespan);
        myDate = new GregorianCalendar(2001, 9, 10 - weeks * 7 - days, 12 - hours, 12 - minutes);
        date = myDate.getTime();
        assertTrue(date.equals(value.getValue()));
    }

    public void testSubstract3() {

        Calendar myDate = new GregorianCalendar(2001, 9, 10, 12, 10);
        Date date = myDate.getTime();
        DateValue value = new DateValue(date, true);

        myDate = new GregorianCalendar(2001, 8, 5, 10, 12);
        Date date2 = myDate.getTime();
        DateValue value2 = new DateValue(date2, true);

        RealTimespanValue ts = (RealTimespanValue) value.subtract(value2);
        assertTrue(ts.toString().equals("5W 0D 1H 58MIN"));

        ts = (RealTimespanValue) value2.subtract(value);
        assertTrue(ts.toString().equals("5W 0D 1H 58MIN"));
    }

    public void testDateValueCompare() {
    	DateValue date1 = new DateValue("01.01.2001 13:37");
    	DateValue date2 = new DateValue("01.01.2001");
    	DateValue date3 = new DateValue("02.02.2002 13:37");
    	DateValue date4 = new DateValue("02.02.2002");

    	assertTrue(date1.compareTo(date2) == 0);
    	assertTrue(date1.compareTo(date3) == -1);
    	assertTrue(date1.compareTo(date4) == -1);
    	assertTrue(date2.compareTo(date3) == -1);
    	assertTrue(date2.compareTo(date4) == -1);
    	assertTrue(date3.compareTo(date4) == 0);
    }

    private RealTimespanValue mockRealTimespan(int weeks, int days, int hours, int minutes) {
        RealTimespanValue f = mock(RealTimespanValue.class);

        while (minutes > 59) {
            minutes -= 60;
            hours++;
        }

        while (hours > 23) {
            hours -= 24;
            days++;
        }

        while (days > 6) {
            days -= 7;
            weeks++;
        }
        when(f.getWeeks()).thenReturn(weeks);
        when(f.getDays()).thenReturn(days);
        when(f.getHours()).thenReturn(hours);
        when(f.getMinutes()).thenReturn(minutes);
        return f;
    }
}
