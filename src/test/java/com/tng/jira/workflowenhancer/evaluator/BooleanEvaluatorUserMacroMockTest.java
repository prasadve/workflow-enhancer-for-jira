package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorUserMacroMockTest extends BooleanEvaluatorMockTests {

    /**
     * Tests if the booleanEvaluator works properly with the user-macro.
     * 
     * @param username The username of the current user
     * @param expression e.g. "[user]=\"hallerh\""
     * @return result true if and only if the expression is evaluated to true
     * @throws ParseException
     */
    public boolean forUserMacroWithCurrentUser(String username, String expression) throws ParseException {
        booleanEvaluator = new BooleanEvaluator(null, null, null, null, null, null, username, null, null);
        return booleanEvaluator.evaluate(expression);
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testEqualsTrue() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "[user]=\"hallerh\"")).isTrue();
    }

    @Test
    public void testEqualsFalse() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("samsag", "[user]=\"hallerh\"")).isFalse();
    }

    @Test
    public void testNotEqualsTrue() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("samsag", "[user]!=\"hallerh\"")).isTrue();
    }

    @Test
    public void testNotEqualsFalse() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "[user]!=\"hallerh\"")).isFalse();
    }

    @Test
    public void testConjunctionTrue() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "([user]==\"hallerh\") & ([user]!=\"muellerp\")")).isTrue();
    }

    @Test
    public void testConjunctionLeftValueFalse() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "([user]==\"samsag\") & ([user]!=\"muellerp\")")).isFalse();
    }

    @Test
    public void testConjunctionRightValueFalse() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "([user] == \"hallerh\") & ([user] != \"hallerh\")"))
                .isFalse();
    }

    @Test
    public void testDisjunctionTrueLeftValue() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "([user] == \"hallerh\") | ([user] != \"hallerh\")"))
                .isTrue();
    }

    @Test
    public void testDisjunctionTrueRightValue() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("muellerp", "([user] == \"hallerh\") | ([user] != \"hallerh\")"))
                .isTrue();
    }

    @Test
    public void testDisjunctionTrueBoth() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("hallerh", "([user] == \"hallerh\") | ([user] != \"muellerp\")"))
                .isTrue();
    }

    @Test
    public void testDusjunctionFalse() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("muellerp", "([user] == \"hallerh\") | ([user] != \"muellerp\")"))
                .isFalse();
    }

    @Test
    public void testArithmetic() throws ParseException {
        assertThat(forUserMacroWithCurrentUser("piet", "5+3=8")).isTrue();
    }

    @Test
    public void testParseException() throws ParseException {
        exception.expect(ParseException.class);
        assertThat(forUserMacroWithCurrentUser("Peter", "24.09.2002")).isFalse();
    }
}