package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorDescriptionFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{Description}" + orderOperator + expression_
     * 
     * @param fieldValue String value of the description field
     * @param orderOperator e.g. "=="
     * @param expression e.g. "\"test\""
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean descriptionFieldWithValue(String fieldValue, String orderOperator, String expression)
            throws ParseException {
        // Given:
        setReturnValuesForFieldIdAndField("Description", IssueFieldConstants.DESCRIPTION);
        when(issue.getDescription()).thenReturn(fieldValue);

        // When:
        boolean result = booleanEvaluator.evaluate("{Description}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testDescriptionEqualsSimpleString() throws Exception {
        assertThat(descriptionFieldWithValue("test", "==", "\"test\"")).isTrue();
    }

    @Test
    public void testDescriptionEqualsSimpleStringFalse() throws Exception {
        assertThat(descriptionFieldWithValue("other word", "==", "\"test\"")).isFalse();
    }

    @Test
    public void testDescriptionNotEqualsSimpleString() throws Exception {
        assertThat(descriptionFieldWithValue("other word", "!=", "\"test\"")).isTrue();
    }

    @Test
    public void testDescriptionEqualsString() throws Exception {
        assertThat(descriptionFieldWithValue("1234567890!??%<yy,.....+*#", "==", "\"1234567890!??%<yy,.....+*#\""))
                .isTrue();
    }

    @Test
    public void testDescriptionEqualsRegex() throws Exception {
        assertThat(descriptionFieldWithValue("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", "==", "/a*b/")).isTrue();
    }

    @Test
    public void testDescriptionEqualsRegex2() throws Exception {
        assertThat(descriptionFieldWithValue("abbbbbb", "=", "/a{1,2}b+c*/")).isTrue();
    }

    @Test
    public void testDescriptionEqualsRegex3() throws Exception {
        assertThat(descriptionFieldWithValue("abababa", "==", "/(ab)*a?/")).isTrue();
    }

}
