package com.tng.jira.workflowenhancer.evaluator.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.tng.jira.workflowenhancer.evaluator.parser.BooleanExpressionParser;
import com.tng.jira.workflowenhancer.evaluator.parser.Node;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

import junit.framework.TestCase;

public class ParserAcceptanceTest extends TestCase {

    public void testSimpleAcceptance() {
        ArrayList<String> testCases = new ArrayList<String>();

        testCases.add("01.01.2000<01.01.2001");
        testCases.add("{customfield_10000}={customfield_10000}");
        testCases.add("\"String\"!=\"OtherString\"");

        ArrayList<Node> testResults;
        try {
            testResults = getEvaluationResults(testCases);
            assertFalse(testResults.contains(null));
            assertTrue(testResults.size() == testCases.size());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testComplexAcceptance() {
        ArrayList<String> testCases = new ArrayList<String>();

        testCases.add("01.01.2000<01.01.2001&\"HelloWorld\">=\"HelloWord\"&{customfield_11111}={customfield_12345}");
        testCases.add("01.01.2000<01.01.2001|\"HelloWorld\">=\"HelloWord\"|{customfield_11111}={customfield_12345}");
        testCases.add("({customfield_10000}<={customfield_10000})|(\"String\"!=\"OtherString\"&01.01.2000<01.01.2001)");
        testCases.add("\"Foo\"!=\"Bar\"|!(01.01.2000<01.01.2001)");

        testCases.add("01.02.2009<13.02.2009-\"11d\"");
        testCases.add("13.04.2009+11d == 25.04.2009-1d");
        testCases.add("10.02.2000 == 10.02.2000 + 0w 0d 1h 3min");

        testCases.add("1+2=3");
        testCases.add("1+\"2\"==2");
        testCases.add("4-6=1");

        ArrayList<Node> testResults;
        try {
            testResults = getEvaluationResults(testCases);
            assertFalse(testResults.contains(null));
            assertTrue(testResults.size() == testCases.size());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testReject() {
        ArrayList<String> testCases = new ArrayList<String>();

        testCases.add("01.01.2000&01.01.2001");
        testCases.add("({customfield_10000}={customfield_10000})<\"String\"!=\"OtherString\"");
        testCases.add("18.10.1988");
        testCases
                .add("({customfield_10000}={customfield_10000})&({customfield_10000}={customfield_10000})|({customfield_10000}={customfield_10000})");
        testCases.add("(!(10.01.2000))<{customfield_10000}");
        testCases.add("10.02.2000 == 10.02.2000 + 3min 2h");
        testCases.add("10.03.2004+2h");

        testCases.add("1.0.2+3");
        testCases.add("1-2-3");
        try {
            assertTrue(checkThatTestsThrowExceptions(testCases));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private boolean checkThatTestsThrowExceptions(ArrayList<String> testCases) throws UnsupportedEncodingException {
        for (int i = 0; i < testCases.size(); i++) {
            InputStream in;

            try {
                in = new ByteArrayInputStream((testCases.get(i)).getBytes("UTF-8"));
                BooleanExpressionParser booleanExpressionParser = new BooleanExpressionParser(in);
                booleanExpressionParser.Start();
                fail(testCases.get(i) + " should have failed in the parser.");
            } catch (ParseException e) {
            }

        }

        return true;
    }

    private ArrayList<Node> getEvaluationResults(ArrayList<String> testCases) throws UnsupportedEncodingException,
            ParseException {
        ArrayList<Node> testResults = new ArrayList<Node>();

        for (int i = 0; i < testCases.size(); i++) {
            InputStream in;
            in = new ByteArrayInputStream((testCases.get(i)).getBytes("UTF-8"));
            BooleanExpressionParser booleanExpressionParser = new BooleanExpressionParser(in);

            Node node = booleanExpressionParser.Start();
            testResults.add(node);
        }
        return testResults;
    }

}
