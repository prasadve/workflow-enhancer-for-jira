package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorNowMacroMockTests extends BooleanEvaluatorMockTests {

    /**
     * tests, if the [now]-macro is evaluatated correctly by the booleanEvaluator.
     * 
     * @param orderOperator e.g. "=="
     * @param expression contains the operator and the right operand the left operand is [now]
     * @return the result of the evaluation
     * @throws ParseException
     */
    public boolean nowMacro(String orderOperator, String expression) throws ParseException {

        boolean result = booleanEvaluator.evaluate("[now]" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testNowNotEqual() throws Exception {
        assertThat(nowMacro("!=", "11.12.2010")).isTrue();
    }

    @Test
    public void testNowGreaterThan() throws Exception {
        assertThat(nowMacro(">", "11.11.2011")).isTrue();
    }

    @Test
    public void testNowGreaterOrEqualOnlyDate() throws Exception {
        assertThat(nowMacro(">=", "31.08.2012")).isTrue();
    }

    @Test
    public void testNowGreaterOrEqual() throws Exception {
        assertThat(nowMacro(">=", "31.08.2012 12:12")).isTrue();
    }

    @Test
    public void testNowSmallerOrEqual() throws Exception {
        assertThat(nowMacro("<=", "04.02.2099 12:55")).isTrue();
    }

    @Test
    public void testNowSmallerThan() throws Exception {
        assertThat(nowMacro("<", "31.12.2099 23:59")).isTrue();
    }

}
