package com.tng.jira.workflowenhancer.evaluator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;

public class BooleanEvaluatorMockTests {

    protected BooleanEvaluator booleanEvaluator;

    @Mock
    protected FieldManager fieldManager;

    @Mock
    protected Issue issue;

    @Mock
    protected Field field;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        new MockComponentWorker()
                .addMock(LabelManager.class, mock(LabelManager.class))
                .init();
        booleanEvaluator = new BooleanEvaluator(fieldManager, null, null, null, null, null, null, issue, null);
    }

    protected void setReturnValuesForFieldIdAndField(String fieldName, String fieldId) {
        when(field.getId()).thenReturn(fieldId);
        when(fieldManager.getField(fieldName)).thenReturn(field);
    }
}
