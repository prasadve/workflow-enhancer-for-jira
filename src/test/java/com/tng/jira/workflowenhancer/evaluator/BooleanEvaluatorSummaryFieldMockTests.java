package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorSummaryFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{Summary}" + orderOperator + expression_
     * 
     * @param fieldValue value of the field
     * @param orderOperator e.g. "=="
     * @param expression e.g. "\"A nonsense text!!!\""
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean summaryFieldWithValue(String fieldValue, String orderOperator, String expression)
            throws ParseException {
        // Given:
        setReturnValuesForFieldIdAndField("Summary", IssueFieldConstants.SUMMARY);
        when(issue.getSummary()).thenReturn(fieldValue);

        // When:
        boolean result = booleanEvaluator.evaluate("{Summary}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testSummaryEqualsSimpleString() throws Exception {
        assertThat(summaryFieldWithValue("A nonsense text!!!", "=", "\"A nonsense text!!!\"")).isTrue();
    }

    @Test
    public void testSummaryNotEmpty() throws Exception {
        assertThat(summaryFieldWithValue("text", "!=", "\"\"")).isTrue();
    }

    @Test
    public void testSummaryEqualsString() throws Exception {
        assertThat(summaryFieldWithValue("1234567890!??%<yy,.....+*#", "==", "\"1234567890!??%<yy,.....+*#\""))
                .isTrue();
    }

    @Test
    public void testSummaryEqualsRegex() throws Exception {
        assertThat(summaryFieldWithValue("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab", "==", "/a*b/")).isTrue();
    }

    @Test
    public void testSummaryEqualsRegex2() throws Exception {
        assertThat(summaryFieldWithValue("abbbbbb", "=", "/a{1,2}b+c*/")).isTrue();
    }

    @Test
    public void testSummaryEqualsRegex3() throws Exception {
        assertThat(summaryFieldWithValue("abababa", "==", "/(ab)*a?/")).isTrue();
    }

    @Test
    public void testSummaryEqualsRegex4() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "==", " /.*TNG.*/")).isTrue();
    }

    @Test
    public void testSummaryEqualsRegexFalse() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "==", " /.*ATU.*/")).isFalse();
    }

    @Test
    public void testSummaryNotEqualsRegex() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "!=", "/.*ATU.*/")).isTrue();
    }

    @Test
    public void testSummaryNotEqualsRegexFalse() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "!=", "/.*TNG.*/")).isFalse();
    }

    @Test
    public void testSummaryNotEqualsRegexString() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "!=", " \".*TNG.*\"")).isTrue();
    }
    
    @Test
    public void testSummaryEqualsRegexAsStringFalse() throws Exception {
        assertThat(summaryFieldWithValue("Text der TNG enthält", "=", "\"/.*TNG.*/\"")).isFalse();
    }
}