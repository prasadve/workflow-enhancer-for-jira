package com.tng.jira.workflowenhancer.evaluator;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;
import com.tng.jira.workflowenhancer.evaluator.types.AbstractTimespanValue;
import com.tng.jira.workflowenhancer.evaluator.types.WorkTimespanValue;

public class BooleanEvaluatorTimeEstimateFieldMockTests extends BooleanEvaluatorMockTests {

    /**
     * Mock Test for _"{TIME_ESTIMATE}" + orderOperator + expression_
     * 
     * @param fieldValue TimespanValue of the time estimate field
     * @param orderOperator e.g. "="
     * @param expression e.g. "4w 4d 12h"
     * @return true if and only if expression is evaluated to true
     * @throws ParseException
     */
    public boolean timeEstimateOn(AbstractTimespanValue fieldValue, String orderOperator, String expression)
            throws ParseException {
        // Given:
        Long valueInSec = fieldValue.getInSeconds();
        setReturnValuesForFieldIdAndField("TIME_ESTIMATE", IssueFieldConstants.TIME_ESTIMATE);
        when(issue.getEstimate()).thenReturn(valueInSec);

        // When:
        boolean result = booleanEvaluator.evaluate("{TIME_ESTIMATE}" + orderOperator + expression);

        return result;
    }

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testTimeEstimateEquals() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 12, 2);
        assertThat(timeEstimateOn(fieldValue, "==", "5w 4d 12h 2min")).isTrue();
    }

    @Test
    public void testTimeEstimateNotEquals() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 12, 3);
        assertThat(timeEstimateOn(fieldValue, "!=", "5w 4d 12h 2min")).isTrue();
    }

    @Test
    public void testTimeEstimateSmallerOrEqual() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 7, 60);
        assertThat(timeEstimateOn(fieldValue, "<=", "5w 5d")).isTrue();
    }

    @Test
    public void testTimeEstimateSmallerOrEqual2() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 12, 2);
        assertThat(timeEstimateOn(fieldValue, "<=", "5w 4d 12h 2min")).isTrue();
    }

    @Test
    public void testTimeEstimateGreaterOrEqual() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(0, 0, 17, 5);
        assertThat(timeEstimateOn(fieldValue, ">=", "1h")).isTrue();
    }

    @Test
    public void testTimeEstimateGreaterOrEqual2() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 12, 2);
        assertThat(timeEstimateOn(fieldValue, ">=", "5w 4d 12h 2min")).isTrue();
    }

    @Test
    public void testTimeEstimateGreater() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(0, 0, 17, 5);
        assertThat(timeEstimateOn(fieldValue, ">", "1h")).isTrue();
    }

    @Test
    public void testTimeEstimateGreater2() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(9, 0, 17, 5);
        assertThat(timeEstimateOn(fieldValue, ">", "1w 1min")).isTrue();
    }

    @Test
    public void testTimeEstimateSmaller() throws Exception {
        WorkTimespanValue fieldValue = new WorkTimespanValue(5, 4, 7, 5);
        assertThat(timeEstimateOn(fieldValue, "<", "5w 5d")).isTrue();
    }
}
