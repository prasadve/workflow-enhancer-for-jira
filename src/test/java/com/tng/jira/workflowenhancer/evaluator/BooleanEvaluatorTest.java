package com.tng.jira.workflowenhancer.evaluator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;

public class BooleanEvaluatorTest {

    private BooleanEvaluator booleanEvaluator;

    @Before
    public void setup() {
        new MockComponentWorker()
                .addMock(LabelManager.class, mock(LabelManager.class))
                .init();
        booleanEvaluator = new BooleanEvaluator(null, null, null, null, null, null, null, null, null);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testSimpleExpressions() throws ParseException {

        assertTrue(booleanEvaluator.evaluate("10.01.2002<30.12.2003"));
        assertTrue(booleanEvaluator.evaluate("\"Foo\"!=\"Bar\""));

        assertTrue(booleanEvaluator.evaluate("\"10.01.2000\"=10.01.2000"));

        assertFalse(booleanEvaluator.evaluate("10.02.2000=01.01.1999"));
        assertFalse(booleanEvaluator.evaluate("01.03.2009<=01.01.2003"));

        assertTrue(booleanEvaluator.evaluate("1h<=2d"));
        assertTrue(booleanEvaluator.evaluate("1w<=2w"));

        assertTrue(booleanEvaluator.evaluate("\" \\\\' $!'\"=/.*/")); // Special characters
    }

    @Test
    public void testArithmeticExpressions() throws ParseException {

        assertTrue(booleanEvaluator.evaluate("19.12.1989 == 19.12.1989 + 3H 5MIN"));
        assertTrue(booleanEvaluator.evaluate("19.12.1989 != 19.12.1989 + 24H"));
        assertTrue(booleanEvaluator.evaluate("18.12.1989 == 17.12.1989 + 1D"));
        assertTrue(booleanEvaluator.evaluate("18.12.1989 == 17.12.1989 + 24H"));

        assertTrue(booleanEvaluator.evaluate("19.12.1989 == 18.12.1989 + 24H 2MIN"));
        assertFalse(booleanEvaluator.evaluate("19.12.1989 > 18.12.1989 + 2D"));

        assertTrue(booleanEvaluator.evaluate("19.12.1989 12:00 = 19.12.1989 13:00 - 1H"));

        assertTrue(booleanEvaluator.evaluate("19.12.1989 13:45 == 19.12.1989 12:00 + 1H45MIN"));
        assertTrue(booleanEvaluator.evaluate("19.12.1989 12:00 < 19.12.1989 11:00 + 120MIN"));

        assertTrue(booleanEvaluator.evaluate("1h 3min == 1h 3min"));
        assertTrue(booleanEvaluator.evaluate("1D == 23H 60MIN"));
        assertTrue(booleanEvaluator.evaluate("1H 30MIN == 90MIN"));

        assertTrue(booleanEvaluator.evaluate("\"Hello\" + \"World\" = \"HelloWorld\""));
        assertTrue(booleanEvaluator.evaluate("\"An\" + \"na\" == \"Anna\""));
    }

    @Test
    public void testStringValueOrderOperations() throws ParseException {
        assertTrue(booleanEvaluator.evaluate("\"test\" = \"test\""));
        assertTrue(booleanEvaluator.evaluate("\"zoo\" >= \"a much longer string but with an a at the beginning.\""));
        assertTrue(booleanEvaluator.evaluate("\"zoo\" >= \"zoo\""));
        assertTrue(booleanEvaluator.evaluate("\"zoo\" != \"ooz\""));
        assertTrue(booleanEvaluator.evaluate("\"a test string\" <= \"a test string b\""));
        assertTrue(booleanEvaluator.evaluate("\"2 test strings\" <= \"a test string\""));
        assertTrue(booleanEvaluator.evaluate("\"256949.223\" <= \"a\""));
    }

    @Test
    public void testDateOperations() throws ParseException {

        assertTrue(booleanEvaluator.evaluate("[now] + 5000W > 01.01.2050"));
        assertFalse(booleanEvaluator.evaluate("[now] + 500W > 01.01.2080"));
        assertFalse(booleanEvaluator.evaluate("[now] + 5000W < 01.01.2050"));
        assertTrue(booleanEvaluator.evaluate("[now] + 5W < 01.01.2080"));

        // The following two tests show that it doesn't matter if you change the order of the date arguments.
        assertTrue(booleanEvaluator.evaluate("10.12.2000 - 8.12.2000 = 2D"));
        assertTrue(booleanEvaluator.evaluate("8.12.2000 - 10.12.2000 = 2D"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 - 27.08.2009 18:33 = 7D"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 - 27.08.2009 18:33 < 8D"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 - 27.08.2009 18:33 < 7D 1MIN"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 - 27.08.2009 18:33 > 6D"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:33 - 27.08.2009 18:33 = 7D"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:33 - 27.08.2009 18:33 < 21D"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:33 - 27.08.2009 18:33 < 8D"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:13 - 27.08.2009 18:23 = 7D 0H 10MIN"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:13 - 27.08.2009 18:23 > 7D 0H 0MIN"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:13 - 27.08.2009 18:23 >= 6D 23H 50MIN"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:23 - 27.08.2009 18:13 = 6D 23H 50MIN"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:23 - 27.08.2009 18:13 < 6D 23H 59MIN"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:23 - 27.08.2009 18:13 >= 6D 23H 45MIN"));

        assertTrue(booleanEvaluator.evaluate("27.08.2009 18:13 - 6D 23H 50MIN = 20.08.2009 18:23"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:23 + 6D 23H 50MIN = 27.08.2009 18:13"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 18:23 + 7D = 27.08.2009 18:23"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 7D 1H 1MIN = 27.08.2009"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 6D 25H 1MIN = 27.08.2009"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 01:00 + 23H = 21.08.2009"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 01:00 + 1W 1D 23H = 29.08.2009"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 01:00 + 1W 1D 1380MIN = 29.08.2009"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 1W 1D 1440MIN = 29.08.2009"));

        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 23H = 20.08.2009 12:00"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 24H = 21.08.2009"));
        assertTrue(booleanEvaluator.evaluate("20.08.2009 + 24H = 21.08.2009 12:00"));
    }

    @Test
    public void testComplexExpressions() throws ParseException {
        assertTrue(booleanEvaluator.evaluate("10.02.2000==01.01.1999|10.01.1990<30.12.2010"));
        assertFalse(booleanEvaluator.evaluate("10.02.2000==01.01.1999&10.01.1990<30.12.2010"));

        assertTrue(booleanEvaluator.evaluate("(10.01.2002<30.12.2003)"));
        assertFalse(booleanEvaluator.evaluate("!(10.01.2002<30.12.2003)"));
        
        assertTrue(booleanEvaluator
                .evaluate("(10.02.2000=01.01.1999 AND 10.01.1990<30.12.2010)||(10.02.2000=01.01.1999 OR 10.01.1990<30.12.2010)"));
        assertFalse(booleanEvaluator
                .evaluate("(10.02.2000=01.01.1999 AND 10.01.1990<30.12.2010)&&(\"Foo\"!=\"Bar\" OR 10.01.1990<30.12.2010)"));

        assertTrue(booleanEvaluator
                .evaluate("(!10.02.2000=01.01.1999 AND 10.01.1990<30.12.2010)||!(10.02.2000=01.01.1999 OR !10.01.1990<30.12.2010)"));

        assertFalse(booleanEvaluator.evaluate("10.10.2030>[now]+1d && (100<100 || 1001>1000) && \"a\" != \"a\""));
        assertTrue(booleanEvaluator.evaluate("((10.02.2000!=[now])&&(10.01.1990<30.12.2010))||((1=1) AND (1=2))"));
    }

    @Test
    public void testRegExComparisonsWithRegExInFirstPosition() throws ParseException {
        // RegEx == String
        assertTrue(booleanEvaluator.evaluate("5==5"));
        assertTrue(booleanEvaluator.evaluate("/[0-9]*d/=\"2d\""));
        assertTrue(booleanEvaluator.evaluate(" /a*b/ == \"aaab\" "));
        assertTrue(booleanEvaluator.evaluate("/a?a?b/==\"aab\""));
        assertTrue(booleanEvaluator.evaluate("/a?b?c?d?/==\"abc\""));
        assertTrue(booleanEvaluator.evaluate("/((ac)+b?)*/==\"acacbacac\""));
        assertTrue(booleanEvaluator.evaluate("/a{1,2}b+c*/==\"aabbbbbbbbbb\""));
        assertTrue(booleanEvaluator.evaluate("/[0-9]*d/=\"2d\""));
    }

    @Test
    public void testRegExComparisonsWithRegExInSecondPosition() throws ParseException {
        assertTrue(booleanEvaluator.evaluate("\"abc\"==/a?b?c?d?/"));
        assertTrue(booleanEvaluator.evaluate(" \"aaab\" = /a*b/ "));
        assertTrue(booleanEvaluator.evaluate("\"ababa\"==/(ab)*a?/"));
    }

    @Test
    public void testFloatFunctionality() throws ParseException {
        assertTrue(booleanEvaluator.evaluate("1+1=2"));
        assertTrue(booleanEvaluator.evaluate("1+1=\"2.0\""));
        assertFalse(booleanEvaluator.evaluate("1+1=3"));
        assertTrue(booleanEvaluator.evaluate("1 - 1 = 0.0"));
        assertTrue(booleanEvaluator.evaluate("1 - 1 = .0"));
        assertTrue(booleanEvaluator.evaluate("2 - 1 = 1"));
        assertTrue(booleanEvaluator.evaluate("423.225-120.125==303.1"));

        assertTrue(booleanEvaluator.evaluate("1<2.0"));
        assertTrue(booleanEvaluator.evaluate("2>1"));
        assertFalse(booleanEvaluator.evaluate("2>=4"));
        assertTrue(booleanEvaluator.evaluate("1.5>1.29"));

        assertTrue(booleanEvaluator.evaluate("13.245<11+3"));
    }

    @Test
    public void testEquals() throws ParseException {
        assertTrue(booleanEvaluator.evaluate("1.234=1.234"));
        assertFalse(booleanEvaluator.evaluate("1.234=4.321"));
        assertFalse(booleanEvaluator.evaluate("1.234!=1.234"));
        assertTrue(booleanEvaluator.evaluate("123.4=123.4"));
        assertFalse(booleanEvaluator.evaluate("123.4!=123.4"));

        assertTrue(booleanEvaluator.evaluate("01.01.2001=01.01.2001 12:12"));
    }
}
