package com.tng.jira.workflowenhancer.evaluator.types;

import static org.fest.assertions.api.Assertions.assertThat;
import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;
import com.tng.jira.workflowenhancer.evaluator.types.RealTimespanValue;

public class RealTimespanValueTest extends TestCase {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testConstructor() {
        assertThat(new RealTimespanValue("1W 2D 3H 4MIN").toString()).isEqualTo("1W 2D 3H 4MIN");
        assertThat(new RealTimespanValue("1W2D3H4MIN").toString()).isEqualTo("1W 2D 3H 4MIN");
    }

    @Test
    public void testConstructor2() {
        assertThat(new RealTimespanValue(1, 2, 3, 44).toString()).isEqualTo("1W 2D 3H 44MIN");
        assertThat(new RealTimespanValue(-1, 6, 0, 0).toString()).isEqualTo("0W 1D 0H 0MIN");
        assertThat(new RealTimespanValue(-1, 7, 0, 0).toString()).isEqualTo("0W 0D 0H 0MIN");
        assertThat(new RealTimespanValue(-1, 6, 25, -60).toString()).isEqualTo("0W 0D 0H 0MIN");
    }

    @Test
    public void testConstructor3() {
        assertThat(new RealTimespanValue(-1).toString()).isEqualTo("0W 0D 0H 1MIN");
        assertThat(new RealTimespanValue(1).toString()).isEqualTo("0W 0D 0H 1MIN");
        assertThat(new RealTimespanValue(37988).toString()).isEqualTo("3W 5D 9H 8MIN");
    }

    @Test
    public void testGets() {
        assertThat(new RealTimespanValue("1W 2D 3H 44MIN").getInMinutes()).isEqualTo(13184);
        assertThat(new RealTimespanValue(37988).getWeeks()).isEqualTo(3);
        assertThat(new RealTimespanValue(37988).getDays()).isEqualTo(5);
        assertThat(new RealTimespanValue(37988).getHours()).isEqualTo(9);
        assertThat(new RealTimespanValue(37988).getMinutes()).isEqualTo(8);
        assertThat(new RealTimespanValue(37988).getInMinutes()).isEqualTo(37988);
    }

    @Test
    public void testAdd() throws IncompatibleTypesException {
        RealTimespanValue span1 = new RealTimespanValue("0W0D0H99MIN");
        RealTimespanValue span2 = new RealTimespanValue("0W0D0H1MIN");
        assertThat(span1.add(span2).toString()).isEqualTo("0W 0D 1H 40MIN");

        span1 = new RealTimespanValue("-80");
        span2 = new RealTimespanValue("80");
        assertThat(span1.add(span2).toString()).isEqualTo("0W 0D 0H 0MIN");
    }

    @Test
    public void testSubtract() throws IncompatibleTypesException {
        RealTimespanValue span1 = new RealTimespanValue("1W 2D 5H 15MIN");
        RealTimespanValue span2 = new RealTimespanValue("1W 1D 1H 1MIN");
        assertThat(span1.subtract(span2).toString()).isEqualTo("0W 1D 4H 14MIN");

    }

}
