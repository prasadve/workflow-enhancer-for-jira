package com.tng.jira.workflowenhancer.evaluator;

import junit.framework.TestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.tng.jira.workflowenhancer.evaluator.TermEvaluator;
import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;

public class TermEvaluatorTest extends TestCase {

    TermEvaluator termEvaluator = new TermEvaluator();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testStringComparison() throws IncompatibleTypesException {
        assertTrue(termEvaluator.evaluate("\"Hello World\"", "=", "\"Hello World\""));
        assertFalse(termEvaluator.evaluate("\"Hello World\"", "!=", "\"Hello World\""));
        assertFalse(termEvaluator.evaluate("\"Bar\"", "=", "\"Foo\""));
        assertTrue(termEvaluator.evaluate("\"Bar\"", "!=", "\"Foo\""));
    }

    @Test
    public void testDataComparisonWithoutTime() throws IncompatibleTypesException {
        assertTrue(termEvaluator.evaluate("10.10.2000", "=", "10.10.2000"));
        assertFalse(termEvaluator.evaluate("10.10.2000", "!=", "10.10.2000"));
        assertFalse(termEvaluator.evaluate("10.10.2000", "=", "11.10.2000"));
        assertTrue(termEvaluator.evaluate("10.10.2000", "!=", "11.10.2000"));

        assertTrue(termEvaluator.evaluate("10.10.2000", "<=", "10.10.2000"));
        assertTrue(termEvaluator.evaluate("10.10.2000", ">=", "10.10.2000"));

        assertTrue(termEvaluator.evaluate("12.03.1999", "<", "11.10.2000"));
        assertTrue(termEvaluator.evaluate("12.03.1999", "<=", "11.01.2010"));
        assertTrue(termEvaluator.evaluate("12.03.1999", ">", "11.10.1988"));
        assertTrue(termEvaluator.evaluate("12.03.1999", ">=", "11.10.1988"));

        assertFalse(termEvaluator.evaluate("12.03.1999", ">", "11.10.2000"));
        assertFalse(termEvaluator.evaluate("12.03.1999", ">=", "11.01.2010"));
        assertFalse(termEvaluator.evaluate("12.03.1999", "<", "11.10.1988"));
        assertFalse(termEvaluator.evaluate("12.03.1999", "<=", "11.10.1988"));
    }

    @Test
    public void testDataComparisonWithTime() throws IncompatibleTypesException {
        assertTrue(termEvaluator.evaluate("01.01.2001 12:30", "<", "01.01.2001 12:31"));
        assertFalse(termEvaluator.evaluate("01.01.2001 12:30", ">", "01.01.2001 15:29"));
        assertTrue(termEvaluator.evaluate("18.10.1988 10:31", "=", "18.10.1988 10:31"));
        assertFalse(termEvaluator.evaluate("01.01.2001 13:49", ">", "01.01.2001"));
        assertTrue(termEvaluator.evaluate("01.01.2001 13:49", "=", "01.01.2001"));
        assertFalse(termEvaluator.evaluate("01.01.2001 23:33", "=", "01.01.2001 23:34"));
    }

    @Test
    public void testSimpleTimespanExpressions() throws IncompatibleTypesException {
        assertTrue(termEvaluator.evaluate("1h", "=", "1h"));
        assertFalse(termEvaluator.evaluate("5w", "=", "1d"));
        assertTrue(termEvaluator.evaluate("5w", "!=", "1d"));
    }

    @Test
    public void testRegExComparisons() throws IncompatibleTypesException {
        // (String, RegEx)
        assertTrue(termEvaluator.evaluate("\"Bar\"", "=", "/Ba[a,r]/"));
        assertTrue(termEvaluator.evaluate("\"Baar\"", "!=", "/Ba[a,r]/"));

        // String comparison for the operators < and > (and <=, >= in case of inequality)
        assertTrue(termEvaluator.evaluate("\"Bar\"", "<=", "/Ba[a,r]/"));
        assertFalse(termEvaluator.evaluate("\"Bar\"", "<", "/Ba[a,r]/"));
        assertTrue(termEvaluator.evaluate("\"A string\"", "<", "/Ba[a,r]/"));
        assertTrue(termEvaluator.evaluate("\"b string\"", ">", "/a[a,r]/"));

        // (RegEx, String)
        assertTrue(termEvaluator.evaluate("/Ba[a,r]/", "=", "\"Baa\""));

        // String comparison for the operators < and > (and <=, >= in case of inequality)
        assertTrue(termEvaluator.evaluate("/Ba[a,r]/", "<", "\"Z\""));
        assertTrue(termEvaluator.evaluate("/Ba[a,r]/", ">", "\"A\""));
    }

    @Test
    public void testdefaultCaseOrderOperations() throws IncompatibleTypesException {
        // Date, String
        assertTrue(termEvaluator.evaluate("10.10.2000", "=", "\"10.10.2000\""));
        assertTrue(termEvaluator.evaluate("\"10.10.2000\"", "=", "10.10.2000"));
        assertTrue(termEvaluator.evaluate("10.10.2000", "<", "\"a string\""));
        assertTrue(termEvaluator.evaluate("10.10.2000", ">", "\"0 a string with a zero at the beginning.\""));

        // Float, String
        assertTrue(termEvaluator.evaluate("42.0", "=", "\"42.0\""));
        assertTrue(termEvaluator.evaluate("\"42.0\"", "=", "42.0"));
        assertTrue(termEvaluator.evaluate("42", "=", "\"42.0\""));
        assertTrue(termEvaluator.evaluate("42", "<", "\"42.0 bla bla\""));
        assertTrue(termEvaluator.evaluate("\"0 a string with a zero at the beginning\"", "<", "42"));

        // Timespan, String
        assertTrue(termEvaluator.evaluate("1w", "==", "\"1w 0d 0h 0min\""));
        assertTrue(termEvaluator.evaluate("\"1w 0d 0h 0min\"", "==", "1w"));
        assertTrue(termEvaluator.evaluate("\"1w 0d 0h 0min bla bla\"", ">", "1w"));
        assertTrue(termEvaluator.evaluate("1w", ">=", "\"1w 0d 0h 0min\""));
    }

    @Test
    public void testNowMacro() throws IncompatibleTypesException {
        assertTrue(termEvaluator.evaluate("[now]", ">", "01.01.2000"));
        assertTrue(termEvaluator.evaluate("[now]", "<", "01.01.2099"));
    }
}
