package com.tng.jira.workflowenhancer.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.mock.component.MockComponentWorker;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.tng.jira.workflowenhancer.util.FieldProvider;

public class FieldProviderTest {

    private static final String PROVIDED_FIELD_NAME = IssueFieldConstants.TIME_ESTIMATE;
    private static final String CUSTOM_FIELD_NAME = "customfield";

    private static final long TIME_ESTIMATE_RETURN_VALUE = 1234L;
    private static final Object CUSTOM_FIELD_RETURN_VALUE = "test";

    private FieldProvider fieldProvider;

    private Field providedField;
    private CustomField customField;
    private Issue issue;

    @Before
    public void buildUp() {
        providedField = mockField(PROVIDED_FIELD_NAME);
        customField = (CustomField) mockField(CUSTOM_FIELD_NAME);
        issue = mockIssue();
        new MockComponentWorker()
                .addMock(LabelManager.class, mock(LabelManager.class))
                .init();
        fieldProvider = new FieldProvider(mockFieldManager(), mockCustomFieldManager(), null, issue, null);
    }

    private FieldManager mockFieldManager() {
        FieldManager mock = mock(FieldManager.class);
        when(mock.getField(PROVIDED_FIELD_NAME)).thenReturn(providedField);

        when(mock.isCustomField(providedField)).thenReturn(false);
        when(mock.isCustomField(customField)).thenReturn(true);
        return mock;
    }

    private CustomFieldManager mockCustomFieldManager() {
        CustomFieldManager mock = mock(CustomFieldManager.class);
        when(mock.getCustomFieldObjectByName(CUSTOM_FIELD_NAME)).thenReturn(customField);
        return mock;
    }

    private Field mockField(String fieldId) {
        if (fieldId.equals(CUSTOM_FIELD_NAME)) {
            CustomField mock = mock(CustomField.class);
            when(mock.getId()).thenReturn(CUSTOM_FIELD_NAME);
            return mock;
        }
        Field mock = mock(Field.class);
        when(mock.getId()).thenReturn(PROVIDED_FIELD_NAME);
        return mock;
    }

    private Issue mockIssue() {
        Issue mock = mock(Issue.class);

        when(mock.getCustomFieldValue(any(CustomField.class))).thenReturn(CUSTOM_FIELD_RETURN_VALUE);
        when(mock.getEstimate()).thenReturn(TIME_ESTIMATE_RETURN_VALUE);

        return mock;
    }

    @Test
    public void testProvidedField() {
        Object fieldReturnValue = fieldProvider.getFieldValueById(PROVIDED_FIELD_NAME);
        assertEquals(TIME_ESTIMATE_RETURN_VALUE, fieldReturnValue);
    }

    @Test
    public void testCustomField() {
        Object fieldReturnValue = fieldProvider.getFieldValueById(CUSTOM_FIELD_NAME);
        verify(issue).getCustomFieldValue(customField);
        assertEquals(CUSTOM_FIELD_RETURN_VALUE, fieldReturnValue);
    }
}
