package com.tng.jira.workflowenhancer.util;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import junit.framework.TestCase;

import com.atlassian.jira.util.I18nHelper;
import com.tng.jira.workflowenhancer.util.PreventiveSyntaxCheck;

public class PreventiveSyntaxCheckTest extends TestCase {

    public void testNothingToDo() {
        assertTrue(PreventiveSyntaxCheck.check("1=1", mockHelper("ERROR")) == null);
        assertTrue(PreventiveSyntaxCheck.check("", mockHelper("ERROR")) != null);
    }

    public void testBrackets() {
        assertTrue(PreventiveSyntaxCheck.check("1=1]", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("[1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("]1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1=[1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1=[[1]]", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1]=[1", mockHelper("ERROR")) != null);
    }

    public void testBraces() {
        assertTrue(PreventiveSyntaxCheck.check("1=1}", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("{1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("}1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1=}1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1={{1}}", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1}={1", mockHelper("ERROR")) != null);
    }

    public void testSlashes() {
        assertTrue(PreventiveSyntaxCheck.check("1=/1/", mockHelper("ERROR")) == null);
        assertTrue(PreventiveSyntaxCheck.check("1=/1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("/1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1=1/", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("/1/=/1", mockHelper("ERROR")) != null);
    }

    public void testQuotes() {
        assertTrue(PreventiveSyntaxCheck.check("1=\"1\"", mockHelper("ERROR")) == null);
        assertTrue(PreventiveSyntaxCheck.check("1=\"1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("\"1=1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1=1\"", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("\"1\"=\"1", mockHelper("ERROR")) != null);
    }

    public void testOperators() {
        assertTrue(PreventiveSyntaxCheck.check("1<1", mockHelper("ERROR")) == null);
        assertTrue(PreventiveSyntaxCheck.check("1>1", mockHelper("ERROR")) == null);
        assertTrue(PreventiveSyntaxCheck.check("1||1", mockHelper("ERROR")) != null);
        assertTrue(PreventiveSyntaxCheck.check("1", mockHelper("ERROR")) != null);
    }

    private I18nHelper mockHelper(String Error) {
        I18nHelper i18n = mock(I18nHelper.class);
        when(i18n.getText(anyString())).thenReturn(Error);
        return i18n;
    }
}
