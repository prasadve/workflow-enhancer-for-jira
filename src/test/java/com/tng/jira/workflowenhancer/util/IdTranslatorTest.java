package com.tng.jira.workflowenhancer.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import com.atlassian.jira.issue.fields.Field;
import com.tng.jira.workflowenhancer.util.IdTranslator;

public class IdTranslatorTest extends TestCase {
    private IdTranslator forwardTranslator = new IdTranslator(generateList(), false);
    private IdTranslator reverseTranslator = new IdTranslator(generateList(), true);

    @Test
    public void testSplitAndTranslate() {
		assertEquals(Arrays.asList("{TEST1}", "{{TEST2}}", "test", "{}", "{TEST3}"),
				forwardTranslator.splitAndTranslate("{test1}{{test2}}test{}{test3}"));
		assertEquals(Arrays.asList("{E}"),
				forwardTranslator.splitAndTranslate("{123}"));
    }

    @Test
    public void testDontTranslate() {
		assertEquals(Arrays.asList("[123]"),forwardTranslator.splitAndTranslate("[123]"));
		assertEquals(Arrays.asList("/123/"), forwardTranslator.splitAndTranslate("/123/"));
		assertEquals(Arrays.asList("\"123\""), forwardTranslator.splitAndTranslate("\"123\""));
		assertEquals(Arrays.asList("\"{123}\""), forwardTranslator.splitAndTranslate("\"{123}\""));
    }

    @Test
    public void testTranslateExpression() {
        assertEquals(forwardTranslator.translateExpression("{test1}{{test2}}test{}{test3}"),
                "{TEST1}{{TEST2}}test{}{TEST3}");
        assertEquals(forwardTranslator.translateExpression("{TEST1}/test1/{{test3}}123"),
        		"{TEST1}/test1/{{TEST3}}123");
    }

    @Test
    public void testDontTranslateExpression() {
        assertEquals(forwardTranslator.translateExpression("a}TEST1{b"), "a}TEST1{b");
        assertEquals(forwardTranslator.translateExpression("no brackets"), "no brackets");
    }

    @Test
    public void testTranslateExpressionReversed() {
        assertEquals(reverseTranslator.translateExpression("{test1}{TEST2}test{}{{test3}}"),
                "{test1}{test2}test{}{{test3}}");
    }

    @Test
    public void testTranslateFieldExpression() {
    	assertEquals(forwardTranslator.translateSubExpressionIfNecessary("{{test1}}"), "{{TEST1}}");
    	assertEquals(forwardTranslator.translateSubExpressionIfNecessary("{test2}"), "{TEST2}");
    	assertEquals(forwardTranslator.translateSubExpressionIfNecessary("{{test4}}"), "{{test4}}");
    	assertEquals(reverseTranslator.translateSubExpressionIfNecessary("{{test1}}"), "{{test1}}");
    	assertEquals(reverseTranslator.translateSubExpressionIfNecessary("{E}"), "{123}");
    	assertEquals(reverseTranslator.translateSubExpressionIfNecessary("[subtasks#E]"), "[subtasks#123]");
    	assertEquals(reverseTranslator.translateSubExpressionIfNecessary("[subtasks#{E}]"), "[subtasks#{E}]");
    	assertEquals(forwardTranslator.translateSubExpressionIfNecessary("[subtasks#test1]"), "[subtasks#TEST1]");
    }

    @Test
    public void testIsFieldExpression() {
    	assertTrue(IdTranslator.isFieldExpression("{{sa/das{}ds/d}}"));
    	assertTrue(IdTranslator.isFieldExpression("{test1}"));
    	assertFalse(IdTranslator.isFieldExpression("{{sadas{}dsd}"));
    	assertFalse(IdTranslator.isFieldExpression("{sadas{}dsd}/"));
    	assertFalse(IdTranslator.isFieldExpression("\"sadas{}dsd\""));
    }

    @Test
    public void testStripExpression() {
    	assertEquals(IdTranslator.getStrippedExpression("/sa\"d{}[]sd/"), "sa\"d{}[]sd");
    	assertEquals(IdTranslator.getStrippedExpression("{sometext}"), "sometext");
    	assertEquals(IdTranslator.getStrippedExpression("{{sometext}}"), "sometext");
    }

    private List<Field> generateList() {
        List<Field> list = new ArrayList<Field>();

        list.add(mockField("test1", "TEST1"));
        list.add(mockField("test2", "TEST2"));
        list.add(mockField("test3", "TEST3"));
        list.add(mockField("123", "E"));

        return list;
    }

    private Field mockField(String Name, String Id) {
        Field f = mock(Field.class);
        when(f.getId()).thenReturn(Id);
        when(f.getName()).thenReturn(Name);
        return f;
    }
}
