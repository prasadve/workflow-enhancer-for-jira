package com.tng.jira.workflowenhancer.util;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ExpressionSplitterTest {

	@Test
    public void testSplit() {
		assertEquals(Arrays.asList("{test1}", "{test2}", "test", "{}", "{test3}"),
            ExpressionSplitter.splitExpression("{test1}{test2}test{}{test3}"));
		assertEquals(Arrays.asList("{a}"),
	            ExpressionSplitter.splitExpression("{a}"));
		assertEquals(Arrays.asList("{a}", "b"),
	            ExpressionSplitter.splitExpression("{a}b"));
		assertEquals(Arrays.asList("b", "{{a}}"),
	            ExpressionSplitter.splitExpression("b{{a}}"));
		assertEquals(Arrays.asList("\"b{}\"", "{{b}}", "hallo", "/test/", "[macro]", "blub"),
	            ExpressionSplitter.splitExpression("\"b{}\"{{b}}hallo/test/[macro]blub"));
		assertEquals(Arrays.asList("{{[/\"sdj][sd}/\"}[][]//}}"),
	            ExpressionSplitter.splitExpression("{{[/\"sdj][sd}/\"}[][]//}}"));
    }

    @Test
    public void testDontSplitIfNoClosingForFirstOpeningDelimiter() {
		assertEquals(Arrays.asList("{blas [] \"\"//"),
	            ExpressionSplitter.splitExpression("{blas [] \"\"//"));
    }

    @Test
    public void testDontSplitIfClosingDelimiterBeforeOpeningDelimiter() {
		assertEquals(Arrays.asList("bla}something{else}{"),
	            ExpressionSplitter.splitExpression("bla}something{else}{"));
    }

    @Test
    public void testDontSplitIfDelimiterEqualButOnlyOneExists() {
		assertEquals(Arrays.asList("/"),
	            ExpressionSplitter.splitExpression("/"));
    }

    @Test
    public void testSplitJustOnceIfThreeDelimitersExist() {
		assertEquals(Arrays.asList("abc", "/def/", "geh/ijk"),
	            ExpressionSplitter.splitExpression("abc/def/geh/ijk"));
    }
}
