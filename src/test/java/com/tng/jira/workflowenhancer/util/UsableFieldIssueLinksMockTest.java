package com.tng.jira.workflowenhancer.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;
import static org.fest.assertions.api.Assertions.assertThat;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.managers.DefaultIssueManager;
import com.tng.jira.workflowenhancer.util.UsableField;



public class UsableFieldIssueLinksMockTest extends TestCase{

	@Mock
	protected IssueLinkManager issueLinkManager;
	@Mock 
	protected Issue issue, issue1, issue2, issue3, issue4;
	@Mock
	protected List<IssueLink> inwardLinks;
	@Mock
	protected List<IssueLink> outwardLinks;
	@Mock
	protected IssueLink inwardIssueLink1;
	@Mock
	protected IssueLink inwardIssueLink2;
	@Mock
	protected IssueLink outwardIssueLink1;
	@Mock
	protected IssueLink outwardIssueLink2;
	@Mock
	protected IssueLinkType issueLinkType1, issueLinkType2, issueLinkType3, issueLinkType4;
	@Mock
	protected Iterator<IssueLink> inwardIterator;
	@Mock
	protected Iterator<IssueLink> outwardIterator;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void testTest() {
		setup();
		assertThat(issueLinkTest(false,false)).isNotEmpty();
	}
	
	@Test
	public void testNoPassiveLinks(){
		setup();
		assertThat(issueLinkTest(true, false)).isEqualTo("\" Aktive: e % f $ g % h\"");
	}
	
	@Test
	public void testNoPassiveLinksOrder(){
		setup();
		assertThat(issueLinkTest(true,false).compareTo("\" B\"")).isLessThan(0);
		assertThat(issueLinkTest(true,false).compareTo("\" Aa\"")).isGreaterThan(0);
	}
	
	@Test
	public void testNoAktiveLinks(){
		setup();
		assertThat(issueLinkTest(false, true)).isEqualTo("\"Passive: a % b $ c % d\"");
	}
	
	@Test
	public void testNoAktiveLinksOrder(){
		setup();
		assertThat(issueLinkTest(false, true).compareTo("\"F\"")).isGreaterThan(0);
		assertThat(issueLinkTest(false, true).compareTo("\"Q\"")).isLessThan(0);
	}
	
	@Test
	public void testNoLinks(){
		setup();
		assertThat(issueLinkTest(true, true)).isEqualTo("\"\"");
	}
	
	@Test
	public void testAktiveAndPassiveLinks(){
		setup();
		assertThat(issueLinkTest(false,false)).isEqualTo("\"Passive: a % b $ c % d Aktive: e % f $ g % h\"");
	}
	
	public String issueLinkTest( boolean inwardIsEmpty, boolean outwardIsEmptpy) {
		String[] iL1 = {"a","b"}, iL2 = {"c", "d"} , oL1 = {"e","f"}, oL2 = {"g","h"};
		List<String[]> inwardValues  = new ArrayList<String[]>(), outwardValues = new ArrayList<String[]>();
		inwardValues.add(iL1);
		inwardValues.add(iL2);
		outwardValues.add(oL1);
		outwardValues.add(oL2);		
		setReturnValuesForInwardAndOutward(inwardValues, outwardValues,inwardIsEmpty,outwardIsEmptpy);
		return (String) UsableField.ISSUE_LINKS.getCurrentValue(issueLinkManager, issue);
	}
		
	
	protected void setReturnValuesForInwardAndOutward(List<String[]> inwardValues, List<String[]> outwardValues, boolean inwardIsEmpty, boolean outwardIsEmpty){
		when(issue.getId()).thenReturn(0L);
		when(issueLinkManager.getInwardLinks(0L)).thenReturn(inwardLinks);
		when(issueLinkManager.getOutwardLinks(0L)).thenReturn(outwardLinks);
		if(inwardIsEmpty){
			when(inwardLinks.isEmpty()).thenReturn(true);
		}else{
			when(inwardLinks.isEmpty()).thenReturn(false);
		}
		if(outwardIsEmpty){
			when(outwardLinks.isEmpty()).thenReturn(true);
		}else{
			when(outwardLinks.isEmpty()).thenReturn(false);
		}

		when(inwardLinks.iterator()).thenReturn(inwardIterator);
		when(inwardIterator.hasNext()).thenReturn(true,true,true,false);
		when(inwardIterator.next()).thenReturn(inwardIssueLink1,inwardIssueLink2);
		when(outwardLinks.iterator()).thenReturn(outwardIterator);
		when(outwardIterator.hasNext()).thenReturn(true,true,true,false);
		when(outwardIterator.next()).thenReturn(outwardIssueLink1,outwardIssueLink2);
		
		when(inwardIssueLink1.getIssueLinkType()).thenReturn( issueLinkType1);
		when(inwardIssueLink2.getIssueLinkType()).thenReturn(issueLinkType2);
		when(inwardIssueLink1.getDestinationObject()).thenReturn(issue1);
		when(inwardIssueLink2.getDestinationObject()).thenReturn(issue2);
		
		when(issueLinkType1.getName()).thenReturn((inwardValues.get(0))[0]);
		when(issueLinkType2.getName()).thenReturn((inwardValues.get(1))[0]);
		when(issue1.getKey()).thenReturn((inwardValues.get(0))[1]);
		when(issue2.getKey()).thenReturn((inwardValues.get(1))[1]);
		
		when(outwardIssueLink1.getIssueLinkType()).thenReturn(issueLinkType3);
		when(outwardIssueLink2.getIssueLinkType()).thenReturn(issueLinkType4);
		when(outwardIssueLink1.getDestinationObject()).thenReturn(issue3);
		when(outwardIssueLink2.getDestinationObject()).thenReturn(issue4);
		
		when(issueLinkType3.getName()).thenReturn(outwardValues.get(0)[0]);
		when(issueLinkType4.getName()).thenReturn(outwardValues.get(1)[0]);
		when(issue3.getKey()).thenReturn(outwardValues.get(0)[1]);
		when(issue4.getKey()).thenReturn(outwardValues.get(1)[1]);
		
	}
}
