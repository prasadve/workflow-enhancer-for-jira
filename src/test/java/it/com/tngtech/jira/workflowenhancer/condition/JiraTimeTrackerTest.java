package it.com.tngtech.jira.workflowenhancer.condition;

import static org.fest.assertions.api.Assertions.assertThat;

import org.junit.Test;

import com.atlassian.jira.functest.framework.admin.TimeTracking;

public class JiraTimeTrackerTest extends ConditionTest {

    @Test
    public void testDaysPerWeek() {
        assertThat(testTimeTracking("1", "3", "3d=1w")).isTrue();
    }

    @Test
    public void testHoursPerDay() {
        assertThat(testTimeTracking("4", "6", "4h=1d")).isTrue();
    }

    @Test
    public void testDaysAndHours() {
        assertThat(testTimeTracking("5", "2", "2d 5h=1w 1d")).isTrue();
    }

    @Test
    public void testDayIsFloatValue() {
        assertThat(testTimeTracking("5.5", "4", "1d=5h 30min")).isTrue();
    }

    @Test
    public void testWeekIsFloatValue() {
        assertThat(testTimeTracking("1", "2.5", "1w=2d 30min")).isTrue();
    }

    public boolean testTimeTracking(String hoursPerDay, String daysPerWeek, String condition) {
        //Setup
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY1);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        //setTimeTicker
        administration.timeTracking().disable();
        // It would be nicer to use the TimeTracking.Format Constans (TimeTracking.Format.PRETTY, TimeTracking.Mode.MINUTE)
        // instead, but this is only possible for JIRA 5.0.x, but not for JIRA 4.4
        administration.timeTracking().enable(hoursPerDay, daysPerWeek, "pretty", "minute", TimeTracking.Mode.MODERN);
        log.debug("Done - set TimeTicker");

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);
        log.debug("Done - Setup ");

        return defaultConditionTestAfterInitializing(issueKey, condition, true);
    }

    @Test
    public void testTimeTrackingDisabled() {
        assertThat(testTimeTracking("5=5")).isTrue();
    }

    @Test
    public void testTimeTrackingDisabledWithTimespanCondition() {
        assertThat(testTimeTracking("1d=1d")).isTrue();
    }

    @Test
    public void testTimeTrackingDisabledDayAndHour() {
        assertThat(testTimeTracking("1d=8h")).isTrue();
    }

    @Test
    public void testTimeTrackingDisabledWeekAndDay() {
        assertThat(testTimeTracking("1w=5d")).isTrue();
    }

    public boolean testTimeTracking(String condition) {
        //Setup
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY1);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        //setTimeTicker
        administration.timeTracking().disable();
        log.debug("Done - set TimeTicker");

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);
        log.debug("Done - Setup ");

        return defaultConditionTestAfterInitializing(issueKey, condition, true);
    }

}
