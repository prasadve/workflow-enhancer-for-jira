package it.com.tngtech.jira.workflowenhancer.validator;

import java.util.Map;

import org.junit.Test;

public class PriorityValidationTest extends ValidationTest {

    /*
     * For English test data: Blocker = 1, Critical = 2, Major = 3, Minor = 4, Trivial = 5
     * 
     * Only text comparison possible
     */

    @Test
    public void testPriorityEntry() {
        Map<String, String[]> issueParam = createMap("priority", "1");
        assertTrue(jiraArrayValidationTest(issueParam, "1=1", true));
    }

    @Test
    public void testPriorityEqualsBlocker() {
        Map<String, String[]> issueParam = createMap("priority", "1");
        assertTrue(jiraArrayValidationTest(issueParam, "{Priority}==\"Blocker\"", true));
    }

    @Test
    public void testPriorityNotEqualsMajor() {
        Map<String, String[]> issueParam = createMap("priority", "1");
        assertTrue(jiraArrayValidationTest(issueParam, "{Priority}!=\"Major\"", true));
    }

    @Test
    public void testPriorityNotEqualsMajorFalse() {
        Map<String, String[]> issueParam = createMap("priority", "3");
        assertTrue(jiraArrayValidationTest(issueParam, "{Priority}!=\"Major\"", false));
    }

    @Test
    public void testPrioritySmallerLexicographicallyGreatString() {
        assertTrue(jiraArrayValidationTest(createMap("priority", "3"), "{Priority}<\"z\"", true));
    }

    @Test
    public void testPriorityGreaterLexicographicallySmallString() {
        assertTrue(jiraArrayValidationTest(createMap("priority", "3"), "{Priority}>\"A\"", true));
    }
}
