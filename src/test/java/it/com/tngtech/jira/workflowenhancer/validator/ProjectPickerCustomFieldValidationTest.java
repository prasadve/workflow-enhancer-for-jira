package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.meterware.httpunit.WebForm;

public class ProjectPickerCustomFieldValidationTest extends ValidationTest {

    @Rule
    ExpectedException exception = ExpectedException.none();

    @Test
    public void testProjectPickerCustomFieldTrivialCondition() throws Exception {
        assertTrue(projectPickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, PROJECT_NAME, PROJECT_KEY, "1=1",
                true));
    }

    @Test
    public void testProjectPickerCustomFieldEquals() throws Exception {
        assertTrue(projectPickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, PROJECT_NAME, PROJECT_KEY, "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} == \"" + PROJECT_NAME + "\"", true));
    }

    @Test
    public void testProjectPickerCustomFieldEqualsFalse() throws Exception {
        assertTrue(projectPickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, PROJECT_NAME, PROJECT_KEY, "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} == \"" + PROJECT_NAME2 + "\"", false));
    }

    /**
     * Tests if universal validations with projectpicker customfields work correctly. Adds project with PROJECT_NAME2
     * and PROJECT_KEY2. Creates a customfield, sets its value, evaluates the condition.
     * 
     * @param fieldName name of the customfield
     * @param projectName
     * @param projectKey
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    public boolean projectPickerCustomFieldValidationTest(String fieldName, String projectName, String projectKey,
            String condition, boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);

        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // Create second project
        createNewProject(PROJECT_NAME2, PROJECT_KEY2);

        // Create customfield
        String customfield_customfieldId = createNewCustomField(PROJECTPICKER_ID, fieldName);
        log.debug("Done - created customfields");

        // Set issue parameters and create issue
        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);
        log.debug("Done - created issue");

        // Set customfield
        String projectId = getProjectId(projectKey);
        navigation.issue().gotoEditIssue(issueKey);
        WebForm editForm = form.getForms()[1];
        assertTrue(editForm.hasParameterNamed("summary"));
        assertTrue(editForm.hasParameterNamed(customfield_customfieldId));
        editForm.setParameter(customfield_customfieldId, projectId);
        tester.submit("Update");

        // Test if customfield is set
        navigation.issue().gotoIssue(issueKey);
        tester.assertElementPresent(customfield_customfieldId + "-val");
        tester.assertTextInElement(customfield_customfieldId + "-val", projectName);
        log.info("Customfield is set.");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }
}
