package it.com.tngtech.jira.workflowenhancer.condition;

import org.junit.Test;

import com.meterware.httpunit.WebForm;

public class CustomFieldConditionTest extends ConditionTest {

    @Test
    public void testCustomFieldNotEqualsString() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "!=\"" + USERNAME1 + "\"", true));
    }

    @Test
    public void testNotCustomFieldNotEqualsString() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "!=\"" + USERNAME1 + "\")", false));
    }

    @Test
    public void testCustomFieldNotEqualsStringFalse() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "!=\"" + USERNAME3 + "\"", false));
    }

    @Test
    public void testNotCustomFieldNotEqualsStringFalse() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "!=\"" + USERNAME3 + "\")", true));
    }

    @Test
    public void testCustomFieldEqualsString() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "==\"" + USERNAME3 + "\"", true));
    }

    @Test
    public void testNotCustomFieldEqualsString() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "==\"" + USERNAME3 + "\")", false));
    }

    @Test
    public void testCustomFieldEqualsStringFalse() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "==\"" + USERNAME4 + "\"", false));
    }

    @Test
    public void testNotCustomFieldEqualsStringFalse() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "==\"" + USERNAME4 + "\")", true));
    }

    @Test
    public void testCustomFieldEqualsAssigneeFalse() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "=={Assignee}+\" minda\"", false));
    }

    @Test
    public void testNotCustomFieldEqualsAssigneeFalse() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "=={Assignee}+\" minda\")", true));
    }

    @Test
    public void testCustomFieldNotEqualsAssignee() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "!={Assignee}", true));
    }

    @Test
    public void testNotCustomFieldNotEqualsAssignee() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "!={Assignee})", false));
    }

    @Test
    public void testCustomFieldNotEqualsCustomField() {
        assertTrue(jiraArrayConditionTest(getJiraFieldExpression() + "!=" + getJiraFieldExpression(), false));
    }

    @Test
    public void testNotCustomFieldNotEqualsCustomField() {
        assertTrue(jiraArrayConditionTest("!(" + getJiraFieldExpression() + "!=" + getJiraFieldExpression() + ")", true));
    }

    /**
     * This method tests if the condition is evaluated correctly. A JIRA instance with an already existing issue is
     * restored. This issue has an userpicker custom field, which is named "FIELDNAME" and as user is selected
     * USERNAME3. You can find these constants in it.tng.jira.workflowenhancer.UniversalTestConstants.java
     * 
     * @param condition
     * @param condIsCorrect boolean, that tells, if the condition is true or false
     * @return if no assertion has failed, true is given back
     */
    public boolean jiraArrayConditionTest(String condition, boolean condIsCorrect) {

        //Setup
        restore(CUSTOMFIELDSDATA);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        log.debug("Login successfull");

        //Set Condition
        goToTransitionCreationStartProgress_Condition(WORKFLOW_NAME);
        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, condition);
        log.debug("Done - set Parameter");
        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");

        //Assign SchemeTo Project
        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
        //assignSchemeToProject(PROJECT_KEY, SCHEME_NAME);

        //Evaluation
        navigation.issue().viewIssue(ISSUE_KEY);
        String htmlSource = tester.getDialog().getResponseText();
        log.debug("Done - getResponseText");
        assertTrue(htmlSource.contains(ISSUE_KEY));
        if (condIsCorrect) {
            assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));
        } else {
            assertFalse(htmlSource.contains(START_PROGRESS_LINK_ID));
        }
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));
        navigation.logout();
        return true;
    }
}