package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Test;

import com.meterware.httpunit.WebForm;

public class CustomFieldValidationTest extends ValidationTest {

    @Test
    public void testCustomFieldNotEqualsString() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "!=\"" + USERNAME1 + "\"", true));
    }

    @Test
    public void testNotCustomFieldNotEqualsString() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "!=\"" + USERNAME1 + "\")", false));
    }

    @Test
    public void testCustomFieldNotEqualsStringFalse() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "!=\"" + USERNAME3 + "\"", false));
    }

    @Test
    public void testNotCustomFieldNotEqualsStringFalse() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "!=\"" + USERNAME3 + "\")", true));
    }

    @Test
    public void testCustomFieldEqualsString() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "==\"" + USERNAME3 + "\"", true));
    }

    @Test
    public void testNotCustomFieldEqualsString() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "==\"" + USERNAME3 + "\")", false));
    }

    @Test
    public void testCustomFieldEqualsStringFalse() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "==\"" + USERNAME4 + "\"", false));
    }

    @Test
    public void testNotCustomFieldEqualsStringFalse() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "==\"" + USERNAME4 + "\")", true));
    }

    @Test
    public void testCustomFieldEqualsAssigneeFalse() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "=={Assignee}+\" minda\"", false));
    }

    @Test
    public void testNotCustomFieldEqualsAssigneeFalse() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "=={Assignee}+\" minda\")", true));
    }

    @Test
    public void testCustomFieldNotEqualsAssignee() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "!={Assignee}", true));
    }

    @Test
    public void testNotCustomFieldNotEqualsAssignee() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "!={Assignee})", false));
    }

    @Test
    public void testCustomFieldNotEqualsCustomField() {
        assertTrue(jiraArrayValidationTest(getJiraFieldExpression() + "!=" + getJiraFieldExpression(), false));
    }

    @Test
    public void testNotCustomFieldNotEqualsCustomField() {
        assertTrue(jiraArrayValidationTest("!(" + getJiraFieldExpression() + "!=" + getJiraFieldExpression() + ")",
                true));
    }

    public boolean jiraArrayValidationTest(String condition, boolean condIsTrue) {
        //Setup		
        restore(CUSTOMFIELDSDATA);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        goToTransitionCreationStartProgress_Validation(WORKFLOW_NAME);
        //Set Validation		
        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, condition);
        log.debug("Done - set Parameter");
        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");

        //Assign Scheme To Project
        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
        //assignSchemeToProject(PROJECT_KEY, SCHEME_NAME);

        //Evaluation
        navigation.issue().viewIssue(ISSUE_KEY);
        String htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(ISSUE_KEY));
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));
        assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));

        tester.clickLink(START_PROGRESS_LINK_ID);
        log.info("Tester clicked on start progress link.");
        htmlSource = tester.getDialog().getResponseText();

        if (condIsTrue) {
            assertFalse(htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_ENGLISH)
                    || htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_GERMAN));
            log.info("Tester started progress and no workflow error occured.");
        } else {
            assertTrue(htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_ENGLISH)
                    || htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_GERMAN));
            log.info("Workflow Error after tester wanted to start progress.");
        }

        navigation.issue().viewIssue(ISSUE_KEY);
        htmlSource = tester.getDialog().getResponseText();

        if (condIsTrue) {
            assertFalse(htmlSource.contains(START_PROGRESS_LINK_ID));
            tester.assertLinkNotPresent(START_PROGRESS_LINK_ID);
            log.info("The start progress link has gone, so we see that the issue has changed its status.");
        } else {
            assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));
            tester.assertLinkPresent(START_PROGRESS_LINK_ID);
            log.info("The start progress link is still there. So the validation inhibited the user to start progress.");
        }

        navigation.logout();

        return true;
    }
}
