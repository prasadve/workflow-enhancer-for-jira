package it.com.tngtech.jira.workflowenhancer.validator;

import java.util.Map;

import org.junit.Test;

public class UserValidationTest extends ValidationTest {

    @Test
    public void testAssigneeEqualsReporter() {
        Map<String, String[]> issueParam = createMap("assignee", USERNAME_ADMIN);
        String[] value = { USERNAME_ADMIN };
        issueParam.put("reporter", value);
        assertTrue(jiraArrayValidationTest(issueParam, "{Assignee}=={Reporter}", true));
    }

    @Test
    public void testAssigneeNotEqualsReporter() {
        Map<String, String[]> issueParam = createMap("assignee", USERNAME_ADMIN);
        String[] value = { USERNAME4 };
        issueParam.put("reporter", value);
        assertTrue(jiraArrayValidationTest(issueParam, "{Assignee}!={Reporter}", true));
    }

    /**
     * Tests if universal validations with jiraArrays work correctly. Adds users with USERNAME3 and USERNAME4. The
     * language of the test environment has to be English or German.
     * 
     * @param issueParam some parameters of the issue
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    @Override
    public boolean jiraArrayValidationTest(Map<String, String[]> issueParam, String condition, boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        administration.usersAndGroups().addUser(USERNAME3, PASSWORD3, FULLNAME3, EMAILADDRESS3);
        administration.usersAndGroups().addUser(USERNAME4, PASSWORD4, FULLNAME4, EMAILADDRESS4);
        administration.usersAndGroups().addUserToGroup(USERNAME3, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME4, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME3, GROUP_NAME_DEVELOPERS);
        administration.usersAndGroups().addUserToGroup(USERNAME4, GROUP_NAME_DEVELOPERS);

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME, issueParam);
        log.debug("Done - created issue");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }

}
