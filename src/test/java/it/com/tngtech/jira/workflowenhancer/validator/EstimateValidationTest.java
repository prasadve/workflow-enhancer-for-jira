package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Test;

public class EstimateValidationTest extends ValidationTest {

    // JIRA intern TimeSpan Units: 1w = 5d, 1d = 8h, 1h = 60m, where m is the usual minute. So d is kind of a work day unit.
    // JWFE WorkTimespan units are equal, except of the syntax for minutes: 1m (in JIRA) = 1min (in JWFE).

    @Test
    public void testCompareInternTimeSpanUnitsToJIRAUnitsMinute() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} == 1min", true, "1m", "0h"));
    }

    @Test
    public void testCompareInternTimeSpanUnitsToJIRAUnitsHour() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} == 1h", true, "1h", "0h"));
    }

    @Test
    public void testCompareInternTimeSpanUnitsToJIRAUnitsDay() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} == 1d", true, "1d", "0h"));
    }

    public void testCompareInternTimeSpanUnitsToJIRAUnitsWeek() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} == 1w", true, "1w", "0h"));
    }

    @Test
    public void testOriginalEstimateSmallerDayTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} < 3d", true, "2d", "1d"));
    }

    @Test
    public void testOriginalEstimateSmallerTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} < 1w 1d 4h", true, "6d 3h", "1d"));
    }

    @Test
    public void testOriginalEstimateSmallerTimeSpan2() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} < 5d 9h 2min", true, "1w 1d 1h 1m", "1d"));
    }

    @Test
    public void testOriginalEstimateGreaterOrEqualHourTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} >= 1h", true, "1h", "0h"));
    }

    @Test
    public void testOriginalEstimateGreaterOrEqualTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} >= 4d 3h 59min", true, "2w 20h ", "0h"));
    }

    @Test
    public void testOriginalEstimateGreaterTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Original Estimate} > 4d 3h 59min", true, "2w 20h ", "0h"));
    }

    @Test
    public void testRemainingEstimateEqualsTimeSpan() {
        assertTrue(jiraEstimateValidationTest("{Remaining Estimate} = 8h 7min", true, "2w 20h ", "1d 7m"));
    }

    /**
     * Tests if universal validations with time estimates work correctly. The language of the test environment has to be
     * English or German.
     * 
     * @param issueParam some parameters of the issue
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @param originalEstimate unit: JIRA intern
     * @param remainingEstimate unit: JIRA intern
     * @return true
     */
    public boolean jiraEstimateValidationTest(String condition, boolean condIsTrue, String originalEstimate,
            String remainingEstimate) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);
        navigation.issue().setEstimates(issueKey, originalEstimate, remainingEstimate);
        log.debug("Done - created issue");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }
}
