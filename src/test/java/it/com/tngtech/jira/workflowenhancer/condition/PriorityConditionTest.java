package it.com.tngtech.jira.workflowenhancer.condition;

import org.junit.Test;

public class PriorityConditionTest extends ConditionTest {

    /*
     * For English test data: Blocker = 1, Critical = 2, Major = 3, Minor = 4, Trivial = 5
     * 
     * Only text comparison possible
     */

    @Test
    public void testPriorityEntry() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "1"), "5==5", true));
    }

    @Test
    public void testPriorityEqualsFalsePriority() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "2"), "{Priority}==\"Trivial\"", false));
    }

    @Test
    public void testPriorityEqualsPriority() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "4"), "{Priority}==\"Minor\"", true));
    }

    @Test
    public void testPriorityLessEquals() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "3"), "{Priority}<=\"Trivial\"", true));
    }

    @Test
    public void testPriorityGreaterEqualsFalse() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "3"), "{Priority}>=\"Trivial\"", false));
    }

    @Test
    public void testPriorityNotEquals() {
        assertTrue(jiraArrayConditionTest(createMap("priority", "5"), "\"Major\"!={Priority}", true));
    }
}