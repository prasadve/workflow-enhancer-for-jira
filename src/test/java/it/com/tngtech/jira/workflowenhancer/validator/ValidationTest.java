package it.com.tngtech.jira.workflowenhancer.validator;

import it.com.tngtech.jira.workflowenhancer.UniversalTestConstants;
import it.com.tngtech.jira.workflowenhancer.WorkflowenhancerUtil;

import java.util.Map;

import org.junit.Ignore;

import com.meterware.httpunit.WebForm;

@Ignore
public class ValidationTest extends WorkflowenhancerUtil implements UniversalTestConstants {

    /**
     * Tests if universal validations with jiraArrays work correctly. The language of the test environment has to be
     * English or German.
     * 
     * @param issueParam some parameters of the issue
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    public boolean jiraArrayValidationTest(Map<String, String[]> issueParam, String condition, boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME, issueParam);

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }

    /**
     * Sets a validation and evaluates, if the validation is working for the issue. If the validation is valid for the
     * issue, it must be possible to open the issue. If the validation is not valid an error message must occur.
     * 
     * @param issueKey
     * @param condIsTrue
     * @return if no assertion failed true is given back, if an assertion fails the test fails
     */
    public boolean defaultValidationTestAfterInitializing(String issueKey, String validation, boolean condIsTrue) {
        goToTransitionCreationStartProgress_Validation(WORKFLOW_NAME);
        //Set Validation
        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, validation);
        log.debug("Done - set Parameter");

        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");
        //Assign workflow to project
        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
//        assignSchemeToProject(PROJECT_KEY, SCHEME_NAME);

        //Evaluation
        navigation.issue().viewIssue(issueKey);
        String htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(ISSUE_NAME));
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));
        assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));

        tester.clickLink(START_PROGRESS_LINK_ID);
        log.info("Tester clicked on start progress link.");
        htmlSource = tester.getDialog().getResponseText();

        if (condIsTrue) {
            assertFalse(htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_ENGLISH)
                    || htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_GERMAN));
            log.info("Tester started progress and no workflow error occured.");
        } else {
            assertTrue(htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_ENGLISH)
                    || htmlSource.contains(VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_GERMAN));
            log.info("Workflow Error after tester wanted to start progress.");
        }

        navigation.issue().viewIssue(issueKey);
        htmlSource = tester.getDialog().getResponseText();

        if (condIsTrue) {
            assertFalse(htmlSource.contains(START_PROGRESS_LINK_ID));
            tester.assertLinkNotPresent(START_PROGRESS_LINK_ID);
            log.info("The start progress link has gone, so we see that the issue has changed its status.");
        } else {
            assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));
            tester.assertLinkPresent(START_PROGRESS_LINK_ID);
            log.info("The start progress link is still there. So the validation inhibited the user to start progress.");
        }

        navigation.logout();

        return true;
    }
}
