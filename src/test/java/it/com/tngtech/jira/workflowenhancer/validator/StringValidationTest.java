package it.com.tngtech.jira.workflowenhancer.validator;

import java.util.Map;

import org.junit.Test;

public class StringValidationTest extends ValidationTest {

    @Test
    public void testEnvironmentEmptyFalse() {
        Map<String, String[]> issueParam = createMap("environment", "fancyEnvironment");
        assertTrue(jiraArrayValidationTest(issueParam, "{environment}=\"\"", false));
    }

    @Test
    public void testEnvironmentEqualsString() {
        Map<String, String[]> issueParam = createMap("environment", "fancyEnvironment");
        assertTrue(jiraArrayValidationTest(issueParam, "{environment}==\"fancyEnvironment\"", true));
    }

    @Test
    public void testDescriptionEqualsString() {
        Map<String, String[]> issueParam = createMap("description", "Test");
        assertTrue(jiraArrayValidationTest(issueParam, "{Description}==\"Test\"", true));
    }

    @Test
    public void testEnvironmentEqualsDescription() {
        Map<String, String[]> issueParam = createMap("environment", "Test");
        String[] value = { "Test" };
        issueParam.put("description", value);
        assertTrue(jiraArrayValidationTest(issueParam, "{Environment}=={Description}", true));
    }

    @Test
    public void testIssueTypeEqualsString() {
        Map<String, String[]> issueParam = createMap("duedate", "27/Jun/2012");
        assertTrue(jiraArrayValidationTest(issueParam, "{Issue Type}==\"" + ISSUE_TYPE + "\"", true));
    }
    
    @Test
    public void testComplexExpressionWithDescription() {
        Map<String, String[]> issueParam = createMap("description", "A text containing TNG.");
        assertTrue(jiraArrayValidationTest(issueParam, "({Description}==/.*TNG.*/)&&([now]>11.11.2011 11:11)", true));
    }
}
