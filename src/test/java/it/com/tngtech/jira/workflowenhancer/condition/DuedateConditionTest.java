package it.com.tngtech.jira.workflowenhancer.condition;

import org.junit.Test;

public class DuedateConditionTest extends ConditionTest {
    @Test
    public void testDuedateEntry() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "28/Aug/2012"), "5==5", true));
    }

    @Test
    // Depends on JIRA language: English: Dec German: Dez
    public void testDuedateLanguage() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "28/Dec/2012"), "5==5", true));
    }

    @Test
    public void testDuedateNotEqualsEmptyString() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "28/Feb/2011"), "{Due Date}!= \"\"", true));
    }

    // JIRA intern date pattern  dd/MMM/yyyy z.B. 28/Aug/2012
    // JWFE date pattern: dd.mm.yyyy hh:mm z.B. 28.08.2012 15:30 

    public void testDuedateGreaterEquals() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "27/Jun/2012"), "{Due Date} >= 27.06.1991", true));
    }

    @Test
    public void testDuedateEquals() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "27/Jun/2012"), "{Due Date}== 27.06.2012", true));
    }

    @Test
    public void testDuedateLessEquals() {
        assertTrue(jiraArrayConditionTest(createMap("duedate", "27/Jun/2012"), "{Due Date} <= 27.06.2042", true));
    }

}
