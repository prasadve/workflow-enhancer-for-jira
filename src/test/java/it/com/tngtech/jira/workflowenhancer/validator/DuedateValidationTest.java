package it.com.tngtech.jira.workflowenhancer.validator;

import java.util.Map;

import org.junit.Test;

public class DuedateValidationTest extends ValidationTest {

    /*
     * JIRA intern datepattern: dd/MMM/yyyy e.g. 28/Aug/2012 JWFE date pattern: dd.mm.yyyy hh:mm e.g. 28.08.2012 15:30
     * Time optional. The highest JWFE date is 31.12.2099.
     */

    @Test
    public void testDuedateNotEmpty() {
        Map<String, String[]> issueParam = createMap("duedate", "28/Aug/2012");
        assertTrue(jiraArrayValidationTest(issueParam, "{Due Date}!= \"\"", true));
    }

    @Test
    public void testDuedateGreaterOrEqualDate() {
        Map<String, String[]> issueParam = createMap("duedate", "27/Jun/2012");
        assertTrue(jiraArrayValidationTest(issueParam, "{Due Date} >= 27.06.1991", true));
    }

    @Test
    public void testDuedateEqualsDateFalse() {
        Map<String, String[]> issueParam = createMap("duedate", "27/Jun/2012");
        assertTrue(jiraArrayValidationTest(issueParam, "{Due Date} == 27.06.1991", false));
    }

    @Test
    public void testDuedateSmallerOrEqualDate() {
        Map<String, String[]> issueParam = createMap("duedate", "27/Jun/2012");
        assertTrue(jiraArrayValidationTest(issueParam, "{Due Date} <= 31.12.2099", true));
    }
}
