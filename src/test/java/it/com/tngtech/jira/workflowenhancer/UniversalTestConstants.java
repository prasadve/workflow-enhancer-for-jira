package it.com.tngtech.jira.workflowenhancer;

public interface UniversalTestConstants {

    final int MAX_THREAD_SLEEP_TIME_MILLISECONDS = 500;
    final int THREAD_SLEEP_INTERVALL = 20;

    final String ISSUE_KEY = "ROCKET-1";
    final String ISSUE_NAME = "FirstBug";
    final String ISSUE_NAME2 = "Ein Issue zum Testen";
    final String PROJECT_NAME = "Rocket";
    final String PROJECT_KEY = "ROCKET";
    final String PROJECT_NAME2 = "IndependentTestProject";
    final String PROJECT_KEY2 = "ITP";
    final String SCHEME_NAME = "TestWorkflowScheme2";
    final String SCHEME_NAME2 = "IndependentScheme";
    final String SUMMARY1 = "Issue 1";
    final String SUMMARY2 = "Not-used-for-testing-issue";
    final String WORKFLOW_NAME = "Black Magic Workflow";
    final String WORKFLOW_NAME2 = "IndependentWorkflow";
    final String ISSUE_TYPE = "Task";
    final String ISSUE_TYPE_GERMAN = "Aufgabe";
    final String FIELDNAME = "GarField";
    final String USERPICKER_CUSTOMFIELD_NAME = "Verifier";
    final String DEFAULT_CUSTOMFIELD_NAME = "TestCustomField";

    // Names of files with test data.
    final String DEFAULT_JIRA = "defaultJIRA.zip";
    final String CUSTOMFIELDSDATA = "CustomFields.zip";

    // Validation Workflow Error Dialog Text.
    final String VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_ENGLISH = "Workflow Error";
    final String VALIDATION_WORKFLOW_ERROR_DIALOG_TEXT_GERMAN = "FEHLER IM ARBEITSABLAUF";

    //////////
    // USER //
    //////////

    final String USERNAME1 = "dick";
    final String PASSWORD1 = "dick";
    final String FULLNAME1 = "Oliver Hardy";
    final String EMAILADRESS1 = "dick@doof.com";

    final String USERNAME2 = "doof";
    final String PASSWORD2 = "doof";
    final String FULLNAME2 = "Stan Laurel";
    final String EMAILADDRESS2 = "doof@dick.com";

    final String USERNAME3 = "merkela";
    final String PASSWORD3 = "christlich";
    final String FULLNAME3 = "Angela Merkel";
    final String EMAILADDRESS3 = "merkel@CDU.de";

    final String USERNAME4 = "gabries";
    final String PASSWORD4 = "sozial";
    final String FULLNAME4 = "Sigmar Gabriel";
    final String EMAILADDRESS4 = "gabriel@SPD.de";

    final String USERNAME_ADMIN = "admin";
    final String PASSWORD_ADMIN = "admin";

    final String GROUP_NAME_ADMIN = "jira-administrators";
    final String GROUP_NAME_DEVELOPERS = "jira-developers";

    /////////////////
    // JIRA-INTERN //
    /////////////////

    final String ADD_SUBMIT_PARAMETER_BUTTON_ID = "add_submit";
    final String JIRAFORM_WORKINGFORM_ID = "jiraform";
    final String EXPRESSION_SOUGHTNAME = "expression";
    final String CLOSE_LINK_ID = "action_id_2";
    final String START_PROGRESS_LINK_ID = "action_id_4";
    final String RESOLVE_LINK_ID = "action_id_5";
    final String STOP_PROGRESS_LINK_ID = "action_id_301";
    final String ASSIGNEE = "assignee";

    ////////////////////
    // CUSTOMFIELD_ID //
    ////////////////////

    final String USERPICKER_ID = "com.atlassian.jira.plugin.system.customfieldtypes:userpicker";
    final String DATAPICKER_ID = "com.atlassian.jira.plugin.system.customfieldtypes:datepicker";
    final String PROJECTPICKER_ID = "com.atlassian.jira.plugin.system.customfieldtypes:project";
    final String TEXTFIELD_ID = "com.atlassian.jira.plugin.system.customfieldtypes:textfield";
}
