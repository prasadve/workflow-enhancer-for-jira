package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.meterware.httpunit.WebForm;

public class TextCustomFieldValidationTest extends ValidationTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testTextCustomFieldTrivialCondition() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, SUMMARY1, "1=1", true));
    }

    @Test
    public void testTextCustomFieldEquals() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, SUMMARY1, "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} == \"" + SUMMARY1 + "\"", true));
    }

    @Test
    public void testTextCustomFieldEqualsFalse() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, SUMMARY1, "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} == \"" + SUMMARY2 + "\"", false));
    }

    /**
     * Tests if universal validations with textfield customfields work correctly. Creates a customfield, sets its value,
     * evaluates the condition.
     * 
     * @param fieldName name of the customfield
     * @param text will be insertet in the customfield
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    public boolean datePickerCustomFieldValidationTest(String fieldName, String text, String condition,
            boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // Create customfield
        String customfield_customfieldId = createNewCustomField(TEXTFIELD_ID, fieldName);
        log.debug("Done - created customfields");

        // Set issue parameters and create issue
        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);

        log.debug("Done - created issue");

        // Set customfield
        navigation.issue().gotoEditIssue(issueKey);
        WebForm editForm = form.getForms()[1];
        assertTrue(editForm.hasParameterNamed("summary"));
        assertTrue(editForm.hasParameterNamed(customfield_customfieldId));
        editForm.setParameter(customfield_customfieldId, text);
        tester.submit("Update");

        // Test if customfield is set
        navigation.issue().gotoIssue(issueKey);
        tester.assertElementPresent(customfield_customfieldId + "-val");
        tester.assertTextInElement(customfield_customfieldId + "-val", text);
        log.info("Customfield is set.");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }

}
