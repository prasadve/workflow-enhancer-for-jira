package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.meterware.httpunit.WebForm;

public class DatePickerCustomFieldValidationTest extends ValidationTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testDatePickerCustomFieldTrivialValidation() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, "11/Sep/12", "11/Sep/12", "1=1", true));
    }

    @Test
    public void testDatePickerCustomFieldEquals() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, "11/Sep/12", "11/Sep/12", "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} = 11.09.2012", true));
    }

    @Test
    public void testDatePickerCustomFieldNotEqualsDuedateFalse() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, "11/Sep/12", "11/Sep/12", "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} != {Due Date}", false));
    }

    @Test
    public void testDatePickerCustomFieldNotEqualsDuedate() throws Exception {
        assertTrue(datePickerCustomFieldValidationTest(DEFAULT_CUSTOMFIELD_NAME, "28/Apr/11", "11/Sep/12", "{"
                + DEFAULT_CUSTOMFIELD_NAME + "} != {Due Date}", true));
    }

    /**
     * Tests if universal validations with datepicker customfields work correctly. Creates a customfield, sets its
     * value, sets the duedate, evaluates the condition.
     * 
     * @param fieldName name of the customfield
     * @param date value of the customfield
     * @param duedate value of duedate
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    public boolean datePickerCustomFieldValidationTest(String fieldName, String date, String duedate, String condition,
            boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // Create customfield
        String customfield_customfieldId = createNewCustomField(DATAPICKER_ID, fieldName);
        log.debug("Done - created customfields");

        // Set issue parameters and create issue
        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME);
        navigation.issue().setDueDate(issueKey, duedate);
        log.debug("Done - created issue");

        // Set customfield
        navigation.issue().gotoEditIssue(issueKey);
        WebForm editForm = form.getForms()[1];
        assertTrue(editForm.hasParameterNamed("summary"));
        assertTrue(editForm.hasParameterNamed(customfield_customfieldId));
        editForm.setParameter(customfield_customfieldId, date);
        tester.submit("Update");

        // Test if customfield is set
        navigation.issue().gotoIssue(issueKey);
        tester.assertElementPresent(customfield_customfieldId + "-val");
        tester.assertTextInElement(customfield_customfieldId + "-val", date);
        log.info("Customfield is set.");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }
}
