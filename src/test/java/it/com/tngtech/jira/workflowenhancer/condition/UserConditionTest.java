package it.com.tngtech.jira.workflowenhancer.condition;

import it.com.tngtech.jira.workflowenhancer.UniversalTestConstants;

import java.util.Map;

import org.junit.Test;

public class UserConditionTest extends ConditionTest implements UniversalTestConstants {

    @Test
    public void testReporterEntry() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME1), "5==5", true));
    }

    @Test
    public void testReporterNotEqualsEmptyString() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME2), "{reporter}!=\"\"", true));
    }

    @Test
    public void testReporterEqualsReporter() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME1), "{Reporter}=={Reporter}", true));
    }

    @Test
    public void testReporterEqualsFullname() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME1), "{reporter}==\"" + USERNAME1 + "\"", true));
    }

    @Test
    public void testReporterEqualsFalseFullname() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME1), "{reporter}==\"" + USERNAME2 + "\"", false));
    }

    @Test
    public void testReporterEqualsRegEx() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME2), "{reporter}==/do+f/", true));
    }

    @Test
    public void testReporterEqualsFalseRegEx() {
        assertTrue(jiraArrayConditionTest(createMap("reporter", USERNAME2), "{reporter}==/a*b+c?/", false));
    }

    @Test
    public void testAssigneeEqualsAdmin() {
        assertTrue(jiraArrayConditionTest(setup("reporter", "admin", "assignee", "admin"), "{assignee}==\"admin\"",
                true));
    }

    @Test
    public void testReporterEqualsAssignee() {
        assertTrue(jiraArrayConditionTest(setup("reporter", USERNAME1, "assignee", USERNAME1),
                "{Reporter}=={Assignee}", true));
    }

    @Test
    public void testReporterNotEqualsAssigneeFalse() {
        assertTrue(jiraArrayConditionTest(setup("reporter", USERNAME1, "assignee", USERNAME1),
                "{Reporter}!={Assignee}", false));
    }

    @Test
    public void testReporterEqualsAssigneeFalse() {
        assertTrue(jiraArrayConditionTest(setup("reporter", USERNAME1, "assignee", USERNAME2),
                "{Reporter}=={Assignee}", false));
    }

    @Test
    public void testReporterNotEqualsAssignee() {
        assertTrue(jiraArrayConditionTest(setup("reporter", USERNAME1, "assignee", USERNAME2),
                "{Reporter}!={Assignee}", true));
    }

    /**
     * Tests if universal conditions with jiraArrays work correctly. Adds users with USERNAME3 and USERNAME4. The
     * language of the test environment has to be English or German.
     * 
     * @param issueParam some parameters of the issue
     * @param condition the usual boolean / regular /... expression
     * @param condIsCorrect is true if and only if the condition is true
     * @return true
     */
    @Override
    public boolean jiraArrayConditionTest(Map<String, String[]> issueParam, String condition, boolean condIsCorrect) {
        // Setup
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY1);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // Create users
        administration.usersAndGroups().addUser(USERNAME1, PASSWORD1, FULLNAME1, EMAILADRESS1);
        administration.usersAndGroups().addUser(USERNAME2, PASSWORD2, FULLNAME2, EMAILADDRESS2);
        administration.usersAndGroups().addUserToGroup(USERNAME1, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME2, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME1, GROUP_NAME_DEVELOPERS);
        administration.usersAndGroups().addUserToGroup(USERNAME2, GROUP_NAME_DEVELOPERS);
        log.debug("Users created");

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME, issueParam);
        log.debug("Done - Setup ");

        return defaultConditionTestAfterInitializing(issueKey, condition, condIsCorrect);
    }
}
