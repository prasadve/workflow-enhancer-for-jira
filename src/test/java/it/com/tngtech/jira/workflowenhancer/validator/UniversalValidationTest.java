package it.com.tngtech.jira.workflowenhancer.validator;

import it.com.tngtech.jira.workflowenhancer.UniversalTestConstants;
import it.com.tngtech.jira.workflowenhancer.WorkflowenhancerUtil;

import org.junit.Test;

import com.meterware.httpunit.WebForm;


public class UniversalValidationTest extends WorkflowenhancerUtil implements UniversalTestConstants {

    @Test
    public void testInterface() {

        restore("TestData.zip");
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // goToTransitionCreation_Validation();
        goToTransitionCreationStartProgress_Validation("TestWorkflow2");

        log.debug("Begin of asserts in testInterface().");
        tester.assertElementPresent("expression");
        tester.assertElementPresent("fields");
        tester.assertElementPresent("evaluateTo");
        tester.assertElementPresent("add_submit");
        tester.assertElementPresent("cancelButton");
        tester.assertElementPresent("message");
        log.debug("End of asserts.");

        createNewProject(PROJECT_NAME2, PROJECT_KEY2);
        createWorkflowSchemeWithCopyOfJiraContained(SCHEME_NAME2, WORKFLOW_NAME2);
        //assignSchemeToProject(PROJECT_KEY2, SCHEME_NAME2);
        administration.project().associateWorkflowScheme(PROJECT_NAME2, SCHEME_NAME2);
        createNewIssue(PROJECT_NAME2, "Bug", SUMMARY2);
        //createNewIssueWithoutKey(PROJECT_KEY2, "Bug", "Beschreibung des Bugs.");

        navigation.logout();
    }

    @Test
    public void testInterfaceWithDefaultJiraData() {

        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME2, PROJECT_KEY2, SCHEME_NAME2, WORKFLOW_NAME2, ISSUE_TYPE,
                SUMMARY2);

        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        createCopyOfJiraWorkflow(WORKFLOW_NAME);

        goToTransitionCreationStartProgress_Validation(WORKFLOW_NAME);

        log.debug("Begin of asserts in testInterfaceWithDefaultJiraData).");
        tester.assertElementPresent("expression");
        tester.assertElementPresent("fields");
        tester.assertElementPresent("evaluateTo");
        tester.assertElementPresent("add_submit");
        tester.assertElementPresent("cancelButton");
        tester.assertElementPresent("message");
        log.debug("End of asserts.");

        navigation.logout();
    }

    @Test
    public void testFalseExpression() {
        validationFailureTest("1=2", false, "message");
    }

    @Test
    public void testMacroNotEqualsUsername() {
        validationFailureTest("[user] != \"admin\"", false, "message 2");
    }

    @Test
    public void testInvalidStringExpression() {
        validationFailureTest("test=test", true, "message 4");
    }

    @Test
    public void testInvalidExpressionWithMacroAndString() {
        validationFailureTest("[now]<datum", true, "message 5");
    }

    @Test
    public void testDatePlusTimespanTerm() {
        validationFailureTest("19.12.1989 = 19.12.1989 + 24H", false, "message 6");
    }

    @Test
    public void testInvalidExpressionWithBooleanOr() {
        validationFailureTest("\"\"=0|1", true, "message 7");
    }

    @Test
    public void testFalseFloatExpression() {
        validationFailureTest(".1 > 42.4242424242", false, "message 8");
    }

    /**
     * Test method
     * 
     * @param expression has to be an invalid expression or a false expression
     * @param invalidInput true if the input expression is invalid (i.e. it throws an InvalidInputException)
     * @param message
     * 
     * @return true if the test is successful
     * 
     */
    public boolean validationFailureTest(String expression, boolean invalidInput, String message) {
        restore("TestData.zip");
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // goToTransitionCreation_Validation();
        goToTransitionCreationStartProgress_Validation("TestWorkflow2");

        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[1];

        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed("expression"));
        assertTrue(f.hasParameterNamed("message"));

        f.setParameter(EXPRESSION_SOUGHTNAME, expression);
        f.setParameter("message", message);

        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);

        String htmlSource = tester.getDialog().getResponseText();
        assertTrue((htmlSource.contains("id=\"JWFEerror\"") == invalidInput));

        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
        //setScheme();

        navigation.issue().viewIssue(ISSUE_KEY);
        htmlSource = tester.getDialog().getResponseText();

        assertTrue(htmlSource.contains(ISSUE_NAME));
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));
        assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));

        tester.clickLink(START_PROGRESS_LINK_ID);

        htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(message));

        navigation.issue().viewIssue(ISSUE_KEY);
        htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));

        navigation.logout();

        return true;
    }
}
