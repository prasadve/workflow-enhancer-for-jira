package it.com.tngtech.jira.workflowenhancer;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.testkit.beans.WorkflowSchemeData;
import com.meterware.httpunit.WebForm;

@Ignore
public class WorkflowenhancerUtil extends FuncTestCase implements UniversalTestConstants {

    protected final static Logger log = Logger.getLogger(WorkflowenhancerUtil.class);

    // TODO: Logging configuaration not working. See JWFE-3.
    @Before
    public void setup() {
        log.setLevel(Level.DEBUG);
    }

    /**
     * Creates a test environment: Restores data from a file, creates a new project, a new workflow scheme with a Copy
     * of Jira Workflow, creates a new issue and deletes default configuration and returns the issueKey.
     * 
     * @param file e.g. "TestData.zip"
     * @param projectName
     * @param projectKey
     * @param schemeName
     * @param workflowName
     * @param issueType e.g. Bug, Verbesserung, ... depends on language
     * @param summary summary / description of the issue
     * @return issueKey
     */
    protected String createTestEnvironment(String file, String projectName, String projectKey, String schemeName,
            String workflowName, String issueType, String summary) {
        restore(file);
        navigation.login("admin", "admin");
        createNewProject(projectName, projectKey);
        createWorkflowSchemeWithCopyOfJiraContained(schemeName, workflowName);
        String issueKey = "???";
        issueKey = createNewIssue(projectName, issueType, summary);
        deleteDefaultCondition(workflowName);
        navigation.logout();

        log.info("Test environment created.");

        return issueKey;
    }

    /**
     * Restores data from a file.
     * 
     * @param file e.g. "TestData.zip"
     */
    protected void restore(String file) {
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        administration.restoreData(file);
        navigation.logout();
        log.info("Restored data from file " + file);
    }

    /**
     * Assigns an existing project to an existing workflow scheme.
     * 
     * @param projectKey
     * @param schemeName
     */
    protected void assignSchemeToProject(String projectKey, String schemeName) {
        tester.gotoPage("/plugins/servlet/project-config/" + projectKey + "/workflows");
        tester.assertLinkPresent("project-config-workflows-scheme-change");
        tester.clickLink("project-config-workflows-scheme-change");
    	
    	tester.setWorkingForm(FunctTestConstants.JIRA_FORM_NAME);
        tester.assertElementPresent("schemeId_select");
        tester.selectOption("schemeId", schemeName);
        tester.assertButtonPresent("associate_submit");
        tester.clickButton("associate_submit");
        tester.assertButtonPresent("workflow-mapping-submit");
        tester.clickButton("workflow-mapping-submit");

        int sleepingTime = 0;
        String htmlSource;
        while (sleepingTime < MAX_THREAD_SLEEP_TIME_MILLISECONDS) {
            try {
                Thread.sleep(THREAD_SLEEP_INTERVALL);
            } catch (InterruptedException e) {
                log.error(e, e);
            }
            tester.gotoPage("/plugins/servlet/project-config/" + projectKey + "/workflows");
            htmlSource = tester.getDialog().getResponseText();
            sleepingTime += THREAD_SLEEP_INTERVALL;
            if (htmlSource.contains(schemeName))
                break;
        }
        log.debug("Thread slept for " + sleepingTime + " milliseconds.");

        tester.gotoPage("/plugins/servlet/project-config/" + projectKey + "/workflows");
        tester.assertTextInElement("project-config-workflows-scheme-name", schemeName);

        log.info("Assigned project with key " + projectKey + " to workflow scheme with name " + schemeName + ".");
    }

    /**
     * Creates a new project.
     * 
     * @param projectName
     * @param projectKey
     * @return returns the projectID
     */
    protected long createNewProject(String projectName, String projectKey) {
        long projectID = administration.backdoor().project().addProject(projectName, projectKey, "admin");
        log.info("Created project with name: " + projectName + ", key: " + projectKey + ", ID: " + projectID + ".");
        return projectID;
    }

    /**
     * Creates a copy of the default jira workflow, creates a new workflow scheme and assignes the workflow to it.
     * 
     * @param schemeName name of the jira workflow copy
     * @param workflowName name of the new workflow scheme
     */
    protected void createWorkflowSchemeWithCopyOfJiraContained(String schemeName, String workflowName) {
        administration.backdoor().workflow().cloneWorkflow("jira", workflowName);
//		Long schemeId = administration.backdoor().workflowSchemes().copyScheme("classic", schemeName);
//		WorkflowSchemeData workflowScheme = administration.backdoor().workflowSchemes().getWorkflowScheme(schemeId);
		
        WorkflowSchemeData workflowScheme = new WorkflowSchemeData();
        workflowScheme.setDefaultWorkflow(workflowName);
        workflowScheme.setName(schemeName);
		administration.backdoor().workflowSchemes().createScheme(workflowScheme);
    }

    /**
     * Creates a new workflow and tests its existence.
     * 
     * @param workflowName name of the workflow that shall be created
     */
    protected void createNewWorkflow(String workflowName) {
        navigation.gotoWorkflows();
        tester.assertTextNotInTable("workflows_table", workflowName);
        tester.clickLink("add-workflow");

        WebForm f = form.getForms()[0];
        assertTrue(f.hasParameterNamed("newWorkflowName"));
        assertTrue(f.hasParameterNamed("description"));
        f.setParameter("newWorkflowName", workflowName);
        tester.clickButton("add-workflow-submit");

        navigation.gotoWorkflows();
        tester.assertLinkPresent("steps_live_" + workflowName);

        log.info("Created new workflow with name " + workflowName + ".");
    }

    /**
     * Creates a copy of the default jira workflow.
     * 
     * @param workflowName name of the jira workflow copy
     */
    protected void createCopyOfJiraWorkflow(String workflowName) {
        navigation.gotoWorkflows();
        //tester.assertTextNotInTable("workflows_table", workflowName);
        tester.assertTextNotInElement("ViewWorkflows", workflowName);
        tester.clickLink("copy_jira");

        WebForm f = form.getForms()[1];
        assertTrue(f.hasParameterNamed("newWorkflowName"));
        assertTrue(f.hasParameterNamed("description"));
        f.setParameter("newWorkflowName", workflowName);
        tester.submit("Update");

        navigation.gotoWorkflows();
        //tester.assertLinkPresent("steps_live_" + workflowName);
        tester.assertLinkPresent("edit_live_" + workflowName);

        log.info("Created new workflow (copy of Jira) with name " + workflowName + ".");
    }

    /**
     * Creates a new workflow scheme.
     * 
     * @param schemeName name of the new workflow scheme
     */
    protected void createNewWorkflowScheme(String schemeName) {
        navigation.gotoWorkflows();
        tester.clickLink("workflow_schemes_tab");
        tester.assertTextNotInTable("workflow_schemes_table", schemeName);

        tester.assertLinkPresent("add_workflowscheme");
        tester.clickLink("add_workflowscheme");

        WebForm f = form.getForms()[0];
        log.debug("Die ID der Form ist " + f.getID() + ".");

        assertTrue(f.hasParameterNamed("name"));
        assertTrue(f.hasParameterNamed("description"));
        f.setParameter("name", schemeName);

        tester.clickButton("add-workflow-scheme-submit");

        tester.clickLink("workflow_schemes_tab");
        tester.assertTextInTable("workflow_schemes_table", schemeName);

        log.info("Created new workflow scheme with name " + schemeName);
    }

    /**
     * Creates a new issue in an existing project.
     * 
     * @param projectName ProjectName
     * @param issueType e.g. Bug, Verbesserung, ... depends on language
     * @param summary summary / description of the issue
     * @return issueKey
     */
    protected String createNewIssue(String projectName, String issueType, String summary) {
        String issue_Key = "???";
        issue_Key = navigation.issue().createIssue(projectName, issueType, summary);

        navigation.issue().gotoIssue(issue_Key);

        log.info("Created issue with key " + issue_Key + " of type " + issueType + " in project with name "
                + projectName + ".");
        return issue_Key;
    }

    /**
     * Creates a new issue in an existing project.
     * 
     * @param projectName ProjectName
     * @param issueType e.g. Bug, Verbesserung, ... depends on language
     * @param summary summary / description of the issue
     * @param issueParam map of issue parameters
     * @return issueKey
     */
    protected String createNewIssue(String projectName, String issueType, String summary,
            Map<String, String[]> issueParam) {
        assertTrue(administration.project().projectWithKeyExists(projectName));
        String issue_Key = "???";
        issue_Key = navigation.issue().createIssue(projectName, issueType, summary, issueParam);
        navigation.issue().gotoIssue(issue_Key);

        log.info("Created issue with key " + issue_Key + " of type " + issueType + " in project with name "
                + projectName + ".");
        return issue_Key;
    }

    /**
     * Creates a new issue in an existing project.
     * 
     * @param projectKey ProjectKey
     * @param issueType e.g. Bug, Verbesserung, ... depends on language
     * @param summary summary / description of the issue
     */
    /* NOTE: Does not work in JIRA 5.2 anymore. You can navigate to "Create Issue" with
    		  tester.clickLink("create_link");
    		 but then you need to select the correct issueType, which now have numeric values
    		 instead of their name.
    */
    /*protected void createNewIssueWithoutKey(String projectKey, String issueType, String summary) {
        navigation.browseProject(projectKey);
        tester.assertLinkPresentWithText(issueType);
        tester.clickLinkWithText(issueType);
        tester.assertElementPresent("summary");

        WebForm f = form.getForms()[1];
        log.debug("Die ID der Form ist " + f.getID() + "."); // Has to be issue-create
        assertTrue(f.hasParameterNamed("summary"));
        f.setParameter("summary", summary);

        tester.setWorkingForm(f.getID());
        tester.assertButtonPresent("issue-create-submit");
        tester.clickButton("issue-create-submit");

        log.info("Created issue of type " + issueType + " in project with name " + projectKey + ".");
    }*/

    /**
     * Gets the projectId of the project with the given projectKey. There has to be a login before calling this method.
     * 
     * @param projectKey
     * @return projectId as String
     */
    protected String getProjectId(String projectKey) {
        tester.clickLink("edit_project");
        String pid = tester.getDialog().getFormParameterValue("pid");
        log.debug("The projectId of the project with key " + projectKey + " is " + pid + ".");
        return pid;
    }

    /**
     * Creates a new custom field of type fieldType and name fieldName.
     * 
     * @param fieldType
     * @param fieldName
     * @return String "customfield_(customfieldId)" where (customfieldId) is replaced by the id of the custom field.
     */
    protected String createNewCustomField(String fieldType, String fieldName) {
        String customfield_customfieldId = administration.customFields().addCustomField(fieldType, fieldName);
        log.info("Created custom field of type " + fieldType + " with name " + fieldName + " and id "
                + customfield_customfieldId + ".");
        return customfield_customfieldId;
    }

    protected String getJiraFieldExpression() {
        return "{" + FIELDNAME + "}";
    }

    public static Map<String, String[]> createMap(String key, String value) {
        Map<String, String[]> issueParam = new HashMap<String, String[]>();
        String[] arg = { value };
        issueParam.put(key, arg);
        return issueParam;
    }

    protected Map<String, String[]> setup(String key1, String value1, String key2, String value2) {
        Map<String, String[]> result = createMap(key1, value1);
        String[] arg = { value2 };
        result.put(key2, arg);
        return result;
    }

    // ///////////////////////////////////////
    // Methods that need special test data //
    // ///////////////////////////////////////

    /**
     * Goes to the screen for adding a validation for the start progress link in step Open. Assigned workflow should be
     * copy of Jira. There has to be a login before use of the method. The workflow has to be inactive.
     * 
     * @param workflowName
     */
    protected void goToTransitionCreationStartProgress_Validation(String workflowName) {
        navigation.gotoWorkflows();
        //tester.clickLink("steps_live_" + workflowName);
        tester.clickLink("edit_live_" + workflowName);
        tester.clickLink("workflow-text");
        tester.clickLink("edit_action_1_4");
        tester.clickLink("view_validators");
        tester.clickLink("add-workflow-validator");
        tester.assertRadioOptionValuePresent("type", "com.tngtech.jira.plugins.workflowenhancer:universalvalidator");
        tester.checkRadioOption("type", "com.tngtech.jira.plugins.workflowenhancer:universalvalidator");
        tester.clickButton("add_submit");

        log.debug("Done - goToTransitionCreationStartProgress_Validation");
    }

    /**
     * Goes to the screen for adding a condition for the start progress link in step Open. Assigned workflow should be
     * copy of Jira. There has to be a login before use of the method. The workflow has to be inactive.
     * 
     * @param workflowName
     */
    protected void deleteDefaultCondition(String workflowName) {
        navigation.gotoWorkflows();
        //tester.clickLink("steps_live_" + workflowName);
        tester.clickLink("edit_live_" + workflowName);
        
        tester.clickLink("workflow-text");
        tester.clickLink("edit_action_1_4");
        log.debug("Done - edit_action_1_4");

        tester.assertTextPresent("issue can execute this transition.");
        tester.clickLinkWithTextAfterText("Delete", "issue can execute this transition.");

        log.info("Done - deleteDefaultCondition");

    }

    /**
     * Goes to the screen for adding a validation for the start progress link in step Open. Assigned workflow should be
     * copy of Jira. There has to be a login before use of the method. The workflow has to be inactive.
     * 
     * @param workflowName
     */
    protected void goToTransitionCreationStartProgress_Condition(String workflowName) {
        navigation.gotoWorkflows();
        //tester.clickLink("steps_live_" + workflowName);
        tester.clickLink("edit_live_" + workflowName);
        tester.clickLink("workflow-text");
        tester.clickLink("edit_action_1_4");
        navigation.gotoWorkflows();
        //tester.clickLink("steps_live_" + workflowName);
        tester.clickLink("edit_live_" + workflowName);
        tester.clickLink("workflow-text");
        tester.clickLink("edit_action_1_4");

        tester.clickLink("add-workflow-condition");
        log.info("Done - add-workflow-condition");
        tester.assertRadioOptionValuePresent("type", "com.tngtech.jira.plugins.workflowenhancer:universalcondition");
        tester.checkRadioOption("type", "com.tngtech.jira.plugins.workflowenhancer:universalcondition");
        log.debug("Done - checkRadio Option");

        tester.clickButton("add_submit");
        log.debug("Done - goToTransitionCreationStartProgress_Condition");
    }

    // /////////////////////////////////
    // Methods only for TestData.zip //
    // /////////////////////////////////

    protected void setScheme() {
        tester.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY + "/workflows");
        tester.clickLink("project-config-workflows-scheme-change");
        tester.setWorkingForm(FunctTestConstants.JIRA_FORM_NAME);
        tester.selectOption("schemeId", SCHEME_NAME);

        tester.clickButton("associate_submit");
        tester.clickButton("associate_submit");

        int sleepingTime = 0;
        String htmlSource;
        while (sleepingTime < MAX_THREAD_SLEEP_TIME_MILLISECONDS) {
            try {
                Thread.sleep(THREAD_SLEEP_INTERVALL);
            } catch (InterruptedException e) {
                log.error(e, e);
            }
            tester.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY + "/workflows");
            htmlSource = tester.getDialog().getResponseText();
            sleepingTime += THREAD_SLEEP_INTERVALL;
            if (htmlSource.contains(SCHEME_NAME))
                break;
        }
        log.debug("Thread slept for " + sleepingTime + " milliseconds.");

        tester.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY + "/workflows");
        tester.assertTextInElement("project-config-workflows-scheme-name", SCHEME_NAME);
    }

    protected void goToTransitionCreation_Validation() {
        navigation.gotoWorkflows();
        tester.clickLink("steps_live_TestWorkflow2");
        tester.clickLink("edit_action_1_4");
        tester.clickLink("view_validators");
        tester.clickLink("add-workflow-validator");
        tester.assertRadioOptionValuePresent("type", "com.tngtech.jira.plugins.workflowenhancer:universalvalidator");
        tester.checkRadioOption("type", "com.tngtech.jira.plugins.workflowenhancer:universalvalidator");
        tester.clickButton("add_submit");

        log.debug("Done - goToTransitionCreation_Validation");
    }

    protected void goToTransitionCreation_Condition() {
        navigation.gotoWorkflows();
        tester.clickLink("steps_live_TestWorkflow2");
        tester.clickLink("edit_action_1_4");
        tester.clickLink("add-workflow-condition");
        tester.assertRadioOptionValuePresent("type", "com.tngtech.jira.plugins.workflowenhancer:universalcondition");
        tester.checkRadioOption("type", "com.tngtech.jira.plugins.workflowenhancer:universalcondition");
        tester.clickButton("add_submit");

        log.debug("Done - goToTransitionCreation_Condition");
    }

    protected void useUserFred() {
        navigation.login("fred", "password");
        // Issue is currently open
        navigation.issue().viewIssue(ISSUE_KEY);

        String htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(ISSUE_NAME));
        // The boolean expression {Aktualisiert}>[now] must evaluate to .
        tester.assertLinkNotPresent(RESOLVE_LINK_ID);
        // The boolean expression {Autor}=[user] must evaluate to.
        tester.assertLinkNotPresent(START_PROGRESS_LINK_ID);
        // No conditions
        tester.assertLinkPresent(CLOSE_LINK_ID);

        navigation.logout();
    }

    protected void useUserAdmin() {
        String htmlSource;
        navigation.login("admin", "admin");
        navigation.issue().viewIssue(ISSUE_KEY);
        
        htmlSource = tester.getDialog().getResponseText();
        assertTrue(htmlSource.contains(ISSUE_NAME));
        tester.assertLinkNotPresent(RESOLVE_LINK_ID);
        // Since admin is the author
        tester.assertLinkPresent(START_PROGRESS_LINK_ID);
        tester.assertLinkPresent(CLOSE_LINK_ID);

        tester.clickLink(START_PROGRESS_LINK_ID);

        tester.assertLinkPresent(STOP_PROGRESS_LINK_ID);
        tester.assertLinkPresent(CLOSE_LINK_ID);

        navigation.logout();
    }

    protected void useUserEnglish() {
        navigation.login("english", "english");
        navigation.issue().viewIssue(ISSUE_KEY);

        tester.assertLinkPresent(STOP_PROGRESS_LINK_ID);
        tester.assertLinkPresent(CLOSE_LINK_ID);

        tester.clickLink(STOP_PROGRESS_LINK_ID);

        tester.assertLinkPresent(CLOSE_LINK_ID);

        navigation.logout();
    }

}
