package it.com.tngtech.jira.workflowenhancer.validator;

import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.meterware.httpunit.WebForm;

public class UserPickerCustomFieldValidationTest extends ValidationTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testUserpickerCustomFieldTrivialCondition() throws Exception {
        assertTrue(userPickerCustomFieldValidationTest(USERPICKER_CUSTOMFIELD_NAME, USERNAME3, FULLNAME3, USERNAME4,
                "1=1", true));
    }

    @Test
    public void testUserpickerCustomFieldEqualsUsername() throws Exception {
        assertTrue(userPickerCustomFieldValidationTest(USERPICKER_CUSTOMFIELD_NAME, USERNAME3, FULLNAME3, USERNAME4,
                "{" + USERPICKER_CUSTOMFIELD_NAME + "} = \"" + USERNAME3 + "\"", true));
    }

    @Test
    public void testUserpickerCustomFieldEqualsFalseUsername() throws Exception {
        assertTrue(userPickerCustomFieldValidationTest(USERPICKER_CUSTOMFIELD_NAME, USERNAME3, FULLNAME3, USERNAME4,
                "{" + USERPICKER_CUSTOMFIELD_NAME + "} == \"" + USERNAME4 + "\"", false));
    }

    @Test
    public void testUserpickerCustomFieldNotEqualsAssignee() throws Exception {
        assertTrue(userPickerCustomFieldValidationTest(USERPICKER_CUSTOMFIELD_NAME, USERNAME3, FULLNAME3, USERNAME4,
                "{" + USERPICKER_CUSTOMFIELD_NAME + "} != {Assignee}", true));
    }

    @Test
    public void testUserpickerCustomFieldEqualsReporter() throws Exception {
        assertTrue(userPickerCustomFieldValidationTest(USERPICKER_CUSTOMFIELD_NAME, USERNAME3, FULLNAME3, USERNAME3,
                "{" + USERPICKER_CUSTOMFIELD_NAME + "} == {Reporter}", true));
    }

    /**
     * Tests if universal validations with userpicker customfields work correctly. Adds users with USERNAME3 and
     * USERNAME4. Creates a customfield, sets its value, sets the reporter, evaluates the condition.
     * 
     * @param fieldName name of the customfield
     * @param username of the user in the customfield
     * @param fullname of the user in the customfield
     * @param reporterUsername username of the reporter
     * @param condition the usual boolean / regular /... expression
     * @param condIsTrue is true if and only if the condition is true
     * @return true
     */
    public boolean userPickerCustomFieldValidationTest(String fieldName, String username, String fullname,
            String reporterUsername, String condition, boolean condIsTrue) {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        administration.usersAndGroups().addUser(USERNAME3, PASSWORD3, FULLNAME3, EMAILADDRESS3);
        administration.usersAndGroups().addUser(USERNAME4, PASSWORD4, FULLNAME4, EMAILADDRESS4);
        administration.usersAndGroups().addUserToGroup(USERNAME3, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME4, GROUP_NAME_ADMIN);
        administration.usersAndGroups().addUserToGroup(USERNAME3, GROUP_NAME_DEVELOPERS);
        administration.usersAndGroups().addUserToGroup(USERNAME4, GROUP_NAME_DEVELOPERS);

        // Create customfield
        String customfield_customfieldId = createNewCustomField(USERPICKER_ID, fieldName);
        log.debug("Done - created customfields");

        // Set issue parameters and create issue
        Map<String, String[]> issueParam = createMap("reporter", reporterUsername);
        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME, issueParam);
        log.debug("Done - created issue");

        // Set customfield
        navigation.issue().gotoEditIssue(issueKey);
        WebForm editForm = form.getForms()[1];
        assertTrue(editForm.hasParameterNamed("summary"));
        assertTrue(editForm.hasParameterNamed(customfield_customfieldId));
        editForm.setParameter(customfield_customfieldId, username);
        tester.submit("Update");

        // Test if customfield is set
        navigation.issue().gotoIssue(issueKey);
        tester.assertElementPresent(customfield_customfieldId + "-val");
        tester.assertTextInElement(customfield_customfieldId + "-val", fullname);

        log.info("Customfield is set.");

        return defaultValidationTestAfterInitializing(issueKey, condition, condIsTrue);
    }

}
