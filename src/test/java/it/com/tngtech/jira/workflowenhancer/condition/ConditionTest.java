package it.com.tngtech.jira.workflowenhancer.condition;

import it.com.tngtech.jira.workflowenhancer.UniversalTestConstants;
import it.com.tngtech.jira.workflowenhancer.WorkflowenhancerUtil;

import java.util.Map;

import org.junit.Ignore;

import com.meterware.httpunit.WebForm;

@Ignore
public class ConditionTest extends WorkflowenhancerUtil implements UniversalTestConstants {

    /**
     * Tests if conditions with jiraArrays work correctly
     * 
     * @param issueParam some parameters of the issue
     * @param condition the condition
     * @param condIsCorrect boolean, which tells if the condition is correct or not
     * @return if no assertion failed, true is given back
     */
    public boolean jiraArrayConditionTest(Map<String, String[]> issueParam, String condition, boolean condIsCorrect) {
        //Setup
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY1);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        String issueKey = "";
        issueKey = createNewIssue(PROJECT_NAME, ISSUE_TYPE, ISSUE_NAME, issueParam);
        log.debug("Done - Setup ");

        return defaultConditionTestAfterInitializing(issueKey, condition, condIsCorrect);
    }

    /**
     * Sets a condition and evaluates, if the condition is working for the issue. If the condition is valid the html
     * source must contain the START_PROGRESS_LINK_ID and if the condition is not valid this id must not be there.
     * 
     * @param issueKey
     * @param condition
     * @param condIsCorrect
     * @return if no assertion failed true is given back, if an assertion fails the test fails
     */
    public boolean defaultConditionTestAfterInitializing(String issueKey, String condition, boolean condIsCorrect) {
        //Set Condition
        goToTransitionCreationStartProgress_Condition(WORKFLOW_NAME);

        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, condition);
        log.debug("Done - set Parameter");
        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");

        //Assign SchemeTo Project
        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
        //assignSchemeToProject(PROJECT_KEY, SCHEME_NAME);

        //Evaluation
        navigation.issue().viewIssue(issueKey);
        String htmlSource = tester.getDialog().getResponseText();
        log.debug("Done - getResponseText");
        assertTrue(htmlSource.contains(ISSUE_NAME));
        if (condIsCorrect) {
            assertTrue(htmlSource.contains(START_PROGRESS_LINK_ID));
        } else {
            assertFalse(htmlSource.contains(START_PROGRESS_LINK_ID));
        }
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));
        navigation.logout();
        return true;
    }
}
