package it.com.tngtech.jira.workflowenhancer.condition;

import java.util.Map;

import org.junit.Test;

public class StringConditionTest extends ConditionTest {

    @Test
    public void testDescriptionEntry() {
        assertTrue(jiraArrayConditionTest(createMap("description", "Test"), "5==5", true));
    }

    @Test
    public void testDescriptionEqualsString() {
        assertTrue(jiraArrayConditionTest(createMap("description", "Test"), "{Description}==\"Test\"", true));
    }
    
    @Test
    public void testSummaryEqualsString() {
        assertTrue(jiraArrayConditionTest(createMap("description", "Test"), "{Summary}==\"" + ISSUE_NAME + "\"", true));
    }

    @Test
    public void testEnvironmentNotEqualsEmptyString() {
        assertTrue(jiraArrayConditionTest(createMap("environment", "Rettet die Wale"), "{Environment}!=\"\"", true));
    }

    @Test
    public void testEnvironmentEqualsRegEx() {
        assertTrue(jiraArrayConditionTest(createMap("environment", "Waaaale"), "{Environment} ==/W?a*(le)+/", true));
    }

    @Test
    public void testEnvironmentEqualsFalseRegEx() {
        assertTrue(jiraArrayConditionTest(createMap("environment", "Waaaale"), "{Environment}==/(Green)*p?ea+ce/",
                false));
    }

    @Test
    public void testEnvironmentEqualsDescription() {
        Map<String, String[]> map = createMap("environment", "Test");
        String[] value = { "Test" };
        map.put("description", value);
        assertTrue(jiraArrayConditionTest(map, "{Environment}=={Description}", true));
    }

    @Test
    public void testKeyNotEqualsEmptyString() {
        assertTrue(jiraArrayConditionTest(createMap("environment", "Test"), "{Key}!=\"\"", true));
    }

    @Test
    public void testIssueTypeEquals() {
        assertTrue(jiraArrayConditionTest(createMap("description", ""), "{Issue Type}==\"" + ISSUE_TYPE + "\"", true));
    }

    // Hier muss man aufpassen. Das Description-Feld enthält einen String. Also muss man eigentlich auch einen String übergeben. Daher "\"....\"".
    @Test
    public void testDescriptionEqualsDate() {
        assertTrue(jiraArrayConditionTest(createMap("description", "\"3w 4d 5h\""), "{Description}==\"3w 4d 5h\"", true));
    }
}