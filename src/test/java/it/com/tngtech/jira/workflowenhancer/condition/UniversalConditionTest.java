package it.com.tngtech.jira.workflowenhancer.condition;

import it.com.tngtech.jira.workflowenhancer.UniversalTestConstants;
import it.com.tngtech.jira.workflowenhancer.WorkflowenhancerUtil;

import org.junit.Test;

import com.meterware.httpunit.WebForm;


public class UniversalConditionTest extends WorkflowenhancerUtil implements UniversalTestConstants {

    @Test
    public void testInterface() {
        restore("TestData.zip");
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // goToTransitionCreation_Condition();
        goToTransitionCreationStartProgress_Condition("TestWorkflow2");

        tester.assertElementPresent("fields");
        tester.assertElementPresent("evaluateTo");
        tester.assertElementPresent("add_submit");
        tester.assertElementPresent("cancelButton");

        navigation.logout();
    }

    @Test
    public void testInterface2() {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME2, PROJECT_KEY2, SCHEME_NAME2, WORKFLOW_NAME2, ISSUE_TYPE,
                SUMMARY2);

        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        createCopyOfJiraWorkflow(WORKFLOW_NAME);
        goToTransitionCreationStartProgress_Condition(WORKFLOW_NAME);

        tester.assertElementPresent("fields");
        tester.assertElementPresent("evaluateTo");
        tester.assertElementPresent("add_submit");
        tester.assertElementPresent("cancelButton");

        navigation.logout();
    }

    @Test
    public void testFalseExpression() {
        conditionFailureTest("1=2", true);
    }

    @Test
    public void testJustOpeningBrace() {
        conditionFailureTest("1={1", false);
    }

    @Test
    public void testOddNumberOfBraces() {
        conditionFailureTest("1={{1}", false);
    }

    @Test
    public void testRightOperandMissing() {
        conditionFailureTest("1=", false);
    }

    @Test
    public void testOddNumberOfBrackets() {
        conditionFailureTest("1={[1}", false);
    }

    @Test
    public void testOnlyNumberInserted() {
        conditionFailureTest("1", false);
    }

    @Test
    public void testOperandMissingChars() {
        conditionFailureTest("a=", false);
    }

    @Test
    public void testInvalidExpression() {
        conditionFailureTest("a=a", false);
    }

    @Test
    public void testTwoEqualSigns() {
        conditionFailureTest("1=1=1", false);
    }

    @Test
    public void testNoBooleanExpresionInserted() {
        conditionFailureTest("\"1=2\"", false);
    }

    @Test
    public void testIncompatibleTypes() {
        conditionFailureTest("\"\"=0", true);
    }

    @Test
    public void testIncompatibleTypesWithOr() {
        conditionFailureTest("\"\"=0|1", false);
    }

    @Test
    public void testConditionsWithDifferentUsers() {
        restore("TestData.zip");
        useUserFred();
        useUserAdmin();
        useUserEnglish();
    }

    private boolean conditionFailureTest(String expression, boolean evaluateTo) {
        restore("TestData.zip");
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);

        // goToTransitionCreation_Condition();
        goToTransitionCreationStartProgress_Condition("TestWorkflow2");

        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, expression);
        log.debug("Done - set Parameter");
        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");

        String htmlSource = tester.getDialog().getResponseText();
        assertTrue(!(htmlSource.contains("id=\"JWFEerror\"") == evaluateTo));
        log.debug("Done - testHtmlCode");

        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
        //setScheme();
        log.debug("Done - setScheme");

        navigation.issue().viewIssue(ISSUE_KEY);
        htmlSource = tester.getDialog().getResponseText();
        log.debug("Done - getResponseText");

        assertTrue(htmlSource.contains(ISSUE_NAME));
        assertFalse(htmlSource.contains(START_PROGRESS_LINK_ID));
        assertTrue(htmlSource.contains(CLOSE_LINK_ID));

        navigation.logout();
        return true;
    }
}
