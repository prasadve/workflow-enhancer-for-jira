package it.com.tngtech.jira.workflowenhancer.validator;

import org.junit.Test;

import com.meterware.httpunit.WebForm;

public class CustomFieldSelectionTest extends ValidationTest {

    /**
     * Doesn't test anything. Creates a test environment with several custom fields which have to be checked by the
     * programmer.
     */
    @Test
    public void testCF() {
        createTestEnvironment(DEFAULT_JIRA, PROJECT_NAME, PROJECT_KEY, SCHEME_NAME, WORKFLOW_NAME, ISSUE_TYPE, SUMMARY2);
        navigation.login(USERNAME_ADMIN, PASSWORD_ADMIN);
        createNewIssue(PROJECT_NAME, ISSUE_TYPE, "TEST");

        // Create customfield
        createNewCustomField(USERPICKER_ID, "UserPicker_definded_by_Me");
        createNewCustomField(DATAPICKER_ID, "DatePicker_ also_defined_by_me");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker",
                "MultiUserPicker:this_one_too");
        createNewCustomField("com.atlassian.jira.plugins.jira-importers-plugin:bug-importid", "BugImport");

        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect", "CascadingSelect");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:importid", "ImportID");
        createNewCustomField("com.atlassian.jirafisheyeplugin:jobcheckbox", "Job Checkbox");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes", "MultiCheckboxes ");

        createNewCustomField("com.atlassian.jirafisheyeplugin:hiddenjobswitch", "Hidden Job Switch");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:multiselect", "MultiSelect");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons", "RadioButtons");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:readonlyfield", "Read-Only-Text-Field");
        createNewCustomField("com.atlassian.jira.plugin.system.customfieldtypes:select", "SelectList");

        goToTransitionCreationStartProgress_Validation(WORKFLOW_NAME);
        //Set Validation
        tester.setWorkingForm(JIRAFORM_WORKINGFORM_ID);
        WebForm f = form.getForms()[0];
        assertTrue(f.getName().equals(JIRAFORM_WORKINGFORM_ID));
        assertTrue(f.hasParameterNamed(EXPRESSION_SOUGHTNAME));
        f.setParameter(EXPRESSION_SOUGHTNAME, "5==5");
        log.debug("Done - set Parameter");

        tester.clickButton(ADD_SUBMIT_PARAMETER_BUTTON_ID);
        log.debug("Done - add_submit");
        //Assign workflow to project
        administration.project().associateWorkflowScheme(PROJECT_NAME, SCHEME_NAME);
    }

}
