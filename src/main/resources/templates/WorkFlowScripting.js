var TNG_JWFE = (function() {
    var availableUtilityFunctions = {
        "clickedSyntax" : clickedSyntax(),
    };

    //inserts clicked values into the boolean expression
    function clickedSyntax() {
        function insert(myValue, field, isNotIe) {
            if (isNotIe) {
                return field + myValue;
            }
            return field.text + myValue;
        }

        function enclose(myValue, field, isNotIe) {
            if (isNotIe) {
                return "(" + field + ")";
            }
            return "(" + field.text + ")";
        }

        function clickedSyntax(myValue) {
            var myField = document.getElementById("expression"), 
                insertOrEnclose = (myValue === "()"    ? enclose : insert),
                sel, startPos, endPos;

            // IE support
            if (document.selection) {
                myField.focus();
                sel = document.selection.createRange();
                sel.text = insertOrEnclose(myValue, sel);
            }

            // GECKO/WEBKIT support
            else if (myField.selectionStart || myField.selectionStart == '0') {
                startPos = myField.selectionStart;
                endPos = myField.selectionEnd;
                myField.value = myField.value.substring(0, startPos)
                        + insertOrEnclose(myValue, myField.value.substring(
                                        startPos, endPos), true)
                        + myField.value.substring(endPos, myField.value.length);
            } else {
                myField.value += myValue;
            }
        }
        return clickedSyntax;
    }
    
    return availableUtilityFunctions;
}());
