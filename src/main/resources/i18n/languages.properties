workflow.jwfe.name.condition = Universal Condition
workflow.jwfe.name.validator = Universal Validator
workflow.jwfe.name.postfunction = Universal Post Function

workflow.jwfe.description.condition = Condition that depends on a Boolean expression. 
workflow.jwfe.description.validator = Validator that depends on a Boolean expression. 
workflow.jwfe.description.postfunction = Executes any post function to be executed depending on a Boolean expression.

workflow.jwfe.view.expression.description = Expression:
workflow.jwfe.view.expression.hint = You can enter a boolean expression here. For details, see below.
workflow.jwfe.view.expression.insert.clickhere = (Click here to insert a field name!)
workflow.jwfe.view.expression.labels = Labels
workflow.jwfe.view.expression.transition.comment = Transition comment
workflow.jwfe.view.expression.components = Components
workflow.jwfe.view.expression.macros.now = [now]-macro
workflow.jwfe.view.expression.macros.user = [user]-macro
workflow.jwfe.view.expression.macros.groups = [groups]-macro
workflow.jwfe.view.expression.macros.roles = [roles]-macro
workflow.jwfe.view.expression.macros.subtasks.number = [numberSubtasks]-macro
workflow.jwfe.view.expression.macros.subtasks.field = [subtasks#field]-macro

workflow.jwfe.view.expression.expressionIsEmpty = Expression is empty

workflow.jwfe.view.evaluateto.description = Expression must evaluate to:
workflow.jwfe.view.evaluateto.true = True
workflow.jwfe.view.evaluateto.false = False
workflow.jwfe.view.evaluateto.hint = Choose whether the expression must evaluate to true or false for the condition to hold.

workflow.jwfe.view.postfunction.choose = Choose post function to execute:
workflow.jwfe.view.postfunction.selection = -- Please choose a post function --

workflow.jwfe.view.validator.message.description = Message:
workflow.jwfe.view.validator.message.hint = Enter the message you want to be displayed if the input is invalid.

workflow.jwfe.view.syntax.title = Valid Syntax:
workflow.jwfe.view.syntax.hint = (Click on the blue text to insert it into the expression)

workflow.jwfe.view.syntax.values.title = Values
workflow.jwfe.view.syntax.values.field = Field: <b>&#123;Field Name&#125;</b> or &#123;fieldID&#125; (field values will be evaluated)
workflow.jwfe.view.syntax.values.nonevalfield = Non-evaluating Field: <b>&#123;&#123;Field Name&#125;&#125;</b> or &#123;&#123;fieldID&#125;&#125; (field values will not be evaluated)
workflow.jwfe.view.syntax.values.string = String: <b>&quot;...&quot;</b> (Any string has to be enclosed by <b>&quot;</b> (quotation marks))
workflow.jwfe.view.syntax.values.regex = RegEx: <b>/.../</b> (Regular expressions have to be enclosed by <b>/</b> (forward slashes))
workflow.jwfe.view.syntax.values.float = Floating point number: <b>3.14</b>
workflow.jwfe.view.syntax.values.time = Time: <b>24.12.2009 14:30</b> or <b>24.12.2009</b>
workflow.jwfe.view.syntax.values.worktimespan = Labor Timespan (adapted to JIRA timespans): <b>1w 2d 3h 4min</b> (One or more units may be omitted, but the order must not be changed)
workflow.jwfe.view.syntax.values.realtimespan = Real Timespan (a day has 24 hours, a week 7 days): <b>1W 2D 3H 4MIN</b>

workflow.jwfe.view.syntax.fields.title = Hint for JIRA-Fields
workflow.jwfe.view.syntax.fields.hint = Please compare JIRA-Fields always with English field values
workflow.jwfe.view.syntax.fields.example = e.g. &#123;Resolution&#125;&nbsp;==&nbsp;"Resolved"

workflow.jwfe.view.syntax.macros.title = Macros
workflow.jwfe.view.syntax.macros.now = for the time at which the condition is evaluated.
workflow.jwfe.view.syntax.macros.user = for the user who is logged in at evaluation.
workflow.jwfe.view.syntax.macros.groups = for the groups of the user who is logged in at evaluation, e.g. [jira-users, group1].
workflow.jwfe.view.syntax.macros.roles = for the roles of the user who is logged in at evaluation, e.g. [role1, role3].
workflow.jwfe.view.syntax.macros.numberSubtasks = for the number of subtasks of the issue.
workflow.jwfe.view.syntax.macros.subtasksfield = for a list of the field values of &#123;&#123;field&#125;&#125; (not evaluated) of every subtask of the current issue.

workflow.jwfe.view.syntax.booleans.title = Boolean operators
workflow.jwfe.view.syntax.booleans.and = (alternatively &amp;&amp; or AND)
workflow.jwfe.view.syntax.booleans.or = (alternatively || or OR)
workflow.jwfe.view.syntax.booleans.not = not...
workflow.jwfe.view.syntax.booleans.brackets = 

workflow.jwfe.view.syntax.order.title = Order/equality operators
workflow.jwfe.view.syntax.order.equals = (alternatively ==)
workflow.jwfe.view.syntax.order.nequals = 
workflow.jwfe.view.syntax.order.less = or
workflow.jwfe.view.syntax.order.greater = or

workflow.jwfe.view.syntax.arithmetic.title = Arithmetic operators
workflow.jwfe.view.syntax.arithmetic.plusminus = or

workflow.jwfe.view.syntax.documentationtext = You can find more detailed information about the syntax in our 
workflow.jwfe.view.syntax.documentationlinktext = documentation

workflow.jwfe.errors.parsing.main = Parsing of the expression has failed. Please check its syntax.
workflow.jwfe.errors.parsing.youentered = You entered:
workflow.jwfe.errors.parsing.alwaysfalse = This expression will always evaluate to <b>false!</b>
workflow.jwfe.errors.parsing.sysmsg = System message:

workflow.jwfe.errors.missingordering = No ordering or equality operator (such as = < >) found in your expression!
workflow.jwfe.errors.wrongsquare = There are pairs of '[...]' which are not used correctly.

workflow.jwfe.errors.brackets.searchfor = searching for
workflow.jwfe.errors.brackets.butfound = but found
workflow.jwfe.errors.brackets.atpos = at position
workflow.jwfe.errors.brackets.missing = Not every bracket is closing correctly, syntax error while looking for
workflow.jwfe.errors.brackets.and = and

workflow.jwfe.viewtransition.valcon.first = The Boolean expression
workflow.jwfe.viewtransition.valcon.second = must evaluate to
workflow.jwfe.viewtransition.valcon.third = .

workflow.jwfe.viewtransition.postfunction.first = If the Boolean expression
workflow.jwfe.viewtransition.postfunction.second = evaluates to
workflow.jwfe.viewtransition.postfunction.third = , call the post function 
workflow.jwfe.viewtransition.postfunction.fourth = .