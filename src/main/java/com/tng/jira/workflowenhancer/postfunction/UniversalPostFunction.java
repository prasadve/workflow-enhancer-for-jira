package com.tng.jira.workflowenhancer.postfunction;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.FunctionProvider;
import com.opensymphony.workflow.TypeResolver;
import com.opensymphony.workflow.WorkflowException;
import com.tng.jira.workflowenhancer.condition.UniversalCondition;
import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;
import com.tng.jira.workflowenhancer.evaluator.parser.TokenMgrError;
import com.tng.jira.workflowenhancer.evaluator.types.AbstractTimespanValue;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the args Map.
 */

public class UniversalPostFunction extends AbstractJiraFunctionProvider {
    
    private final FieldManager fieldManager;
    private final CustomFieldManager customFieldManager;
    public final IssueLinkManager issueLinkManager;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final ProjectRoleManager projectRoleManager;
    private final static Logger log = Logger.getLogger(UniversalCondition.class);
    
    public UniversalPostFunction(FieldManager fieldManager, CustomFieldManager customFieldManager,
    		IssueLinkManager issueLinkManager, TimeTrackingConfiguration timeTrackingConfiguration,
    		GroupManager groupManager, UserManager userManager, ProjectRoleManager projectRoleManager) {
        this.fieldManager = fieldManager;
        this.customFieldManager = customFieldManager;
        this.issueLinkManager  = issueLinkManager;
        this.groupManager = groupManager;
        this.userManager = userManager;
        this.projectRoleManager = projectRoleManager;
        AbstractTimespanValue.numberOfDaysPerWorkingWeek = timeTrackingConfiguration.getDaysPerWeek().floatValue();
        AbstractTimespanValue.numberOfHoursPerWorkingDay = timeTrackingConfiguration.getHoursPerDay().floatValue();
    }
    
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
    	
        MutableIssue issue = getIssue(transientVars);
        String callerName  = getCallerUser(transientVars, args).getName();
        String expression  = (String) args.get("expression");
        boolean evaluateTo = (args.get("evaluateTo")).equals("true");

        if(postfunctionShouldBeExecuted(expression, evaluateTo, callerName, issue, transientVars)) {
            String paramsForPostFunctionToExecuteJson = (String) args.get("paramsForPostFunctionToExecute");
            Gson gson = new Gson();
            Map paramsForPostFunctionToExecute = gson.fromJson(
                    paramsForPostFunctionToExecuteJson,
                    new TypeToken<Map<String, Object>>() {}.getType());
            String postfunctionClassName = (String) args.get("postfunctionClassName");

            Map<String, String> argsForPostFunctionToExecute = new HashMap<String, String>();
            argsForPostFunctionToExecute.put("class.name", postfunctionClassName);

            TypeResolver resolver = TypeResolver.getResolver();
            FunctionProvider functionProvider = resolver.getFunction("class", argsForPostFunctionToExecute);
            functionProvider.execute(transientVars, paramsForPostFunctionToExecute, ps);
        }
        
    }
    
    
    private boolean postfunctionShouldBeExecuted(String expression, boolean evaluateTo, String callerName, Issue issue, Map transientVars) {
        try {
            BooleanEvaluator booleanEvaluator = new BooleanEvaluator(fieldManager, customFieldManager,
            		issueLinkManager, groupManager, userManager, projectRoleManager, callerName, issue, transientVars);
            boolean evaluationResult = booleanEvaluator.evaluate(expression);

            return evaluationResult == evaluateTo;
        } catch (Exception e) { //Exceptions in this step can cause severe trouble and should by all means be caught.
            log.error(e, e);

            return false;
        }
    }
    
}