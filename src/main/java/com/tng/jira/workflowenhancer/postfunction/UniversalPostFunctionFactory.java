package com.tng.jira.workflowenhancer.postfunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import com.atlassian.jira.plugin.workflow.JiraWorkflowPluginConstants;
import com.atlassian.jira.plugin.workflow.WorkflowFunctionModuleDescriptor;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.predicate.EnabledModulePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorOfTypePredicate;
import com.atlassian.plugin.predicate.ModuleDescriptorPredicate;
import com.google.gson.Gson;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.tng.jira.workflowenhancer.AbstractUniversalWorkflowPluginFactory;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class UniversalPostFunctionFactory extends AbstractUniversalWorkflowPluginFactory implements WorkflowPluginFunctionFactory{

    public static final String POSTFUNCTIONS = "postfunctions";
    public static final String POSTFUNCTION_KEY = "postfunctionKey";
    public static final String POSTFUNCTION_NAME = "postfunctionName";
    public static final String POSTFUNCTION_CLASS_NAME = "postfunctionClassName";
    public static final String PARAMS_FOR_POSTFUNCTION_TO_EXECUTE = "paramsForPostFunctionToExecute";
    
    /*
     * In JIRA some post functions are used internally to e.g. update the issue status of an issue.
     * By default the Universal Post Function interface will display all possible postfunctions,
     * including those used by JIRA internally. On the one hand these have in their current form no usecases,
     * on the other hand the postfunctions' names (e.g. Create Comment, Create Issue) could lead users to
     * the impression that the addon isn't working properly, which is why we want to hide these postfunctions.
     */
    private static final String[] nonfunctionalPostfunctions = {
    	"com.atlassian.jira.workflow.function.issue.UpdateIssueStatusFunction", //Updates Issue Status a second time
    	"com.atlassian.jira.workflow.function.misc.CreateCommentFunction", //Creates a second comment if a comment was entered during transition
    	"com.atlassian.jira.workflow.function.issue.IssueCreateFunction", //Recreates the issue
    	"com.atlassian.jira.workflow.function.issue.GenerateChangeHistoryFunction", //Change history of issue is already generated automatically
    	"com.atlassian.jira.workflow.function.issue.IssueStoreFunction" //Issue is already stored
    };
    
    public UniversalPostFunctionFactory(
            ModuleDescriptorFactory   moduleDescriptorFactory,
            PluginAccessor            pluginAccessor,
            JiraAuthenticationContext jiraAuthenticationContext
    ){
        super(jiraAuthenticationContext);
        this.moduleDescriptorFactory = moduleDescriptorFactory;
        this.pluginAccessor          = pluginAccessor;
    }
    
    private PluginAccessor pluginAccessor;
    private ModuleDescriptorFactory moduleDescriptorFactory;
    
    private static class ModuleAndDescriptorPredicate<T> implements ModuleDescriptorPredicate<T> {
        private ModuleDescriptorPredicate<T> leftOperand;
        private ModuleDescriptorPredicate<T> rightOperand;

        private ModuleAndDescriptorPredicate(ModuleDescriptorPredicate<T> leftOperand, ModuleDescriptorPredicate<T> rightOperand) {
            this.leftOperand  = leftOperand;
            this.rightOperand = rightOperand;
        }

        @Override
        public boolean matches(ModuleDescriptor<? extends T> moduleDescriptor) {
            return leftOperand.matches(moduleDescriptor) && rightOperand.matches(moduleDescriptor);
        }
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams){
        Collection<ModuleDescriptor<WorkflowPluginFunctionFactory>> moduleDescriptors = getFunctionalModuleDescriptors();
        super.getVelocityParamsForInput(velocityParams);

        velocityParams.put(POSTFUNCTIONS, moduleDescriptors);
    }

    private Collection<ModuleDescriptor<WorkflowPluginFunctionFactory>> getModuleDescriptors() {
        return pluginAccessor.getModuleDescriptors(
                new ModuleAndDescriptorPredicate<WorkflowPluginFunctionFactory>(
                        new EnabledModulePredicate<WorkflowPluginFunctionFactory>(pluginAccessor),
                        new ModuleDescriptorOfTypePredicate<WorkflowPluginFunctionFactory>(
                                moduleDescriptorFactory,
                                JiraWorkflowPluginConstants.MODULE_NAME_WORKFLOW_FUNCTION
                        )
                )
        );
    }
    
    private Collection<ModuleDescriptor<WorkflowPluginFunctionFactory>> getFunctionalModuleDescriptors() {
    	Collection<ModuleDescriptor<WorkflowPluginFunctionFactory>> moduleDescriptors = getModuleDescriptors();
    	Collection<ModuleDescriptor<WorkflowPluginFunctionFactory>> functionalModuleDescriptors = new ArrayList<ModuleDescriptor<WorkflowPluginFunctionFactory>>();

    	for (ModuleDescriptor<WorkflowPluginFunctionFactory> moduleDescriptor : moduleDescriptors) {
    		String postfunctionClassName = ((WorkflowFunctionModuleDescriptor) moduleDescriptor).getImplementationClass().getName();
    		if (!Arrays.asList(nonfunctionalPostfunctions).contains(postfunctionClassName)) {
    			functionalModuleDescriptors.add(moduleDescriptor);
    		}
        }
    	return functionalModuleDescriptors;
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor){
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);

        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
        String pluginKey = (String) functionDescriptor.getArgs().get(POSTFUNCTION_KEY);
        velocityParams.put(POSTFUNCTION_KEY, pluginKey);
        velocityParams.put("postfunctionHtml", getPostfunctionHtml(pluginKey, functionDescriptor));
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor){
        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
        String postfunctionName = (String) functionDescriptor.getArgs().get(POSTFUNCTION_NAME);
        velocityParams.put(POSTFUNCTION_NAME, postfunctionName);
        super.getVelocityParamsForView(velocityParams, descriptor);
    }

    public Map<String,Object> getDescriptorParams(Map<String, Object> formParams){
        Map<String, Object> params = super.getDescriptorParams(formParams);
        String pluginKey = extractSingleParam(formParams, POSTFUNCTIONS);

        Map<String, ?> descriptorParams = null;
        String postfunctionClassName = null;
        String postfunctionName = null;
        for (ModuleDescriptor<WorkflowPluginFunctionFactory> moduleDescriptor : getModuleDescriptors()) {
            if (moduleDescriptor.getCompleteKey().equals(pluginKey)) {
                WorkflowPluginFunctionFactory workflowPluginFunctionFactory = moduleDescriptor.getModule();
                descriptorParams      = workflowPluginFunctionFactory.getDescriptorParams(formParams);
                postfunctionClassName = ((WorkflowFunctionModuleDescriptor) moduleDescriptor).getImplementationClass().getName();
                postfunctionName      = moduleDescriptor.getName();
            }
        }

        Gson gson = new Gson();
        String descriptorParamsJson = gson.toJson(descriptorParams);

        params.put(POSTFUNCTION_KEY, pluginKey);
        params.put(POSTFUNCTION_CLASS_NAME, postfunctionClassName);
        params.put(POSTFUNCTION_NAME, postfunctionName);
        params.put(PARAMS_FOR_POSTFUNCTION_TO_EXECUTE, descriptorParamsJson);
        params.putAll(descriptorParams);

        return params;
    }
    
    private String getPostfunctionHtml(String postfunctionKey, FunctionDescriptor functionDescriptor) {
        WorkflowFunctionModuleDescriptor moduleDescriptor =
                (WorkflowFunctionModuleDescriptor) pluginAccessor.getEnabledPluginModule(postfunctionKey);
        if(moduleDescriptor == null) {
            return null;
        }
        
        return moduleDescriptor.getHtml(
                JiraWorkflowPluginConstants.RESOURCE_NAME_EDIT_PARAMETERS,
                functionDescriptor
        );
    }
}