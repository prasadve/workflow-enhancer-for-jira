package com.tng.jira.workflowenhancer.servlet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.plugin.workflow.JiraWorkflowPluginConstants;
import com.atlassian.jira.plugin.workflow.WorkflowFunctionModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.base.Strings;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import webwork.action.ActionContext;

public class EditPostFunctionServlet extends HttpServlet {
    
    private PluginAccessor pluginAccessor;
    
    public EditPostFunctionServlet(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String postfunctionKey = request.getParameter("postfunctionKey");
        if(Strings.isNullOrEmpty(postfunctionKey)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing parameter [postfunctionkey]");
            return;
        }
        response.getWriter().append(getDescriptorHtml(postfunctionKey, request.getParameterMap()));
    }
    
    public String getDescriptorHtml(String postfunctionKey, Map parameters) {
        WorkflowFunctionModuleDescriptor moduleDescriptor =
                (WorkflowFunctionModuleDescriptor) pluginAccessor.getEnabledPluginModule(postfunctionKey);
        if(moduleDescriptor == null) {
            return null;
        }
        
        ActionContext.setParameters(parameters);    //we manually have to add the url parameters to the action context

        return moduleDescriptor.getHtml(
                JiraWorkflowPluginConstants.RESOURCE_NAME_INPUT_PARAMETERS,
                (AbstractDescriptor) null
        );
    }
}
