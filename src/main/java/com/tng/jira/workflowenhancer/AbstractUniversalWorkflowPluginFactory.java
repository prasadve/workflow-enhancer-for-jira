package com.tng.jira.workflowenhancer;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import com.tng.jira.workflowenhancer.evaluator.parser.BooleanExpressionParser;
import com.tng.jira.workflowenhancer.util.FieldUtils;
import com.tng.jira.workflowenhancer.util.IdTranslator;
import com.tng.jira.workflowenhancer.util.PreventiveSyntaxCheck;

public abstract class AbstractUniversalWorkflowPluginFactory extends AbstractWorkflowPluginFactory {

    private final I18nHelper i18nHelper;
    private final static Logger log = Logger.getLogger(AbstractUniversalWorkflowPluginFactory.class);

    public AbstractUniversalWorkflowPluginFactory(JiraAuthenticationContext jiraAuthenticationContext) {
        this.i18nHelper = jiraAuthenticationContext.getI18nHelper();
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParameters) {
        velocityParameters.put("fieldList", FieldUtils.getPossibleFieldsForExpression());
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParameters,
            AbstractDescriptor abstractDescriptor) {
        Map<String, Object> args;
        if (abstractDescriptor instanceof ConditionDescriptor) {
            ConditionDescriptor conditionDescriptor = (ConditionDescriptor) abstractDescriptor;
            args = conditionDescriptor.getArgs();
        } else if (abstractDescriptor instanceof ValidatorDescriptor){
            ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) abstractDescriptor;
            args = validatorDescriptor.getArgs();
        } else  if (abstractDescriptor instanceof FunctionDescriptor) {
            FunctionDescriptor functionDescriptor=(FunctionDescriptor) abstractDescriptor;
            args = functionDescriptor.getArgs();
        } else {
            throw new IllegalArgumentException("Descriptor must be a Function-, Condition- or ValidatorDescriptor.");
        }

        String expression = (String) args.get("expression");
        String evaluateTo = (String) args.get("evaluateTo");
        String error = (String) args.get("error");
        String valErrorMessage = StringEscapeUtils.unescapeHtml((String) args.get("val-errorMessage"));

        // translates IDs to Names
        expression = (new IdTranslator(FieldUtils.getPossibleFieldsForExpression(), true))
                .translateExpression(expression);

        velocityParameters.put("expression", StringEscapeUtils.unescapeHtml(expression));
        if (evaluateTo.equals("false") || evaluateTo.equals("true")) {
            velocityParameters.put("evaluateTo", evaluateTo);
        } else {
            error = "Internal error concerning the expression!";
        }
        if (error == null) {
            velocityParameters.put("error", "false");
        } else {
            velocityParameters.put("error", error);
        }
        velocityParameters.put("val-errorMessage", valErrorMessage);
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParameters,
            AbstractDescriptor abstractDescriptor) {
        getVelocityParamsForInput(velocityParameters);
        getVelocityParamsForView(velocityParameters, abstractDescriptor);
    }

    @Override
    public Map<String, Object> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> out = new HashMap<String, Object>();
        String expression = extractSingleParam(conditionParams, "expression");
        String evaluateTo = extractSingleParam(conditionParams, "evaluateTo");

        boolean invalid = isInvalidExpression(expression, evaluateTo, out);
        if (invalid) {
            return out;
        }

        // Translates Names to IDs
        IdTranslator idTranslator = new IdTranslator(FieldUtils.getPossibleFieldsForExpression(), false);
        expression = StringEscapeUtils.escapeHtml(idTranslator.translateExpression(expression));

        out.put("expression", expression);
        out.put("evaluateTo", evaluateTo);

        invalid = isInvalidExpression(expression, evaluateTo, out);
        if (invalid) {
            return out;
        }

        try {
            // expression sucht "{...}" => ersetzt custom field durch Id e.g. customfield_10000
            BooleanExpressionParser bEParser = new BooleanExpressionParser(new ByteArrayInputStream(StringEscapeUtils
                    .unescapeHtml(expression).getBytes("UTF-8")));
            bEParser.Start();
            out.put("error", "false");
        } catch (Throwable e) {
            out.put("error", "true");
            String errormessage = PreventiveSyntaxCheck.check(expression, i18nHelper);
            String unescapedErrorMessage = i18nHelper.getText("workflow.jwfe.errors.parsing.main") + " "
                    + i18nHelper.getText("workflow.jwfe.errors.parsing.youentered") + " " + expression
                    + ". " + i18nHelper.getText("workflow.jwfe.errors.parsing.alwaysfalse") + " "
                    + i18nHelper.getText("workflow.jwfe.errors.parsing.sysmsg") + " "
                    + (errormessage == null ? e.getMessage() : errormessage);
            log.error(e, e);
            out.put("val-errorMessage", StringEscapeUtils.escapeHtml(unescapedErrorMessage));
        }

        return out;
    }

    /**
     * Checks if the expression is not null and not empty, and puts error messages in the map.
     * 
     * @param expression
     * @param evaluateTo must also be put in the map
     * @param out
     * @return
     */
    private boolean isInvalidExpression(String expression, String evaluateTo, Map<String, Object> out) {
        if (expression == null) {
            out.put("expression", expression);
            out.put("evaluateTo", evaluateTo);
            out.put("error", "true");
            out.put("val-errorMessage", "Expression is null, error");
            log.error(new NullPointerException("Expression is null in Abstract UniversalWorkflowPluginFactory"));
            return true;
        }
        if (expression.isEmpty()) {
            out.put("expression", expression);
            out.put("evaluateTo", evaluateTo);
            out.put("error", "true");
            out.put("val-errorMessage", i18nHelper.getText("workflow.jwfe.view.expression.expressionIsEmpty"));
            return true;
        }
        return false;
    }
}
