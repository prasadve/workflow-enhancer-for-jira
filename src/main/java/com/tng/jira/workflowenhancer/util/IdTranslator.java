package com.tng.jira.workflowenhancer.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.bidimap.TreeBidiMap;

import com.atlassian.jira.issue.fields.Field;

/**
 * Takes a complete expression and translates fieldIDs to field names or vice versa if reverse is true.
 */
public class IdTranslator {
	private static final ArrayList<String> fieldOpeningDelimiters = new ArrayList<String>();
	static {
		fieldOpeningDelimiters.add("{");
		fieldOpeningDelimiters.add("{{");
	}

	private final boolean reverse;
	private final TreeBidiMap fieldDictionary;

	public IdTranslator(List<Field> possibleFieldsForExpression, boolean reverse) {
		this.reverse = reverse;

		Map<String, String> mapIdToName = possibleFieldsForExpression.stream()
				.collect(Collectors.toMap(field -> field.getId(), field -> field.getName()));
		fieldDictionary = new TreeBidiMap(mapIdToName);
	}

	public String translateExpression(String expression) {
		return splitAndTranslate(expression).stream().collect(Collectors.joining(""));
	}

	protected List<String> splitAndTranslate(String expression) {
		List<String> sequences = ExpressionSplitter.splitExpression(expression);

		return sequences.stream()
				.map(sequence -> translateSubExpressionIfNecessary(sequence))
				.collect(Collectors.toList());
	}

	protected String translateSubExpressionIfNecessary(String expression) {
		if (isFieldExpression(expression)) {
			return translateFieldExpression(expression);
		}
		if (isSubtasksMacroExpression(expression)) {
			return "[subtasks#" +translateFieldNameOrId(expression.substring(10, expression.length() - 1)) + "]";
		}
		return expression;
	}

	private String translateFieldExpression(String expression) {
		String openingDelimiter = ExpressionSplitter.getFirstOpeningDelimiter(expression);
		String closingDelimiter = ExpressionSplitter.getClosingDelimiter(openingDelimiter);
		String fieldNameOrId = getStrippedExpression(expression);
		return openingDelimiter + translateFieldNameOrId(fieldNameOrId) + closingDelimiter;
	}

	private String translateFieldNameOrId(String fieldNameOrId) {
		if (reverse) {
			return translateIdToName(fieldNameOrId);
		} else {
			return translateNameToId(fieldNameOrId);
		}
	}

	private String translateIdToName(String fieldId) {
		String fieldName = (String) fieldDictionary.get(fieldId);
		if (fieldName == null) {
			return fieldId;
		} else {
			return fieldName;
		}
	}

	private String translateNameToId(String fieldName) {
		String fieldId = (String) fieldDictionary.getKey(fieldName);
		if (fieldId == null) {
			return fieldName;
		} else {
			return fieldId;
		}
	}

	/**
	 * Expects expression that contains just a single block, e.g. RegEx, String, Field, Macro etc.
	 */
	protected static boolean isFieldExpression(String expression) {
		String openingDelimiter = ExpressionSplitter.getFirstOpeningDelimiter(expression);
		String closingDelimiter = ExpressionSplitter.getClosingDelimiter(openingDelimiter);
		if (!fieldOpeningDelimiters.contains(openingDelimiter)) {
			return false;
		}
		return expression.startsWith(openingDelimiter) && expression.endsWith(closingDelimiter);
	}

	private static boolean isSubtasksMacroExpression(String expression) {
		return expression.startsWith("[subtasks#") && expression.endsWith("]");
	}

	/**
	 * Expects expression starting with an opening delimiter and ending with the corresponding closing delimiter.
	 */
	protected static String getStrippedExpression(String expression) {
		String openingDelimiter = ExpressionSplitter.getFirstOpeningDelimiter(expression);
		String closingDelimiter = ExpressionSplitter.getClosingDelimiter(openingDelimiter);
		return expression.substring(openingDelimiter.length(),
				expression.length() - closingDelimiter.length());
	}
}