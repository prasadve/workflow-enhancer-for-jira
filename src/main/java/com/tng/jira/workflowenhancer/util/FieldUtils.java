package com.tng.jira.workflowenhancer.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;

public class FieldUtils {

    private static final String[] notSupported = { "Bug-Import-ID", "Cascading Select", "Hidden Job Switch",
            "Import Id", "Job Checkbox", "Multi Checkboxes", "Radio Buttons", "Read-only Text Field",
            "Multi Select" };
    
    //Auskommentiert für Bug fix  "Select List",
    private static final HashSet<String> notSupportedSet = new HashSet<String>(Arrays.asList(notSupported));

    /**
     * Gets all the fields that are of any use in an expression (i.e. excludes collections).
     * 
     * @return the fields that can be used in an expression.
     */
    public static List<Field> getPossibleFieldsForExpression() {
        FieldManager fieldManager = ComponentAccessor.getComponent(FieldManager.class);
        CustomFieldManager customFieldManager = ComponentAccessor.getComponent(CustomFieldManager.class);

        List<Field> allowedFields = new ArrayList<Field>();

        UsableField[] usableFields = UsableField.values();

        for (UsableField field : usableFields) {
            allowedFields.add(fieldManager.getField(field.getFieldId()));
        }

        for (CustomField customField : customFieldManager.getCustomFieldObjects()) {
            @SuppressWarnings("rawtypes")
            CustomFieldType customFieldType = customField.getCustomFieldType();
            String customFieldName = customFieldType.getName();

            if (isSupported(customFieldName)) {
                allowedFields.add(customField);
            }
        }

        return allowedFields;
    }

    private static boolean isSupported(String name) {

        if (notSupportedSet.contains(name)) {
            return false;
        }
        return true;
    }
}
