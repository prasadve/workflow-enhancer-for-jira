package com.tng.jira.workflowenhancer.util;

import java.util.Iterator;
import java.util.List;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLink;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;


/*
 * List of all available fields for boolean expressions
 * For built in fields see IssueFieldConstants
 * Custom fields can also be supported
 */
enum UsableField {
    AFFECTED_VERSIONS(IssueFieldConstants.AFFECTED_VERSIONS) {
        @Override protected Object getCurrentValue (IssueLinkManager issueLinkManager, Issue issue){
            return issue.getAffectedVersions().toString();
        }

    },

    // Returns value in seconds
    AGGREGATE_TIME_ORIGINAL_ESTIMATE(IssueFieldConstants.AGGREGATE_TIME_ORIGINAL_ESTIMATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Long time = issue.getOriginalEstimate();
            for (Issue subTask : issue.getSubTaskObjects()) {
                time += subTask.getOriginalEstimate();
            }
            if (time == null) {
                return 0L;
            }
            return time;
        }
    },
    // Returns value in seconds
    AGGREGATE_TIME_ESTIMATE(IssueFieldConstants.AGGREGATE_TIME_ESTIMATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Long time = issue.getEstimate();
            for (Issue subTask : issue.getSubTaskObjects()) {
                time += subTask.getEstimate();
            }
            if (time == null) {
                return 0L;
            }
            return time;
        }
    },
    // Returns value in seconds
    AGGREGATE_TIME_SPENT(IssueFieldConstants.AGGREGATE_TIME_SPENT) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Long time = issue.getTimeSpent();
            for (Issue subTask : issue.getSubTaskObjects()) {
                time += subTask.getTimeSpent();
            }
            if (time == null) {
                return 0L;
            }
            return time;
        }
    },

    ASSIGNEE(IssueFieldConstants.ASSIGNEE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getAssigneeUser().getName();
        }
    },

    CREATED(IssueFieldConstants.CREATED) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getCreated();
        }
    },

    DESCRIPTION(IssueFieldConstants.DESCRIPTION) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getDescription();
        }
    },

    DUE_DATE(IssueFieldConstants.DUE_DATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getDueDate();
        }
    },

    ENVIRONMENT(IssueFieldConstants.ENVIRONMENT) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getEnvironment();
        }
    },

    FIX_VERSIONS(IssueFieldConstants.FIX_FOR_VERSIONS) {
        @Override protected Object getCurrentValue (IssueLinkManager issueLinkManager, Issue issue){
            return issue.getFixVersions().toString();
        }

    },

    ISSUE_KEY(IssueFieldConstants.ISSUE_KEY) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getKey();
        }
    },

    ISSUE_TYPE(IssueFieldConstants.ISSUE_TYPE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            IssueType issueTypeObject = issue.getIssueType();
            if (issueTypeObject != null) {
                return issueTypeObject.getName();
            }
            return "\"\"";
        }
    },

    ISSUE_LINKS(IssueFieldConstants.ISSUE_LINKS) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            long issueId = issue.getId();
            List<IssueLink> inwardLinks = issueLinkManager.getInwardLinks(issueId);
            List<IssueLink> outwardLinks = issueLinkManager.getOutwardLinks(issueId);
            return buildLinkString(inwardLinks, outwardLinks);
        }
    },

    PRIORITY(IssueFieldConstants.PRIORITY) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Priority priorityObject = issue.getPriority();
            if (priorityObject != null) {
                return priorityObject.getName();
            }
            return "\"\"";
        }
    },

    PROJECT(IssueFieldConstants.PROJECT) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Project projectObject = issue.getProjectObject();
            if (projectObject != null) {
                return projectObject.getName();
            }
            return "\"\"";
        }
    },

    REPORTER(IssueFieldConstants.REPORTER) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            ApplicationUser reporter = issue.getReporter();
            if (reporter != null) {
                return reporter.getName();
            }
            return "\"\"";
        }
    },

    RESOLUTION(IssueFieldConstants.RESOLUTION) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Resolution resolutionObject = issue.getResolution();
            if (resolutionObject != null) {
                return resolutionObject.getName();
            }
            return "\"\"";
        }
    },

    RESOLUTION_DATE(IssueFieldConstants.RESOLUTION_DATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getResolutionDate();
        }
    },

    STATUS(IssueFieldConstants.STATUS) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            Status statusObject = issue.getStatus();
            if (statusObject != null) {
                return statusObject.getName();
            }
            return "\"\"";
        }
    },

    SUMMARY(IssueFieldConstants.SUMMARY) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getSummary();
        }
    },

    // Returns value in seconds
    TIME_ESTIMATE(IssueFieldConstants.TIME_ESTIMATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            if (issue.getEstimate() == null) {
                return 0L;
            }
            return issue.getEstimate();
        }
    },

    // Returns value in seconds
    TIME_ORIGINAL_ESTIMATE(IssueFieldConstants.TIME_ORIGINAL_ESTIMATE) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            if (issue.getOriginalEstimate() == null) {
                return 0L;
            }
            return issue.getOriginalEstimate();
        }
    },

    // Returns value in seconds
    TIME_SPEND(IssueFieldConstants.TIME_SPENT) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            if (issue.getTimeSpent() == null) {
                return 0L;
            }
            return issue.getTimeSpent();
        }
    },

    UPDATED(IssueFieldConstants.UPDATED) {
        @Override protected Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue) {
            return issue.getUpdated();
        }
    };




    private final String fieldId;

    private UsableField(String fieldId) {
        this.fieldId = fieldId;
    }

    public static Object getAppropriateValue(String fieldId, IssueLinkManager issueLinkManager, Issue issue) {
        for (UsableField field : UsableField.values()) {
            if (field.fieldId.equals(fieldId)) {
                return field.getCurrentValue(issueLinkManager, issue);
            }
        }
        return null;
    }

    protected abstract Object getCurrentValue(IssueLinkManager issueLinkManager, Issue issue);

    public String getFieldId() {
        return fieldId;
    }
    
    private static String buildLinkString(List<IssueLink> inwardLinks, List<IssueLink> outwardLinks){
    	StringBuilder ret = new StringBuilder("\"");
		//Iteriere durch die Listen
		IssueLink it; 
		if(!inwardLinks.isEmpty()) {
            ret.append("Passive: ");

            Iterator<IssueLink> inwardIterator = inwardLinks.iterator();
            while (inwardIterator.hasNext()) {
                it = inwardIterator.next();
                ret.append(it.getIssueLinkType().getName());
                ret.append(" % ");
                ret.append(it.getDestinationObject().getKey());
                if (inwardIterator.hasNext()) {
                    ret.append(" $ ");
                }
            }
        }
		if(!outwardLinks.isEmpty()) {
            ret.append(" Aktive: ");

            Iterator<IssueLink> outwardIterator = outwardLinks.iterator();
            while (outwardIterator.hasNext()) {
                it = outwardIterator.next();
                ret.append(it.getIssueLinkType().getName());
                ret.append(" % ");
                ret.append(it.getDestinationObject().getKey());
                if (outwardIterator.hasNext()) {
                    ret.append(" $ ");
                }
            }
        }
		ret.append("\"");
		return ret.toString() ;
    }
}