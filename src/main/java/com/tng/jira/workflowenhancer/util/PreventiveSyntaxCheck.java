package com.tng.jira.workflowenhancer.util;

import com.atlassian.jira.util.I18nHelper;

public class PreventiveSyntaxCheck {

    public PreventiveSyntaxCheck() {

    }

    public static String check(String expression, I18nHelper i18nHelper) {

        // check for Order/equality operators
        if ((!expression.contains("=")) && (!expression.contains("<")) && (!expression.contains(">"))) {
            return i18nHelper.getText("workflow.jwfe.errors.missingordering");
        }

        // check Brackets
        String errormessage = null;

        char[][] brackets = { { '[', ']', '/', '\"' }, { '{', '}', '/', '\"' }, { '/' }, { '\"' } };

        errormessage = checkForKeywords(expression, i18nHelper);
        if (errormessage == null) {
            errormessage = countBrackets(expression, brackets[1][0], brackets[1][1], brackets[0], i18nHelper);
            if (errormessage == null) {
                errormessage = countBrackets(expression, brackets[0][0], brackets[0][1], brackets[1], i18nHelper);
                if (errormessage == null) {
                    errormessage = simpleCountBrackets(expression, brackets[3][0], i18nHelper);
                    if (errormessage == null) {
                        errormessage = simpleCountBrackets(expression, brackets[2][0], i18nHelper);
                    }
                }
            }
        }

        return errormessage;
    }

    private static String simpleCountBrackets(String expression, char bracket, I18nHelper i18nH1elper) {
        int count = 0;
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == bracket) {
                count++;
            }
        }
        if (count % 2 == 0) {
            return null;
        }
        return i18nH1elper.getText("workflow.jwfe.errors.brackets.missing") + " " + bracket;
    }

    private static String countBrackets(String expression, char opening, char closing, char[] wrongBrackets,
            I18nHelper i18nH1elper) {
        int sumBrackets = 0;
        for (int i = 0; i < expression.length(); i++) {
            for (int j = 0; j < wrongBrackets.length; j++) {
                if (sumBrackets == 1) {
                    if (expression.charAt(i) == wrongBrackets[j]) {
                        return i18nH1elper.getText("workflow.jwfe.errors.brackets.searchfor") + " <b>\'" + closing
                                + "\'</b> " + i18nH1elper.getText("workflow.jwfe.errors.brackets.butfound") + " <b>\'"
                                + wrongBrackets[j] + "\'</b> "
                                + i18nH1elper.getText("workflow.jwfe.errors.brackets.atpos") + " " + (i + 1) + " ( "
                                + expression.substring(0, i) + "<b> " + expression.charAt(i) + " </b>"
                                + expression.substring(i + 1) + " )";
                    }
                }
            }
            if (expression.charAt(i) == opening) {
                if (++sumBrackets != 1) {
                    return i18nH1elper.getText("workflow.jwfe.errors.brackets.searchfor") + " <b>\'" + closing
                            + "\'</b> " + i18nH1elper.getText("workflow.jwfe.errors.brackets.butfound") + " <b>\'"
                            + opening + "\'</b> " + i18nH1elper.getText("workflow.jwfe.errors.brackets.atpos") + " "
                            + (i + 1) + " ( " + expression.substring(0, i) + "<b> " + expression.charAt(i) + " </b>"
                            + expression.substring(i + 1) + " )";

                }
            } else if (expression.charAt(i) == closing) {
                if (--sumBrackets != 0) {
                    return i18nH1elper.getText("workflow.jwfe.errors.brackets.searchfor") + " <b>\'" + opening
                            + "\'</b> " + i18nH1elper.getText("workflow.jwfe.errors.brackets.butfound") + " <b>\'"
                            + closing + "\'</b> " + i18nH1elper.getText("workflow.jwfe.errors.brackets.atpos") + " "
                            + (i + 1) + " ( " + expression.substring(0, i) + "<b> " + expression.charAt(i) + " </b>"
                            + expression.substring(i + 1) + " )";
                }
            }
        }

        if (sumBrackets != 0) {
            return i18nH1elper.getText("workflow.jwfe.errors.brackets.missing") + " " + opening + " "
                    + i18nH1elper.getText("workflow.jwfe.errors.brackets.and") + " " + closing;
        }
        return null;
    }

    private static String checkForKeywords(String expression, I18nHelper i18nH1elper) {
        if (expression.contains("[")) {
            for (int i = 0; i < expression.length(); i++) {
                if (expression.charAt(i) == '[') {
                    if (expression.length() <= i + 4) {
                        return i18nH1elper.getText("workflow.jwfe.errors.wrongsquare");
                    }
                    String partToCheck = expression.substring(i, i + 5);
                    if (!partToCheck.equals("[now]")) {
                        if (expression.length() <= i + 5) {
                            return i18nH1elper.getText("workflow.jwfe.errors.wrongsquare");
                        }
                        partToCheck = expression.substring(i, i + 6);
                        if (!partToCheck.equals("[user]")) {
                            return i18nH1elper.getText("workflow.jwfe.errors.wrongsquare");
                        }
                    }
                }
            }
        }
        return null;
    }

}