package com.tng.jira.workflowenhancer.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class ExpressionSplitter {
	private static final Map<String, String> delimiters = new HashMap<String, String>();
	static {
		delimiters.put("\"", "\"");
		delimiters.put("{{", "}}");
		delimiters.put("{", "}");
		delimiters.put("/", "/");
		delimiters.put("[", "]");
	}

	public static List<String> splitExpression(String expression) {
		List<String> sequences = splitExpressionAtFirstDelimiterPair(expression);
		int numberOfSequences = sequences.size();
		if (numberOfSequences == 1) {
			return sequences;
		}

		String restOfExpression = sequences.get(numberOfSequences - 1);

		sequences.remove(numberOfSequences - 1);
		sequences.addAll(splitExpression(restOfExpression));

		return sequences;
	}

	private static List<String> splitExpressionAtFirstDelimiterPair(String expression) {
		String openingDelimiter = getFirstOpeningDelimiter(expression);
		if (openingDelimiter == null) {
			List<String> sequences = new ArrayList<String>();
			sequences.add(expression);
			return sequences;
		}
		return splitExpressionOnceAtDelimiters(expression, openingDelimiter, getClosingDelimiter(openingDelimiter));
	}

	private static List<String> splitExpressionOnceAtDelimiters(String expression, String openingDelimiter, String closingDelimiter) {
		List<String> sequences = new ArrayList<String>();

		if (!containsDelimitersInCorrectOrder(expression, openingDelimiter, closingDelimiter)) {
			sequences.add(expression);
			return sequences;
		}

		int indexOfOpeningDelimiter = expression.indexOf(openingDelimiter);
		int indexOfClosingDelimiter = expression.indexOf(closingDelimiter, indexOfOpeningDelimiter + 1);

		if (indexOfOpeningDelimiter > 0) {
			sequences.add(expression.substring(0, indexOfOpeningDelimiter));
		}

		sequences.add(expression.substring(indexOfOpeningDelimiter, indexOfClosingDelimiter + closingDelimiter.length()));

		if (indexOfClosingDelimiter + closingDelimiter.length() < expression.length()) {
			sequences.add(expression.substring(indexOfClosingDelimiter + closingDelimiter.length()));
		}

		return sequences;
	}

	private static boolean containsDelimitersInCorrectOrder(String expression, String openingDelimiter, String closingDelimiter) {
		int indexOfOpeningDelimiter = expression.indexOf(openingDelimiter);
		int indexOfClosingDelimiter = expression.indexOf(closingDelimiter);

		// Handle case of closing delimiter being substring or starting with a suffix of opening delimiter
		if (indexOfClosingDelimiter >= indexOfOpeningDelimiter
				&& indexOfClosingDelimiter <= indexOfOpeningDelimiter + openingDelimiter.length() - 1) {
			indexOfClosingDelimiter = expression.indexOf(closingDelimiter, indexOfOpeningDelimiter + openingDelimiter.length());
		}

		return indexOfOpeningDelimiter != -1
				&& indexOfClosingDelimiter != -1
				&& indexOfOpeningDelimiter < indexOfClosingDelimiter;
	}

	public static String getFirstOpeningDelimiter(String expression) {
		int indexOfFirstOpeningDelimiter = expression.length();
		String firstOpeningDelimiter = null;

		for (String potentialOpeningDelimiter : delimiters.keySet()) {
			int index = expression.indexOf(potentialOpeningDelimiter);
			if (index != -1 && index < indexOfFirstOpeningDelimiter) {
				indexOfFirstOpeningDelimiter = index;
				firstOpeningDelimiter = potentialOpeningDelimiter;
			// Handle case of two delimiters starting identically
			} else if (index == indexOfFirstOpeningDelimiter && potentialOpeningDelimiter.length() > firstOpeningDelimiter.length()) {
				firstOpeningDelimiter = potentialOpeningDelimiter;
			}
		}

		return firstOpeningDelimiter;
	}

	public static String getClosingDelimiter(String openingDelimiter) {
		return delimiters.get(openingDelimiter);
	}
}
