package com.tng.jira.workflowenhancer.util;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.label.LabelManager;
import com.atlassian.jira.user.ApplicationUser;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableGroup;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;

public class FieldProvider {

    private final static Logger log = Logger.getLogger(FieldProvider.class);
    private final FieldManager fieldManager;
    private final CustomFieldManager customFieldManager;
    private final IssueLinkManager issueLinkManager;
    private final Issue issue;
    private final Map transientVars;

    public FieldProvider(FieldManager fieldManager, CustomFieldManager customFieldManager, IssueLinkManager issueLinkManager,
    		Issue issue, Map transientVars) {

        this.fieldManager = fieldManager;
        this.customFieldManager = customFieldManager;
        this.issueLinkManager = issueLinkManager;
        this.issue = issue;
        this.transientVars = transientVars;
    }

    /**
     * Given the JIRA field_id, this method will get the value of the field from the FieldManager corresponding to the
     * Issue that was defined in the constructor.
     * 
     * @param fieldIdString
     * 
     * @return the value of the field corresponding to the given String.
     */
    public Object getFieldValueById(String fieldIdString) {
        if (fieldIdString == null || fieldIdString.equals("")) {
            log.warn("empty fieldIdString given to class");
            return null;
        }
        
        if (fieldIdString.equals("transitionComment")) {
        	Object fieldValue = transientVars.get("comment");
        	if (fieldValue == null) {
        		return "";
        	}
        	return fieldValue;
        }

        Field field = fieldManager.getField(fieldIdString);
        if (field == null) {
            field = customFieldManager.getCustomFieldObjectByName(fieldIdString);
            if (field == null) {
                log.warn("Field " + fieldIdString + " could not be found neither as default nor as custom field.");
                return null;
            }
        }
        if (fieldManager.isCustomField(field)) {
            return getCustomFieldValue(issue, field);
        }
        Object fieldValue;
        try { // Empty assignee fields throw a NullPointerException here
            fieldValue = UsableField.getAppropriateValue(field.getId(), issueLinkManager, issue);
        } catch (NullPointerException e) {
            fieldValue = null;
            log.warn("NullPointerException thrown during field value evaluation; this might be caused by an empty assignee field.");
        }
        if (fieldValue == null && fieldIdString.equals("labels")) {
            fieldValue = issue.getLabels().toString();
        }
        if (fieldValue == null && fieldIdString.equals("components")) {
        	Collection<ProjectComponent> components = issue.getComponents();
    		List<String> componentNames = components.stream().map(ProjectComponent::getName).collect(Collectors.toList());
    		fieldValue = componentNames.toString();
        }
        if (fieldValue == null) {
            fieldValue = "";
        }
        return fieldValue;
    }

    /**
     * Takes the value of the issue's custom field and secures that it fits to the values which UsableFields.java
     * returns. There are some cases which are not necessary for the custom fields which JIRA provides by default (e.g.
     * Project, Priority, Status, Resolution). But if there are any other custom fields these types could be useful.
     * 
     * @param issue
     * @param field
     * @return the parameter, JWFE works with e.g. the username
     */
    protected Object getCustomFieldValue(Issue issue, Field field) {
        Object result = issue.getCustomFieldValue((CustomField) field);

        if (result instanceof ApplicationUser) {
            return ((ApplicationUser) result).getName();
        }
        if (result instanceof User) {
            return ((User) result).getName();
        }
        if (result instanceof Timestamp) {
            return result;
        }
        if (result instanceof Date) {
            return new Timestamp(((Date) result).getTime());
        }
        if (result instanceof Long) {
            return result;
        }
        if (result instanceof Status) {
            return ((Status) result).getName();
        }
        if (result instanceof Resolution) {
            return ((Resolution) result).getName();
        }
        if (result instanceof Project) {
            return ((Project) result).getName();
        }
        if (result instanceof Priority) {
            return ((Priority) result).getName();
        }
        if (result instanceof IssueType) {
            return ((IssueType) result).getName();
        }
        if (result instanceof GenericValue) {
            GenericValue res = (GenericValue) result;
            //ProjectPicker
            if (res.getEntityName().equals("Project")) {
                return res.getAllFields().get("name");
            }
        }
        
        if (result instanceof ArrayList) {
            ArrayList<?> res = (ArrayList<?>) result;
            String resul = "";
            
            if(res.size() == 0) {
                return resul;
            }
            
            //MultiGroupPicker
            if (res.get(0) instanceof ImmutableGroup) {
                for (int i = 0; i < res.size(); i++) {
                    resul += ((ImmutableGroup) res.get(i)).getName() + ", ";
                }
                return resul.substring(0, Math.max(resul.length() - 2, 0)); //Eliminate last ", "
            }

            //MultiUserPicker
            if (res.get(0) instanceof User) {
                for (int i = 0; i < res.size(); i++) {
                    resul += ((User) res.get(i)).getName() + ", ";
                }
                return resul.substring(0, Math.max(resul.length() - 2, 0)); //Eliminate last ", " 
            }

            //VersionPicker & every other ArrayList
            resul = res.toString();
            resul = resul.substring(1, resul.length() - 1); //Eliminate first [ and last ]
            return resul;
        }

        //Label picker
        if (result instanceof Set) {
            return result.toString().substring(1, result.toString().length() - 1); //Eliminate first [ and last ]
        }
        
        // Cascading Select, return "null=<parent option value>" or "null=<parent option value>, 1=<child option value>"
        // E.g. validation query to ensure parent and child options are set: {Field}==/null=.*, 1=.*/ 
        if (result instanceof Map) {
        	try {
        		Map<?,?> map = (Map<?,?>) result;
        		if (map.size() == 1 && map.containsKey(null)) {
        			return "null=" + map.get(null);
        		} else if (map.size() == 2 && map.containsKey(null) && map.containsKey("1")) {
        			return "null=" + map.get(null) + ", 1=" + map.get("1");
        		}
        	} catch (NullPointerException | ClassCastException e) { 
        		/* ignore, just in case a map does not support null key or a key of type String.class */ 
    		}
        }

        return result;
    }
    
    public FieldProvider getFieldProviderForOtherIssue(Issue issue) {
    	return new FieldProvider(fieldManager, customFieldManager, issueLinkManager, issue, transientVars);
    }
}
