package com.tng.jira.workflowenhancer.condition;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.tng.jira.workflowenhancer.AbstractUniversalWorkflowPluginFactory;

public class UniversalConditionFactory extends AbstractUniversalWorkflowPluginFactory implements
        WorkflowPluginConditionFactory {

    public UniversalConditionFactory(JiraAuthenticationContext jiraAuthenticationContext,
            TimeTrackingConfiguration timeTrackingConfiguration) {
        super(jiraAuthenticationContext);
    }

}
