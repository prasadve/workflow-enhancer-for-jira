package com.tng.jira.workflowenhancer.condition;

import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;
import com.tng.jira.workflowenhancer.evaluator.parser.TokenMgrError;
import com.tng.jira.workflowenhancer.evaluator.types.AbstractTimespanValue;

public class UniversalCondition extends AbstractJiraCondition {

    private final FieldManager fieldManager;
    private final CustomFieldManager customFieldManager;
    public final IssueLinkManager issueLinkManager;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final ProjectRoleManager projectRoleManager;
    private final static Logger log = Logger.getLogger(UniversalCondition.class);

    public UniversalCondition(FieldManager fieldManager, CustomFieldManager customFieldManager,
    		IssueLinkManager issueLinkManager, TimeTrackingConfiguration timeTrackingConfiguration,
    		GroupManager groupManager, UserManager userUtil, ProjectRoleManager projectRoleManager) {
        this.fieldManager = fieldManager;
        this.customFieldManager = customFieldManager;
        this.issueLinkManager  = issueLinkManager;
        this.groupManager = groupManager;
        this.userManager = userUtil;
        this.projectRoleManager = projectRoleManager;
        AbstractTimespanValue.numberOfDaysPerWorkingWeek = timeTrackingConfiguration.getDaysPerWeek().floatValue();
        AbstractTimespanValue.numberOfHoursPerWorkingDay = timeTrackingConfiguration.getHoursPerDay().floatValue();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps) throws WorkflowException {

        if (args.get("error") != null && ((String) args.get("error")).equals("true")) {
            return false;
        }

        Issue issue = getIssue(transientVars);
        String callerName  = getCallerUser(transientVars, args).getName();
        String expression = (String) args.get("expression");
        boolean evaluateTo = ((String) args.get("evaluateTo")).equals("true") ? true : false;

        try {
            BooleanEvaluator booleanEvaluator = new BooleanEvaluator(fieldManager, customFieldManager,
            		issueLinkManager, groupManager, userManager, projectRoleManager, callerName, issue, transientVars);

            boolean evaluationResult = booleanEvaluator.evaluate(expression);
            return evaluationResult == evaluateTo;
        } catch (ParseException e) { // This method shouldn't fail even if we cannot parse the expression
            log.error(e, e);
            return false;
        } catch (TokenMgrError e) {
            log.error(e, e);
            return false;
        }
    }
}
