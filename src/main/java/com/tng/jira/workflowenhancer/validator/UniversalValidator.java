package com.tng.jira.workflowenhancer.validator;

import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import com.tng.jira.workflowenhancer.condition.UniversalCondition;
import com.tng.jira.workflowenhancer.evaluator.BooleanEvaluator;
import com.tng.jira.workflowenhancer.evaluator.parser.ParseException;
import com.tng.jira.workflowenhancer.evaluator.parser.TokenMgrError;
import com.tng.jira.workflowenhancer.evaluator.types.AbstractTimespanValue;

public class UniversalValidator extends AbstractJiraFunctionProvider implements Validator {

    private final FieldManager fieldManager;
    public final IssueLinkManager issueLinkManager;
    private final CustomFieldManager customFieldManager;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final ProjectRoleManager projectRoleManager;
    private final static Logger log = Logger.getLogger(UniversalCondition.class);

    public UniversalValidator(FieldManager fieldManager, CustomFieldManager customFieldManager,
    		IssueLinkManager issueLinkManager, TimeTrackingConfiguration timeTrackingConfiguration,
    		GroupManager groupManager, UserManager userManager, ProjectRoleManager projectRoleManager) {
        this.fieldManager = fieldManager;
        this.customFieldManager = customFieldManager;
        this.issueLinkManager = issueLinkManager;
        this.groupManager = groupManager;
        this.userManager = userManager;
        this.projectRoleManager = projectRoleManager;
        AbstractTimespanValue.numberOfDaysPerWorkingWeek = timeTrackingConfiguration.getDaysPerWeek().floatValue();
        AbstractTimespanValue.numberOfHoursPerWorkingDay = timeTrackingConfiguration.getHoursPerDay().floatValue();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {

        Issue issue = getIssue(transientVars);
        String callerName  = getCallerUser(transientVars, args).getName();
        String expression = (String) args.get("expression");
        String message = (String) args.get("message");

        if (args.get("error") != null && ((String) args.get("error")).equals("true")) {
            throw new InvalidInputException(message);
        }

        boolean evaluateTo = ((String) args.get("evaluateTo")).equals("true") ? true : false;

        try {
            BooleanEvaluator booleanEvaluator = new BooleanEvaluator(fieldManager, customFieldManager,
            		issueLinkManager, groupManager, userManager, projectRoleManager, callerName, issue, transientVars);

            boolean evaluationResult = booleanEvaluator.evaluate(expression);
            if (evaluationResult != evaluateTo) {
                throw new InvalidInputException(message);
            }
        } catch (ParseException e) { // This method shouldn't fail even if we
            // cannot parse the Exception
            log.error(e, e);
        } catch (TokenMgrError e) {
            log.error(e, e);
        }

    }

    @Override
    @SuppressWarnings("rawtypes")
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        // not used
    }

}
