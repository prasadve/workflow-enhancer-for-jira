package com.tng.jira.workflowenhancer.validator;

import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;

import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import com.tng.jira.workflowenhancer.AbstractUniversalWorkflowPluginFactory;

public class UniversalValidatorFactory extends AbstractUniversalWorkflowPluginFactory implements
        WorkflowPluginValidatorFactory {

    public UniversalValidatorFactory(JiraAuthenticationContext jiraAuthenticationContext,
            TimeTrackingConfiguration timeTrackingConfiguration) {
        super(jiraAuthenticationContext);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor abstractDescriptor) {
        super.getVelocityParamsForEdit(velocityParams, abstractDescriptor);

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) abstractDescriptor;
        Map<String, Object> args = validatorDescriptor.getArgs();

        String message = (String) args.get("message");

        if (message != null) {
            velocityParams.put("message", StringEscapeUtils.escapeHtml(message));
        }
    }

    @Override
    public Map<String, Object> getDescriptorParams(Map<String, Object> params) {
        Map<String, Object> out = super.getDescriptorParams(params);

        String message = extractSingleParam(params, "message");
        out.put("message", message);

        return out;
    }

}
