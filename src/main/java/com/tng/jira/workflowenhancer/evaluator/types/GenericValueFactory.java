package com.tng.jira.workflowenhancer.evaluator.types;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.tng.jira.workflowenhancer.util.FieldProvider;

public class GenericValueFactory {
	public static final String REALTIMESPAN_FORMAT = "((([0-9])+W))?(( )*(([0-9])+D))?(( )*(([0-9])+H))?(( )*(([0-9])+MIN))?";
	public static final String WORKTIMESPAN_FORMAT = "((([0-9])+w))?(( )*(([0-9])+d))?(( )*(([0-9])+h))?(( )*(([0-9])+min))?";
	private static final int MAX_RECURSION_DEPTH = 50;

	private FieldProvider fieldProvider;
	private GroupManager groupManager;
	private UserManager userManager;
	private ProjectRoleManager projectRoleManager;
	private String callerName;

	// Gives consistency for [now]==[now] and doesn't become obsolete because GenericValueFactory exists only for a single query
	private Timestamp currentTime = new Timestamp(System.currentTimeMillis());

	public GenericValueFactory(FieldProvider fieldProvider, GroupManager groupManager, UserManager userManager,
			ProjectRoleManager projectRoleManager, String callerName) {
		this.fieldProvider = fieldProvider;
		this.groupManager = groupManager;
		this.userManager = userManager;
		this.projectRoleManager = projectRoleManager;
		this.callerName = callerName;
	}

	/**
	 * Gets a GenericValue given a String and an Issue.
	 * The string may represent a value, a field or a macro.
	 * For all fields and some macros the Issue repsents the issue they are evaluated on.
	 * 
	 * @return the GenericValue that corresponds to the String
	 */
	public GenericValue getGenericValueFromString(String value, Issue issue) {
		return getGenericValueFromString(value, fieldProvider, issue, 0);
	}

	private GenericValue getGenericValueFromString(String value, FieldProvider fieldProvider, Issue issue, int recursionCount) {
		recursionCount++;

		if (value.isEmpty()) {
			return new StringValue(value);
		} else if (value.startsWith("\"")) {
			return new StringValue(value, true);
		} else if (value.startsWith("/")) {
			return new RegExValue(value, true);
		}
		// Simple data types are DateValue, FloatValue, RealTimespanValue, WorkTimespanValue
		else if (isSimpleDataType(value)) {
			return getGenericValueForSimpleDataType(value);
		} else if (isFieldIdentifier(value)) {
			return getGenericValueForField(value, fieldProvider, issue, recursionCount);
		} else if (isMacro(value)) {
			return getGenericValueForMacro(value, fieldProvider, issue);
		} else {
			return new StringValue(value);
		}
	}

	private static GenericValue getGenericValueForSimpleDataType(String value) {
		if (isDateValue(value)) {
			return new DateValue(value);
		} else if (isFloatValue(value)) {
			return new FloatValue(value);
		} else if (isRealTimespanValue(value)) {
			return new RealTimespanValue(value);
		} else if (isWorktimespanValue(value)) {
			return new WorkTimespanValue(value);
		} else {
			return new StringValue(value);
		}
	}

	private GenericValue getGenericValueForField(String fieldId, FieldProvider fieldProvider, Issue issue, int recursionCount) {
		boolean shouldBeEvaluated;
		if (fieldId.startsWith("{{")) {
			fieldId = fieldId.substring(2, fieldId.length() - 2); // Strip {{}}
			shouldBeEvaluated = false;
		} else {
			fieldId = fieldId.substring(1, fieldId.length() - 1); // Strip {}
			shouldBeEvaluated = true;
		}

		if (recursionCount > MAX_RECURSION_DEPTH) {
			shouldBeEvaluated = false;
		}

		Object fieldValue = fieldProvider.getFieldValueById(fieldId);

		// JIRA will return null if the Field has not been set
		if (fieldValue == null) {
			return new StringValue("");
		} else if (fieldValue instanceof Timestamp) {
            //JIRA returns a Timestamp value for Due Date although it should be a simple date
            if (fieldId.equals("duedate")) {//"duedate" has been translated previously, thus is independent of JIRA language
            	return new DateValue((Timestamp) fieldValue, false);
            }
            return new DateValue((Timestamp) fieldValue);
		} else if (fieldValue instanceof Long) {
			return new WorkTimespanValue(safeLongSecondsToIntegerMinutes((Long) fieldValue));
		}

		String value = fieldValue.toString();
		if (value.isEmpty()) {
			return new StringValue(value);
		} else if (isSimpleDataType(value)) {
			return getGenericValueForSimpleDataType(value);
		}
		else if (shouldBeEvaluated) {
			// Recursively evaluate field
			return getGenericValueFromString(value, fieldProvider, issue, recursionCount);
		} else {
			return new StringValue(value);
		}
	}

    private GenericValue getGenericValueForMacro(String value, FieldProvider fieldProvider, Issue issue) {
    	if (value.equals("[now]")) {
            return new DateValue(currentTime);
        } else if (value.equals("[user]")) {
            return new StringValue(callerName);
        } else if (value.equals("[groups]")) {
        	Object[] groupNames = groupManager.getGroupNamesForUser(callerName).toArray();
        	return new StringValue(Arrays.toString(groupNames)); // dont strip [] to be in line with e.g. {labels}
        } else if (value.equals("[roles]")) {
        	Collection<ProjectRole> projectRoles = projectRoleManager.getProjectRoles(
        			userManager.getUserByName(callerName), issue.getProjectObject());
        	List<String> projectRoleNames = projectRoles.stream()
        			.map(ProjectRole::getName)
        			.collect(Collectors.toList());
        	return new StringValue(projectRoleNames.toString()); // dont strip [] to be in line with e.g. {labels}
        } else if (value.equals("[numberSubtasks]")) {
        	return new FloatValue((double)issue.getSubTaskObjects().size());
        } else if (value.startsWith("[subtasks#")) {
        	String fieldId = "{{" + value.substring(10, value.length() - 1) + "}}";
        	Collection<Issue> subtasks = issue.getSubTaskObjects();
        	List<GenericValue> values = subtasks.stream()
        			.map(
        				subtask -> getGenericValueForField(fieldId,
        						fieldProvider.getFieldProviderForOtherIssue(subtask),
        						subtask,
        						0))
        			.collect(Collectors.toList());
        	return new StringValue(values.toString());
        } else {
            return new StringValue(value);
        }
    }

	private static boolean isSimpleDataType(String value) {
		return isDateValue(value) || isFloatValue(value) || isRealTimespanValue(value) || isWorktimespanValue(value);
	}

	private static boolean isDateValue(String value) {
		try {
			DateValue.dateFormatter.parseDateTime(value);
			return true;
		} catch (IllegalArgumentException e) {
			try {
				DateValue.dateTimeFormatter.parseDateTime(value);
				return true;
			} catch (IllegalArgumentException iae) {}
		}
		return false;
	}

	private static boolean isFloatValue(String value) {
		// Copied from https://docs.oracle.com/javase/8/docs/api/java/lang/Double.html#valueOf(java.lang.String)
		// Modified in order not to accept hexdigits and trailing dDfF

		final String Digits     = "(\\p{Digit}+)";
		// an exponent is 'e' or 'E' followed by an optionally signed decimal integer.
		final String Exp        = "[eE][+-]?"+Digits;
		final String fpRegex    =
				("[\\x00-\\x20]*"+  // Optional leading "whitespace"
						"[+-]?(" + // Optional sign character
						"NaN|" +           // "NaN" string
						"Infinity|" +      // "Infinity" string

    	       // Digits ._opt Digits_opt ExponentPart_opt
    	       "(("+Digits+"(\\.)?("+Digits+"?)("+Exp+")?)|"+

    	       // . Digits ExponentPart_opt
    	       "(\\.("+Digits+")("+Exp+")?)"+
    	       "))" +
						"[\\x00-\\x20]*");// Optional trailing "whitespace"

		return Pattern.matches(fpRegex, value);
	}

	private static boolean isRealTimespanValue(String value) {
		return value.matches(REALTIMESPAN_FORMAT);
	}

	private static boolean isWorktimespanValue(String value) {
		return value.matches(WORKTIMESPAN_FORMAT);
	}

	private static boolean isFieldIdentifier(String value) {
		if (value.startsWith("{")) { // Includes starting with {{
			return true;
		}
		return false;
	}

	private static boolean isMacro(String value) {
		return value.startsWith("[");
	}

	private static int safeLongSecondsToIntegerMinutes(long seconds) {
		long minutes = seconds / 60;
		if (minutes < Integer.MIN_VALUE || minutes > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(minutes + " cannot be cast to int without changing its value.");
		}
		return (int) minutes;
	}
}
