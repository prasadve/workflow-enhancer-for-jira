package com.tng.jira.workflowenhancer.evaluator.types;

public class OrderOperations {

    /**
     * Compares two GenericValues. Throws an IncompatibleTypesException if the order operations (< <= = >= >) are not
     * defined for the two GenericValues. The order operations are definded for the following pairs: (RegExValue, anyValue),
     * (anyValue, RegExValue), (StringValue, anyValue), (anyValue, StringValue), (FloatValue, FloatValue),
     * (DateValue, DateValue), (TimespanValue, TimespanValue) where anyValue is any instance of GenericValue.
     * 
     * @param leftValue
     * @param rightValue
     * @return -1 if leftValue is smaller than rightValue, 0 if they are equal, else -1.
     * @throws IncompatibleTypesException
     */
    public static int compareTo(GenericValue leftValue, GenericValue rightValue) throws IncompatibleTypesException {

    	// (RegExValue, GenericValue)
        if (leftValue instanceof RegExValue) {
        	return ((RegExValue) leftValue).compareTo(new StringValue(rightValue.toString()));
        }

    	// (GenericValue, RegExValue)
        else if (rightValue instanceof RegExValue) {
        	return - compareTo(rightValue, leftValue);
        }

        // (StringValue, GenericValue)
        else if (leftValue instanceof StringValue) {
            return ((StringValue) leftValue).compareTo(new StringValue(rightValue.toString()));
        }

        // (GenericValue, StringValue)
        else if (rightValue instanceof StringValue) {
            return - compareTo(rightValue, leftValue);
        }

        // (FloatValue, FloatValue)
        else if ((leftValue instanceof FloatValue) && (rightValue instanceof FloatValue)) {
            return ((FloatValue) leftValue).compareTo((FloatValue) rightValue);
        }

        // (DateValue, DateValue)
        else if ((leftValue instanceof DateValue) && (rightValue instanceof DateValue)) {
            return ((DateValue) leftValue).compareTo((DateValue) rightValue);
        }

        // (TimespanValue, TimespanValue)
        else if ((leftValue instanceof AbstractTimespanValue) && (rightValue instanceof AbstractTimespanValue)) {
            return ((AbstractTimespanValue) leftValue).compareTo((AbstractTimespanValue) rightValue);
        }

        // Exception is caught in BooleanEvaluator.evaluate(Node node), this method returns false.
        throw new IncompatibleTypesException("CompareTo is not defined for this arguments: " + leftValue.toString()
                + ", " + rightValue.toString());
    }

    /**
     * Checks if two GenericValues are equal. Throws an IncompatibleTypesException if equality is not defined for the
     * two GenericValues. Equality is defined for the following pairs: (RegExValue, anyValue),
     * (anyValue, RegExValue), (StringValue, anyValue), (anyValue, StringValue), (FloatValue, FloatValue),
     * (DateValue, DateValue), (TimespanValue, TimespanValue) where anyValue is any instance of GenericValue.
     * 
     * @param leftValue
     * @param rightValue
     * @return true if and only if ( compareTo(leftValue, rightValue) == 0 )
     * @throws IncompatibleTypesException
     */
    public static boolean equals(GenericValue leftValue, GenericValue rightValue) throws IncompatibleTypesException {

        return compareTo(leftValue, rightValue) == 0;

    }
}
