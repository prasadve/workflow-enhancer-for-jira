package com.tng.jira.workflowenhancer.evaluator.types;

import java.util.List;

public class RealTimespanValue extends AbstractTimespanValue {

    /**
     * Constructs a RealTimespanValue from a string.
     * 
     * @param value String in real timespan format _W _D _H _MIN
     */
    public RealTimespanValue(String value) {
        this.DaysPerWeek = 7f;
        this.HoursPerDay = 24f;

        float minutes = 0;
        List<String> valueComponents = getValueComponents(value);

        for (int i = 0; i < valueComponents.size(); i++) {
            if ((valueComponents.get(i)).endsWith("W")) {
                minutes = minutes + DaysPerWeek * HoursPerDay * 60 * getIntFromStr(valueComponents.get(i));
            } else if ((valueComponents.get(i)).endsWith("D")) {
                minutes = minutes + HoursPerDay * 60 * getIntFromStr(valueComponents.get(i));
            } else if ((valueComponents.get(i)).endsWith("H")) {
                minutes = minutes + 60 * getIntFromStr(valueComponents.get(i));
            } else {
                minutes = minutes + getIntFromStr(valueComponents.get(i));
            }
        }

        this.addTime(Math.abs(minutes));
    }

    public RealTimespanValue(int weeks, int days, int hours, int minutes) {
        this.DaysPerWeek = 7;
        this.HoursPerDay = 24;
        float min = Math.abs(minutes + 60 * hours + HoursPerDay * 60 * days + DaysPerWeek * HoursPerDay * 60 * weeks);
        this.addTime(Math.abs(min));
    }

    public RealTimespanValue(int minutes) {
        this.DaysPerWeek = 7;
        this.HoursPerDay = 24;
        this.addTime(Math.abs(minutes));
    }

    @Override
    public String toString() {
        return weeks + "W " + days + "D " + hours + "H " + minutes + "MIN";
    }

    @Override
    public int getIntFromStr(String stringNumber) {
        if (!stringNumber.endsWith("MIN")) {
            return Integer.parseInt(stringNumber.substring(0, stringNumber.length() - 1));
        }
        return Integer.parseInt(stringNumber.substring(0, stringNumber.length() - 3));
    }
}
