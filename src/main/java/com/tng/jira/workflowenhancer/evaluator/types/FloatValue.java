package com.tng.jira.workflowenhancer.evaluator.types;

public class FloatValue extends AbstractGenericValue implements GenericValue {

    private final double value;

    public FloatValue(String value) {
        this.value = Double.parseDouble(value);
    }

    public FloatValue(double value) {
        this.value = value;
    }

    public int compareTo(FloatValue f) {
        return Double.compare(value, f.value);
    }

    public GenericValue add(FloatValue f) {
        return new FloatValue(value + f.value);
    }

    public GenericValue subtract(FloatValue f) {
        return new FloatValue(value - f.value);
    }

    @Override
    public Object getValue() {
        return new Double(value);
    }

    @Override
    public int hashCode() {
        assert false : "hashCode not designed";
        return 1337; // arbitrary constant
    }
}