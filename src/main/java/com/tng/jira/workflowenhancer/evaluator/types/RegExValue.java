package com.tng.jira.workflowenhancer.evaluator.types;

public class RegExValue extends AbstractGenericValue implements GenericValue {

    private String value;

    public RegExValue(String value) {
        this(value, false);
    }

    public RegExValue(String value, boolean strip) {
        if (strip) {
            this.value = value.substring(1, value.length() - 1); // Strip '/ /'
        } else {
            this.value = value;
        }
    }

    @Override
    public Object getValue() {
        return value;
    }

    /**
     * Compares this RegExValue to a given StringValue.
     * 
     * @param stringValue
     * @return 0 if the given StringValue matches this RegExValue, -1 if this.toString() is lexicographically smaller
     *         than stringValue.toString() and 1 if this.toString() is lexicographically greater than
     *         stringValue.toString().
     */
    public int compareTo(StringValue stringValue) {
        if (stringValue.toString().matches(toString())) {
            return 0;
        }
        return toString().compareTo(stringValue.toString());
    }

    public GenericValue add(RegExValue argument) {
        return new RegExValue(value + argument.value);
    }
}
