package com.tng.jira.workflowenhancer.evaluator.types;

import org.apache.log4j.Logger;

import com.tng.jira.workflowenhancer.evaluator.TermEvaluator;

public abstract class AbstractGenericValue {

	private final static Logger log = Logger.getLogger(AbstractGenericValue.class);
    public abstract Object getValue();

    @Override
    public String toString() {
        return getValue().toString();
    }

    /**
     * Checks if the two GenericValues are equal. Throws an IncompatibleTypesException if this order operation is not
     * defined for the two GenericValues (see OrderOperations).
     * 
     * @param rightValue
     * @return true if and only if the GenericValues are equal.
     * @throws IncompatibleTypesException
     */
    public boolean equals(GenericValue rightValue) throws IncompatibleTypesException {
    	boolean result = OrderOperations.equals((GenericValue) this, rightValue);
       	log.debug(this.toString() + " = "+  rightValue.toString() + " was evaluated to: " + result);
        return result;
    }

    /**
     * Compares the two GenericValues. Throws an IncompatibleTypesException if the order operations are not defined for
     * the two GenericValues (see OrderOperations).
     * 
     * @param rightValue
     * @return -1 if this GenericValue is smaller than rightValue, 0 if they are equal, else -1.
     * @throws IncompatibleTypesException
     */
    public int compareTo(GenericValue rightValue) throws IncompatibleTypesException {
    	int result = OrderOperations.compareTo((GenericValue) this, rightValue);
    	String res = " = ";
    	if ( result != 0  ){
    		res = (result < 0 ) ? "<"  : ">";
    	}
    	log.debug(this.toString() + " compared to "+  rightValue.toString() + " was evaluated to: " + res);
    	return result;
    }

    /**
     * Adds the two GenericValues if possible.
     * 
     * @param rightValue
     * @return the result of the addition
     * @throws IncompatibleTypesException
     */
    public GenericValue add(GenericValue rightValue) throws IncompatibleTypesException {
        return ArithmeticOperations.add((GenericValue) this, rightValue);
    }

    /**
     * Subtracts the two GenericValues if possible.
     * 
     * @param rightValue
     * @return the result of the subtraction
     * @throws IncompatibleTypesException
     */
    public GenericValue subtract(GenericValue rightValue) throws IncompatibleTypesException {
        return ArithmeticOperations.subtract((GenericValue) this, rightValue);
    }

}