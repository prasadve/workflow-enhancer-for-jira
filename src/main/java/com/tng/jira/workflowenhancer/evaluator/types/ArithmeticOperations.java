package com.tng.jira.workflowenhancer.evaluator.types;

public abstract class ArithmeticOperations {

    /**
     * Adds two GenericValue if possible. Otherwise it throws an IncompatibleTypesException. Possible additions:
     * StringValue + StringValue, FloatValue + FloatValue, DateValue + RealTimespanValue, RealTimespanValue + DateValue,
     * TimespanValue + TimespanValue.
     * 
     * @param leftValue
     * @param rightValue
     * @return the result of the addition
     * @throws IncompatibleTypesException
     */
    public static GenericValue add(GenericValue leftValue, GenericValue rightValue) throws IncompatibleTypesException {

        // StringValue + StringValue
        if ((leftValue instanceof StringValue) && (rightValue instanceof StringValue)) {
            return ((StringValue) leftValue).add((StringValue) rightValue);
        }

        // FloatValue + FloatValue
        if ((leftValue instanceof FloatValue) && (rightValue instanceof FloatValue)) {
            return ((FloatValue) leftValue).add((FloatValue) rightValue);
        }

        // DateValue + RealTimespanValue
        if ((leftValue instanceof DateValue) && (rightValue instanceof RealTimespanValue)) {
            return ((DateValue) leftValue).add((RealTimespanValue) rightValue);
        }

        if (leftValue instanceof AbstractTimespanValue) {
            // TimespanValue + TimespanValue
            if (rightValue instanceof AbstractTimespanValue) {
                return ((AbstractTimespanValue) leftValue).add((AbstractTimespanValue) rightValue);
            }
            // RealTimespanValue + DateValue
            if ((leftValue instanceof RealTimespanValue) && (rightValue instanceof DateValue)) {
                return ((DateValue) rightValue).add((RealTimespanValue) leftValue);
            }
        }

        // Exception is caught in ArithmeticEvaluator.evaluate, then the leftValue is given back
        throw new IncompatibleTypesException("Addition is not defined for this arguments: " + leftValue.toString()
                + ", " + rightValue.toString());
    }

    /**
     * Subtracts two GenericValue if possible. Otherwise it throws an IncompatibleTypesException. Possible subtractions:
     * FloatValue - FloatValue, DateValue - DateValue, DateValue - RealTimespanValue, RealTimespanValue - DateValue,
     * TimespanValue - TimespanValue.
     * 
     * @param leftValue
     * @param rightValue
     * @return the result of the subtraction
     * @throws IncompatibleTypesException
     */
    public static GenericValue subtract(GenericValue leftValue, GenericValue rightValue)
            throws IncompatibleTypesException {

        // FloatValue - FloatValue
        if ((leftValue instanceof FloatValue) && (rightValue instanceof FloatValue)) {
            return ((FloatValue) leftValue).subtract((FloatValue) rightValue);
        }

        if (leftValue instanceof DateValue) {
            // DateValue - DateValue
            if (rightValue instanceof DateValue) {
                return ((DateValue) leftValue).subtract((DateValue) rightValue);
            }
            // DateValue - RealTimespanValue
            if (rightValue instanceof RealTimespanValue) {
                return ((DateValue) leftValue).subtract((RealTimespanValue) rightValue);
            }
        }

        if (leftValue instanceof AbstractTimespanValue) {
            // TimespanValue - TimespanValue
            if (rightValue instanceof AbstractTimespanValue) {
                return ((AbstractTimespanValue) leftValue).subtract((AbstractTimespanValue) rightValue);
            }
            // TimespanValue - DateValue
            if (rightValue instanceof DateValue && leftValue instanceof RealTimespanValue) {
                return ((DateValue) rightValue).subtract((RealTimespanValue) leftValue);
            }
        }

        // Exception is caught in ArithmeticEvaluator.evaluate, then the leftValue is given back.
        throw new IncompatibleTypesException("Subtraction is not defined for this arguments: " + leftValue.toString()
                + ", " + rightValue.toString());
    }
}
