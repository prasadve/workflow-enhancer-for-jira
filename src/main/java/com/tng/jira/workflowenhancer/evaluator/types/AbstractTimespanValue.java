package com.tng.jira.workflowenhancer.evaluator.types;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public abstract class AbstractTimespanValue extends AbstractGenericValue implements GenericValue {

    public static float numberOfHoursPerWorkingDay = 8, numberOfDaysPerWorkingWeek = 5;

    protected float HoursPerDay;
    protected float DaysPerWeek;
    protected int weeks = 0, days = 0, hours = 0, minutes = 0;

    protected final static Logger log = Logger.getLogger(AbstractTimespanValue.class);

    /**
     * Compares this TimespanValue to a given TimespanValue. Returns 0 if the TimeSpanValues (converted into minutes)
     * are equal, -1 if this is smaller than the given TimespanValue, 1 if this is greater than the given TimespanValue.
     * 
     * @param rightTimespan
     * @return one of {-1, 0, 1}
     */
    public int compareTo(AbstractTimespanValue rightTimespan) {
        return ((Integer) this.getInMinutes()).compareTo(rightTimespan.getInMinutes());
    }

    /**
     * Adds a given AbstractTimespanValue to this AbstractTimespanValue.
     * 
     * @param rightTimespan
     * @return the result is an instance of the class of this AbstractTimespanValue.
     */
    public AbstractTimespanValue add(AbstractTimespanValue rightTimespan) {
        int leftMinutes = this.getInMinutes();
        int rightMinutes = rightTimespan.getInMinutes();
        this.setZero();
        this.addTime(Math.abs(leftMinutes + rightMinutes));
        return this;
    }

    /**
     * Subtracts a given AbstractTimespanValue from this AbstractTimespanValue.
     * 
     * @param rightTimespan
     * @return the result is an instance of the class of this AbstractTimespanValue.
     */
    public GenericValue subtract(AbstractTimespanValue rightTimespan) {
        int leftMinutes = this.getInMinutes();
        int rightMinutes = rightTimespan.getInMinutes();
        this.setZero();
        this.addTime(Math.abs(leftMinutes - rightMinutes));
        return this;
    }

    @Override
    public abstract String toString();

    @Override
    public Object getValue() {
        return toString();
    }

    @Override
    public int hashCode() {
        assert false : "hashCode not designed";
        return 1337; // arbitrary constant
    }

    /**
     * Returns the TimeSpan converted into its smallest unit (minutes)
     * 
     * @return TimeSpan converted into minutes
     */
    public int getInMinutes() {
        float result = minutes + 60 * hours + HoursPerDay * 60 * days + DaysPerWeek * HoursPerDay * 60 * weeks;

        return Math.round(result);
    }

    /**
     * Returns the TimeSpan converted into seconds
     * 
     * @return TimeSpan converted into seconds
     */
    public long getInSeconds() {
        long result = 60 * ((long) getInMinutes());
        return result;
    }

    /**
     * Adds the given amount of minutes to the TimeSpan object and formats every entry of the object s.t. minutes < 60,
     * hours < numberOfHoursPerDay and days < numberOfDaysPerWeek.
     * 
     * @param totalMinutes Should be a positive integer. If totalMinutes <= 0 the object isn't changed.
     */
    protected void addTime(float totalMinutes) {
        if (totalMinutes > 0) {
            totalMinutes += this.getInMinutes();
            this.setZero();

            while (DaysPerWeek * HoursPerDay * 60 <= totalMinutes) {
                this.weeks++;
                totalMinutes -= DaysPerWeek * HoursPerDay * 60;
            }
            while (HoursPerDay * 60 <= totalMinutes) {
                this.days++;
                totalMinutes -= HoursPerDay * 60;
            }
            while (59.5 < totalMinutes) {
                this.hours++;
                totalMinutes -= 60;
            }

            this.minutes = Math.round(totalMinutes);
        }
    }

    protected static List<String> getValueComponents(String value) {
        List<String> valueComponents = new ArrayList<String>();
        try {
            InputStream in = new ByteArrayInputStream(value.getBytes("UTF-8"));

            int inputChar = in.read();
            String valueComponent = "";

            while (inputChar != -1) {
                if (Character.isDigit(inputChar)) {
                    valueComponent += (char) inputChar;
                    inputChar = in.read();
                } else if (Character.isLetter(inputChar)) {
                    while (Character.isLetter(inputChar)) {
                        valueComponent += (char) inputChar;
                        inputChar = in.read();
                    }
                    valueComponents.add(valueComponent);
                    valueComponent = "";
                } else {
                    inputChar = in.read();
                }
            }

        } catch (UnsupportedEncodingException e) {
            log.error(e, e);
        } catch (IOException e) {
            log.error(e, e);
        }
        return valueComponents;
    }

    protected abstract int getIntFromStr(String stringNumber);

    /**
     * Sets the weeks, days, hours and minutes values of this timespan object to zero.
     */
    protected void setZero() {
        this.weeks = 0;
        this.days = 0;
        this.hours = 0;
        this.minutes = 0;
    }

    public int getWeeks() {
        return weeks;
    }

    public int getDays() {
        return days;
    }

    public int getHours() {
        return hours;
    }

    public int getMinutes() {
        return minutes;
    }

}
