package com.tng.jira.workflowenhancer.evaluator;

import java.util.Arrays;
import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.link.IssueLinkManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.tng.jira.workflowenhancer.evaluator.parser.ASTArithmeticOperator;
import com.tng.jira.workflowenhancer.evaluator.parser.ASTLiteral;
import com.tng.jira.workflowenhancer.evaluator.parser.Node;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValue;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValueFactory;
import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;
import com.tng.jira.workflowenhancer.util.FieldProvider;

public class ArithmeticEvaluator {

    private final FieldProvider fieldProvider;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final ProjectRoleManager projectRoleManager;
    private final Issue issue;
    private final String callerName;
    private final static Logger log = Logger.getLogger(ArithmeticEvaluator.class);

    public ArithmeticEvaluator(FieldManager fieldManager, CustomFieldManager customFieldManager,
    		IssueLinkManager issueLinkManager, GroupManager groupManager, UserManager userManager,
    		ProjectRoleManager projectRoleManager, String callerName, Issue issue, Map transientVars) {
        this.fieldProvider = new FieldProvider(fieldManager, customFieldManager,issueLinkManager, issue, transientVars);
        this.groupManager = groupManager;
        this.userManager = userManager;
        this.projectRoleManager = projectRoleManager;
        this.issue = issue;
        this.callerName = callerName;
    }

    /**
     * This method is given a node (which must be an ASTAtom) and returns either the result of evaluating the Arithmetic
     * expression that is described by the children of the node, or simply returns the only child.
     * 
     * @param node the node
     * 
     * @return the GenericValue that is the result of evaluating the expression in the node's child/children.
     */
    public GenericValue evaluate(Node node) {
    	GenericValueFactory genericValueFactory = new GenericValueFactory(fieldProvider, groupManager, userManager,
    			projectRoleManager, callerName);

        // Returning a simple literal
        if (node.jjtGetNumChildren() == 1) {
            String literalString = ((ASTLiteral) node.jjtGetChild(0)).getName();
            return genericValueFactory.getGenericValueFromString(literalString, issue);
        }

        GenericValue leftVal = genericValueFactory.getGenericValueFromString(
                ((ASTLiteral) node.jjtGetChild(0)).getName(), issue);
        String arithmeticOperator = ((ASTArithmeticOperator) node.jjtGetChild(1)).getName();
        GenericValue rightVal = genericValueFactory.getGenericValueFromString(
                ((ASTLiteral) node.jjtGetChild(2)).getName(), issue);
        GenericValue evaluatedExpression;

        // Trying adding and subtracting as these are the only permitted operations
        try {
            if (arithmeticOperator.equals("+")) {
                evaluatedExpression = leftVal.add(rightVal);
            } else {
                evaluatedExpression = leftVal.subtract(rightVal);
            }
        } catch (IncompatibleTypesException e) {
            log.error(e, e);
            evaluatedExpression = leftVal;
            log.warn("Incompatible types in arithmetic Operation (" + leftVal.toString() + " " + arithmeticOperator
                    + " " + rightVal.toString() + ")\n" + Arrays.toString(e.getStackTrace()));
        }
        return evaluatedExpression;
    }
}
