package com.tng.jira.workflowenhancer.evaluator.types;

/**
 * An exception that is thrown when an Operation on two GenericValues is attempted that is undefined.
 * 
 */
public class IncompatibleTypesException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3797583997758906112L;

    public IncompatibleTypesException(String msg) {
        super(msg);

    }
}
