package com.tng.jira.workflowenhancer.evaluator.types;

import java.util.List;

public class WorkTimespanValue extends AbstractTimespanValue {

    /**
     * Constructs a TimespanValue from a string. The units depend on the values of the constants
     * AbstractTimespanValue.numberOfDaysPerWeek and AbstractTimespanValue.numberOfHoursPerDay
     * 
     * @param value String in work timespan format _w _d _h _min
     */
    public WorkTimespanValue(String value) {
        HoursPerDay = numberOfHoursPerWorkingDay;
        DaysPerWeek = numberOfDaysPerWorkingWeek;

        float minutes = 0;
        List<String> valueComponents = getValueComponents(value);

        for (int i = 0; i < valueComponents.size(); i++) {
            if ((valueComponents.get(i)).endsWith("w")) {
                minutes = minutes + DaysPerWeek * HoursPerDay * 60 * getIntFromStr(valueComponents.get(i));
            } else if ((valueComponents.get(i)).endsWith("d")) {
                minutes = minutes + HoursPerDay * 60 * getIntFromStr(valueComponents.get(i));
            } else if ((valueComponents.get(i)).endsWith("h")) {
                minutes = minutes + 60 * getIntFromStr(valueComponents.get(i));
            } else {
                minutes = minutes + getIntFromStr(valueComponents.get(i));
            }
        }

        addTime(Math.abs(minutes));
    }

    public WorkTimespanValue(int weeks, int days, int hours, int minutes) {
        HoursPerDay = numberOfHoursPerWorkingDay;
        DaysPerWeek = numberOfDaysPerWorkingWeek;
        float min = minutes + 60 * hours + HoursPerDay * 60 * days + DaysPerWeek * HoursPerDay * 60 * weeks;

        addTime(Math.abs(min));
    }

    public WorkTimespanValue(int minutes) {
        HoursPerDay = numberOfHoursPerWorkingDay;
        DaysPerWeek = numberOfDaysPerWorkingWeek;

        addTime(Math.abs(minutes));
    }

    @Override
    public String toString() {
        String result = weeks + "w " + days + "d " + hours + "h " + minutes + "min";
        return result;
    }

    @Override
    public int getIntFromStr(String stringNumber) {
        if (!stringNumber.endsWith("min")) {
            return Integer.parseInt(stringNumber.substring(0, stringNumber.length() - 1));
        }
        return Integer.parseInt(stringNumber.substring(0, stringNumber.length() - 3));
    }
}
