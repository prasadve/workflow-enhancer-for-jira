package com.tng.jira.workflowenhancer.evaluator.types;

public class StringValue extends AbstractGenericValue implements GenericValue {

    private String value;

    public StringValue(String value) {
        this(value, false);
    }

    public StringValue(String value, boolean strip) {
    	/*
    	 * Set the value to the empty string if value is null to be consistent with fields that are null.
    	 * This should not happen during normal operations, but can happen e.g. if during import JIRA
    	 * returns null as the current user's name.
    	 */
    	if (value == null) {
    		this.value = "";
    	} else if (strip) {
            this.value = value.substring(1, value.length() - 1); // Strip " "
        } else {
            this.value = value;
        }
    }

    @Override
    public Object getValue() {
        return value;
    }

    /**
     * Compares two strings lexicographically. The comparison is based on the Unicode value of each character in the
     * strings. compareTo returns 0 exactly when the equals method would return true.
     * 
     * @param rightString StringValue
     * @return the value 0 if the argument string is equal to this string; a value less than 0 if this string is
     *         lexicographically less than the string argument; and a value greater than 0 if this string is
     *         lexicographically greater than the string argument.
     * @throws IncompatibleTypesException
     */
    public int compareTo(StringValue rightString) throws IncompatibleTypesException {
        return value.compareTo((String) rightString.getValue());
    }

    /**
     * Addition is concatenation.
     * 
     * @param rightString StringValue
     * @return StringValue containing the concatenation of this.value and StringValue.value
     */
    public GenericValue add(StringValue rightString) {
        return new StringValue(value + rightString.value);

    }

    @Override
    public int hashCode() {
        assert false : "hashCode not designed";
        return 1337; // arbitrary constant
    }
}
