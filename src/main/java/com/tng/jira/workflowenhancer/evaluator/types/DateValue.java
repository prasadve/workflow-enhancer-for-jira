package com.tng.jira.workflowenhancer.evaluator.types;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateValue extends AbstractGenericValue implements GenericValue {
	public static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy");
	public static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm");

    private final Date value;

    // This field denotes whether a time has been entered for this particular
    // date or if it is to be regarded as only a date.
    private boolean timeEntered = false;
    private static final int DEFAULT_HOUR = 12, DEFAULT_MINUTE = 0, DEFAULT_SECOND = 0;

    /**
     * Constructs a DateValue from a string.
     * 
     * @param value String in data format dd:mm:yyyy hh:minmin (time is optional)
     */
    public DateValue(String value) {
        String[] dateTimeComponents = value.split(" ");
        String[] dateComponents = dateTimeComponents[0].split("\\.");
        Calendar myDate = new GregorianCalendar();
        // Decreases the month by one because January=Month 0.
        // The default time is DEFAULT_HOUR:DEFAULT_MINUTE DEFAULT_SECOND
        myDate.set(Integer.parseInt(dateComponents[2]), Integer.parseInt(dateComponents[1]) - 1,
                Integer.parseInt(dateComponents[0]), DEFAULT_HOUR, DEFAULT_MINUTE, DEFAULT_SECOND);
        myDate.set(Calendar.MILLISECOND, 0);

        if (dateTimeComponents.length == 2) {

            // A Time was entered...
            String[] timeComponents = dateTimeComponents[1].split(":");
            myDate.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeComponents[0]));
            myDate.set(Calendar.MINUTE, Integer.parseInt(timeComponents[1]));
            timeEntered = true;
        }
        this.value = myDate.getTime();
    }

    public DateValue(Timestamp timestamp, boolean timeEntered) {
        Calendar myDate = new GregorianCalendar();
        myDate.setTimeInMillis(timestamp.getTime());

        myDate.set(Calendar.SECOND, 0);
        myDate.set(Calendar.MILLISECOND, 0);
        this.value = myDate.getTime();

        this.timeEntered = timeEntered;
    }

    public DateValue(Timestamp timestamp) {
	    this(timestamp, true);
	}

	protected DateValue(Date value) {
        Calendar myDate = new GregorianCalendar();
        myDate.setTime(value);

        myDate.set(Calendar.SECOND, 0);
        myDate.set(Calendar.MILLISECOND, 0);

        this.value = myDate.getTime();
    }

    protected DateValue(Date date, boolean hasTime) {
        this(date);
        timeEntered = hasTime;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        Calendar myDate = new GregorianCalendar();
        myDate.setTime(value);
        String out = "";

        if (myDate.get(Calendar.DATE) < 10) {
            out += "0";
        }
        out += myDate.get(Calendar.DATE) + ".";
        if (myDate.get(Calendar.MONTH) + 1 < 10) {
            out += "0";
        }
        out += (myDate.get(Calendar.MONTH) + 1) + ".";
        out += myDate.get(Calendar.YEAR);

        if (timeEntered) {
            out += " ";
            if (myDate.get(Calendar.HOUR_OF_DAY) < 10) {
                out += "0";
            }
            out += myDate.get(Calendar.HOUR_OF_DAY) + ":";
            if (myDate.get(Calendar.MINUTE) < 10) {
                out += "0";
            }
            out += myDate.get(Calendar.MINUTE);
        }
        return out;
    }

    @Override
    public int hashCode() {
        assert false : "hashCode not designed";
        return 1337; // arbitrary constant
    }

    /**
     * Compares this DateValue to a given DateValue.
     * 
     * @param rightDate
     * @return -1 if this DateValue is smaller than rightDate, 0 if the two values are equal and otherwise 1.
     */
    public int compareTo(DateValue rightDate) {
        Date dateX = value;
        Date dateY = (Date) rightDate.getValue();

        // Compare dates if both times consciously set, otherwise ignore time
        if (timeEntered && rightDate.timeEntered) {
        	return dateX.compareTo(dateY);
        } else {
        	return DateTimeComparator.getDateOnlyInstance().compare(dateX, dateY);
        }
    }

    /**
     * Adds a given RealTimespanValue to this DateValue.
     * 
     * @param timespan a RealTimespanValue to add
     * @return
     */
    public GenericValue add(RealTimespanValue timespan) {
        Calendar myDate = new GregorianCalendar();
        myDate.setTime(value);

        myDate.add(Calendar.DATE, 7 * timespan.getWeeks());
        myDate.add(Calendar.DATE, timespan.getDays());

        if (timeEntered) {
            myDate.add(Calendar.HOUR_OF_DAY, timespan.getHours());
            myDate.add(Calendar.MINUTE, timespan.getMinutes());
        }
        return new DateValue(myDate.getTime(), timeEntered);
    }

    /**
     * Subtracts a given RealTimespanValue from this DateValue.
     * 
     * @param timespan a RealTimespanValue to subtract
     * @return
     */
    public GenericValue subtract(RealTimespanValue timespan) {
        Calendar myDate = new GregorianCalendar();
        myDate.setTime(value);

        myDate.add(Calendar.DATE, -7 * timespan.getWeeks());
        myDate.add(Calendar.DATE, -timespan.getDays());

        if (timeEntered) {
            myDate.add(Calendar.HOUR_OF_DAY, -timespan.getHours());
            myDate.add(Calendar.MINUTE, -timespan.getMinutes());
        }

        return new DateValue(myDate.getTime(), timeEntered);
    }

    /**
     * Computes the difference between this DateValue and a given DateValue. Uses the absolut value of the difference of
     * the two Dates in milliseconds for comparison.
     * 
     * @param right
     * @return a RealTimespanValue which represents the difference between the two dates
     */
    public GenericValue subtract(DateValue right) {
        Calendar leftDate = new GregorianCalendar();
        leftDate.setTime(value);
        Calendar rightDate = new GregorianCalendar();
        rightDate.setTime(right.value);

        long dateDifference = Math.abs((leftDate.getTimeInMillis() - rightDate.getTimeInMillis()));

        int millisecondsPerMinute = 1000 * 60;

        if (timeEntered && right.timeEntered) {
            int minutes = (int) Math.floor(dateDifference / millisecondsPerMinute);
            return new RealTimespanValue(minutes);
        }
        float millisecondsPerDay = millisecondsPerMinute * 60 * 24; //Wird in echter Zeit berechnet
        int days = (int) Math.floor(dateDifference / millisecondsPerDay);
        return new RealTimespanValue(0, days, 0, 0);
    }

}
