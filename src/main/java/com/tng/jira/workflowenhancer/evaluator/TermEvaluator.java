package com.tng.jira.workflowenhancer.evaluator;

import org.apache.log4j.Logger;

import com.tng.jira.workflowenhancer.evaluator.types.GenericValue;
import com.tng.jira.workflowenhancer.evaluator.types.GenericValueFactory;
import com.tng.jira.workflowenhancer.evaluator.types.IncompatibleTypesException;

public class TermEvaluator {

    private final static Logger log = Logger.getLogger(TermEvaluator.class);

    /**
     * Evaluates a term.
     * 
     * @param leftOperand the left operand
     * @param operator the operator
     * @param rightOperand the right operand
     * 
     * @return the result of evaluating the term.
     * 
     * @throws IncompatibleTypesException if the operator is undefined for the operands.
     */
    public boolean evaluate(GenericValue leftOperand, String operator, GenericValue rightOperand)
            throws IncompatibleTypesException {

        if (operator.equals("=") || operator.equals("==")) {
            return leftOperand.equals(rightOperand);
        } else if (operator.equals("!=")) {
            return !leftOperand.equals(rightOperand);
        } else if (operator.equals("<")) {
            return leftOperand.compareTo(rightOperand) < 0;
        } else if (operator.equals("<=")) {
            return leftOperand.compareTo(rightOperand) <= 0;
        } else if (operator.equals(">")) {
            return leftOperand.compareTo(rightOperand) > 0;
        } else if (operator.equals(">=")) {
            return leftOperand.compareTo(rightOperand) >= 0;
        }
        log.error("An error occured evaluating an expression. Returning false. (" + leftOperand + " " + operator + " "
                + rightOperand + ")");
        return false;
    }

    // For unit tests
    public boolean evaluate(String lStr, String operator, String rStr) throws IncompatibleTypesException {
    	GenericValueFactory genericValueFactory = new GenericValueFactory(null, null, null, null, null);
        GenericValue leftValue = genericValueFactory.getGenericValueFromString(lStr, null);
        GenericValue rightValue = genericValueFactory.getGenericValueFromString(rStr, null);

        return evaluate(leftValue, operator, rightValue);
    }
}
