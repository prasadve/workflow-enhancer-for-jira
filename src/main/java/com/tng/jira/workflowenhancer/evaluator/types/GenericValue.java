package com.tng.jira.workflowenhancer.evaluator.types;

/**
 * Values that are used within the expression should implement this interface. Additionally they should extend
 * AbstractGenericValue as this includes the code for throwing IncompatibleTypesExceptions when appropriate, as well as
 * other useful methods.
 */
public interface GenericValue {

    /**
     * Gets the value associated with the GenericValue Object. The type it returns will depend on the implementation of
     * this interface.
     * 
     * @return the value
     */
    public Object getValue();

    /**
     * Checks two GenericValues for equality.
     * 
     * @param genericValue the generic value
     * 
     * @return true if the values are equal.
     * 
     * @throws IncompatibleTypesException if equality for the values to be compared is undefined.
     */
    public boolean equals(GenericValue genericValue) throws IncompatibleTypesException;

    /**
     * Checks the ordering on the two operands.
     * 
     * @param genericValue genericValue
     * 
     * @return a positive integer if THIS > than genericValue, a negative integer if THIS < than the genericValue and 0
     *         if they are equal (for some definition of the ordering operators which must not coincide with the
     *         definition of equality in the equals() method).
     * 
     * @throws IncompatibleTypesException if an ordering for the values to be compared is undefined.
     */
    public int compareTo(GenericValue genericValue) throws IncompatibleTypesException;

    /**
     * Adds or concatenates the two values.
     * 
     * @param genericValue the generic value
     * 
     * @return a new GenericValue that is the result of adding THIS to genericValue.
     * 
     * @throws IncompatibleTypesException if addition for the values is undefined.
     */
    public GenericValue add(GenericValue genericValue) throws IncompatibleTypesException;

    /**
     * Subtract.
     * 
     * @param genericValue the right operand
     * 
     * @return a new GenericValue that is the result of subtracting genericValue from THIS.
     * 
     * @throws IncompatibleTypesException if subtraction for the values is undefined.
     */
    public GenericValue subtract(GenericValue rightOperand) throws IncompatibleTypesException;
}
