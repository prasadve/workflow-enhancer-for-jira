package com.tng.jira.workflowenhancer.evaluator.parser;
public class ASTOperator extends SimpleNode {
  private String name;

  /**
   * Constructor.
   * @param id the id
   */
  public ASTOperator(int id) {
    super(id);
  }

 
  /**
   * Set the name.
   * @param n the name
   */
  public void setName(String n) {
    name = n;
  }

  /**
   * {@inheritDoc}
   * @see org.javacc.examples.jjtree.eg2.SimpleNode#toString()
   */
  public String toString() {
    return super.toString() + ": " + name;
  }

  public String getName() {
      return name;
  }
  
}
