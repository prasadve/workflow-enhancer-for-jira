#JIRA Workflow Enhancer User Guide#

Document Version: 27/08/2009  
Author: Johannes Lehmann  
Company: TNG Technology Consulting  


##Introduction##

This plug-in is aimed at enhancing the workflow validators and conditions in JIRA by allowing the value of a field to be compared not only with another field or a value but making a wide range of comparisons possible. The key features are:

* Can be used as a workflow validator and condition
* Ability to enter full boolean expressions
* String equality and regular expression matching
* Ordering and equality on dates (with or without a time attached)
* Ordering and equality on numeric fields
* Ordering and equality of time spans (in a particular format)
* Addition and subtraction with sensible semantics depending on the context
* Macros for the currently logged in user and the current date
* Custom messages for validator

In the remainder of this document I will explain using examples how the plug-in can be used. Note that a very brief description of the syntax can be found on the page where expressions can be edited, for reference.


##A Top-Level Overview of the Expressions##

I am assuming that the reader is familiar with the notion of a boolean expression. 
Spaces can be inserted between the symbols of the expression as required to make the expression more readable.

A valid **expression** is either a disjunction (ORing) or a conjunction (ANDing) of either terms or bracketed expressions.  
The syntax for OR is any of `|`, `||` and `OR`, for AND it is any of `&`, `&&`, `AND`. 

Let `...` be an arbitrary term.  
Valid expressions are `...&&...AND...`, `...|...|...`, `...&&(...||...)`, `...`.  
Invalid expressions are `...&&...||...`, `...&&...&&`.  

A **term** consists of two values (either of which may be an arithmetic expression) combined with an infix ordering operator. A term may be negated.  
The supported ordering operators are: `=` (or `==`), `!=`, `<`, `<=`, `>`, `>=`.  
The syntax for the negation of a term is a prefixed `!`.
	
Let `...` be an arbitrary value.  
Valid terms are `...<...`, `!...>...`, `!(...=...)`.  
Invalid terms are `...=...=...`, `(...)>(...)`.

A **value** may either be a literal, a field identifier, an arithmetic expression or a macro.

**Literals** may be one of a date, a string, a time span or a floating point number :

* **Date:** Dates must be written in the format `DD.MM.YYYY HH:MM` although specifying a time is optional.
* **String:** A string is enclosed by parentheses.
* **Real time span:** A time span is specified as `wW dD hH minMIN` (where w is the number of weeks, d the number of days etc.). One or more of the units may be omitted but the order must 	be preserved.
* **Labour time span:** A labour time span is specified as `Ww Dd Hh MINmin` (where W is the number of weeks, D the number of days etc.). One or more of the units may be omitted but the order must be preserved. **Note that these weeks and days correspond to work units, e.g. 5 days per week.** To avoid confusion, they cannot be subtracted from or added to dates or real time spans.
* **Floats:** Ordinary floating point numbers.
* **Field identifiers:** Field identifiers are written as `{field_id}` or `{{field_id}}`, where field_id is the id of the field you are referring to. Use the list to the right of the expression to automatically insert the correct identifier for you. The difference is that the former will evaluate the field values while the latter will not (see section Field Identifiers below).

* **Macros:** `[now]` will get the current date and time. `[user]` will get the user name of the current user. `[groups]` will list the groups of the current user. `[roles]` will list the roles of the current user in the current project. **Note that both groups and roles are case-sensitive.** `[numberSubtasks]` will get the number of subtasks of the current issue, while `[subtasks#field]` will list the `{{field}}`-values of the subtasks of the current issue, that is field values will be returned but not evaluated.

Valid values are `24.12.2011 12:30`, `“Foo”`, `3m 2d`, `90min`, `3.14`, `12`, `{duedate}`.  
Invalid values are `01.01.01`, `2m 1y`, `3.14.5`, `{due date}`.

An **arithmetic expression** consists of two literals or field identifiers, combined by one of the two supported infix arithmetic operators. Valid operators are `+` and `-`.

Let `...` be an arbitrary value.  
Valid arithmetic expressions are `...+...`, `...-...`.  
Invalid arithmetic expressions are `...+...+...`, `...+(...-...)`, `(...+...)`.

##Semantics of Operators and Fields##

**Important:** If a part of the expression cannot be evaluated because the types are not compatible, the part will evaluate to false and a warning will be written to the log (atlassian-jira.log). If a field value can not be retrieved it will become the empty string and a warning will be written to the log.

There is nothing to say about the semantics of boolean operators as they have the traditional meaning in this case, which is defined elsewhere. 

###Ordering Operators###

For most comparisons the semantics should be clear although in some cases I have chosen to allow a comparison which may not have perfectly intuitive semantics unless they are what you are looking for. 

**Note in particular that the ordering semantics differ from the equality semantics in the case of strings and hence `A<=B && A>=B` does not imply `A==B` for strings.**

**Noteworthy cases are:**

* Dates: A date is smaller than another if it is earlier. Note that the time is omitted on one side of the comparison then it is not taken into account. Hence `10.12.2000 < 10.12.2000 23:59` will evaluate to false. This matters when getting dates out of JIRA.
* Strings: A string is smaller than another if it is shorter.

###Equality###

As with ordering the equality for most types should be clear.

Noteworthy cases are:

* Dates: As with ordering, time is not taken into account when one side of the equality has no time component (i.e. `10.12.2000 == 10.12.2000 14:23` is true). 
* String == String: If we are comparing two Strings `A` and `B` say,  `A==B` if `A` matches the regular expression `B` or they are syntactically equal. This has two important consequences: 

	* The equality operator is not symmetric for strings (i.e. `A==B` does not imply `B==A`)
	* In some rare cases you may get an unintended match. For example `.` matches any character, not just the dot (in this case you would have to use `\\.`).

For more information on regular expressions please consult [this website](https://docs.oracle.com/javase/tutorial/essential/regex/).

###Addition and Subtraction###

Addition and subtraction is undefined for many combinations of operands. The cases in which they are defined (with semantics) are:

* String + String (Concatenation)
* Date (+/-) Real time span (Returns a date)
* Time span (+/-) Time span (real and labour, respectively)
* Float (+/-) Float
* Date – Date (Returns a real time span)

###String representations###

For comparison with a regular expression each value has a string representation.

* String: Its value.
* Date: The date in the format `DD.MM.YYYY HH:MM`. The time will be omitted if none was entered. If the date comes from a field from JIRA the time will always be regarded as a time that has been entered.
* Float: Floats are represented using Java's `Double.toString()` method. Note that 0 is represented as `0.0` etc.
* Time span: The time span as it has been entered but including all components. 

###Field Identifier###

JIRA fields (written as `{field_id}`) may themselves contain one of the above values. Therefore if a field contains a value such as `{field_id2}`, it will automatically be parsed as the value of the second field. **This allows a chain of field identifiers (by putting the identifier of another field in a field).** An application may be that the first element of the chain is set as a post-function to dynamically change the behavior of the condition / validator depending on the steps in the workflow we have visited). To avoid infinite loops, there is a maximum recursion depth of 50.

However, using `{{field_id}}` will prevent the field's value from being evaluated. In the above example `{{field_id}}` returns the string `{field_id2}` instead of the value of `{field_id2}`.

##Examples##

Here are some examples that illustrate use cases of the plug-in.

* A project may not be submitted if the due date isn't more than seven days from now:
	`{duedate} > [now] + 7d`

* The assignee must be the current user or the admin (the brackets are optional):
	`({assignee} == \[user\]) || ({assignee} == “admin”)`

* A description must be entered:
	`{description} != ""`

* The summary must contain TNG:
	`{summary} = ".*TNG.*"`

* Two date custom fields must not be more than two weeks apart:
	`{customfield_10000}-{customfield_10001} < 14d`


Whilst some of these examples could be done using JIRAs existing capabilities they can be conveniently entered on one screen using this plug-in. 
